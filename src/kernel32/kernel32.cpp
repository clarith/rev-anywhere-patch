#include <windows.h>
#include <assert.h>

HINSTANCE hLibMine;
HINSTANCE hLib;
FARPROC p[1571];

namespace {
    HANDLE hFddDriver;

    BOOL emulateDongle(
        DWORD        dwIoControlCode,
        LPVOID       lpOutBuffer,
        DWORD        nOutBufferSize,
        LPOVERLAPPED lpOverlapped
    )
    {
        assert(lpOverlapped);

        switch (dwIoControlCode) {
            case 0x0022a114:
            {
                lpOverlapped->Internal = 0;
                lpOverlapped->InternalHigh = 0;
                break;
            }

            case 0x0022e24c:
            {
                lpOverlapped->Internal = 0;
                lpOverlapped->InternalHigh = 8;

                assert(nOutBufferSize >= 8);
                BYTE data[] = { 0x0a, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, };
                memcpy(lpOutBuffer, data, 8);
                break;
            }

            case 0x0022e20c:
            {
                lpOverlapped->Internal = 0;
                lpOverlapped->InternalHigh = 4;

                assert(nOutBufferSize >= 4);
                BYTE data[] = { 0x00, 0x02, 0x00, 0x00, };
                memcpy(lpOutBuffer, data, 4);
                break;
            }
        }

        SetEvent(lpOverlapped->hEvent);

        // It's checked by REV app
        SetLastError(ERROR_IO_PENDING);

        // operation is pending
        return 0;
    }
}

extern "C"
{
    __declspec(dllexport) HANDLE WINAPI PROXY_CreateFileW(
        LPCWSTR               lpFileName,
        DWORD                 dwDesiredAccess,
        DWORD                 dwShareMode,
        LPSECURITY_ATTRIBUTES lpSecurityAttributes,
        DWORD                 dwCreationDisposition,
        DWORD                 dwFlagsAndAttributes,
        HANDLE                hTemplateFile
    )
    {
        if (lstrcmpW(lpFileName, L"\\??\\FddDriver") == 0) {
            hFddDriver = (HANDLE)0xdeadbeef;
            return hFddDriver;
        }

        return CreateFileW(lpFileName, dwDesiredAccess, dwShareMode, lpSecurityAttributes, dwCreationDisposition, dwFlagsAndAttributes, hTemplateFile);
    }

    __declspec(dllexport) BOOL WINAPI PROXY_CloseHandle(HANDLE hObject)
    {
        if (hObject == hFddDriver) {
            hFddDriver = NULL;
            return TRUE;
        }

        return CloseHandle(hObject);
    }

    __declspec(dllexport) BOOL WINAPI PROXY_DeviceIoControl(
        HANDLE       hDevice,
        DWORD        dwIoControlCode,
        LPVOID       lpInBuffer,
        DWORD        nInBufferSize,
        LPVOID       lpOutBuffer,
        DWORD        nOutBufferSize,
        LPDWORD      lpBytesReturned,
        LPOVERLAPPED lpOverlapped
    )
    {
        if (hDevice == hFddDriver) {
            return emulateDongle(dwIoControlCode, lpOutBuffer, nOutBufferSize, lpOverlapped);
        }

        return DeviceIoControl(
            hDevice,
            dwIoControlCode,
            lpInBuffer,
            nInBufferSize,
            lpOutBuffer,
            nOutBufferSize,
            lpBytesReturned,
            lpOverlapped
        );
    }

    __declspec(dllexport) BOOL WINAPI PROXY_GetOverlappedResult(
        HANDLE       hFile,
        LPOVERLAPPED lpOverlapped,
        LPDWORD      lpNumberOfBytesTransferred,
        BOOL         bWait
    )
    {
        // Sometimes called with hFile == NULL
        if (hFile == hFddDriver) {
            *lpNumberOfBytesTransferred = lpOverlapped->InternalHigh;
            return TRUE;
        }

        return GetOverlappedResult(hFile, lpOverlapped, lpNumberOfBytesTransferred, bWait);
    }
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    if (fdwReason == DLL_PROCESS_ATTACH)
    {
        hLibMine = hinstDLL;
        hLib = LoadLibrary("C:\\Windows\\SysWOW64\\kernel32.dll");
        if(!hLib) return FALSE;

        p[0] = GetProcAddress(hLib, "BaseThreadInitThunk");
        p[1] = GetProcAddress(hLib, "InterlockedPushListSList"); //Forwarder: NTDLL.RtlInterlockedPushListSList
        p[2] = GetProcAddress(hLib, "AcquireSRWLockExclusive"); //Forwarder: NTDLL.RtlAcquireSRWLockExclusive
        p[3] = GetProcAddress(hLib, "AcquireSRWLockShared"); //Forwarder: NTDLL.RtlAcquireSRWLockShared
        p[4] = GetProcAddress(hLib, "AcquireStateLock");
        p[5] = GetProcAddress(hLib, "ActivateActCtx");
        p[6] = GetProcAddress(hLib, "ActivateActCtxWorker");
        p[7] = GetProcAddress(hLib, "AddAtomA");
        p[8] = GetProcAddress(hLib, "AddAtomW");
        p[9] = GetProcAddress(hLib, "AddConsoleAliasA");
        p[10] = GetProcAddress(hLib, "AddConsoleAliasW");
        p[11] = GetProcAddress(hLib, "AddDllDirectory"); //Forwarder: api-ms-win-core-libraryloader-l1-1-0.AddDllDirectory
        p[12] = GetProcAddress(hLib, "AddIntegrityLabelToBoundaryDescriptor");
        p[13] = GetProcAddress(hLib, "AddLocalAlternateComputerNameA");
        p[14] = GetProcAddress(hLib, "AddLocalAlternateComputerNameW");
        p[15] = GetProcAddress(hLib, "AddRefActCtx");
        p[16] = GetProcAddress(hLib, "AddRefActCtxWorker");
        p[17] = GetProcAddress(hLib, "AddResourceAttributeAce");
        p[18] = GetProcAddress(hLib, "AddSIDToBoundaryDescriptor");
        p[19] = GetProcAddress(hLib, "AddScopedPolicyIDAce");
        p[20] = GetProcAddress(hLib, "AddSecureMemoryCacheCallback");
        p[21] = GetProcAddress(hLib, "AddVectoredContinueHandler"); //Forwarder: NTDLL.RtlAddVectoredContinueHandler
        p[22] = GetProcAddress(hLib, "AddVectoredExceptionHandler"); //Forwarder: NTDLL.RtlAddVectoredExceptionHandler
        p[23] = GetProcAddress(hLib, "AdjustCalendarDate");
        p[24] = GetProcAddress(hLib, "AllocConsole");
        p[25] = GetProcAddress(hLib, "AllocateUserPhysicalPages");
        p[26] = GetProcAddress(hLib, "AllocateUserPhysicalPagesNuma");
        p[27] = GetProcAddress(hLib, "AppContainerDeriveSidFromMoniker");
        p[28] = GetProcAddress(hLib, "AppContainerFreeMemory");
        p[29] = GetProcAddress(hLib, "AppContainerLookupDisplayNameMrtReference");
        p[30] = GetProcAddress(hLib, "AppContainerLookupMoniker");
        p[31] = GetProcAddress(hLib, "AppContainerRegisterSid");
        p[32] = GetProcAddress(hLib, "AppContainerUnregisterSid");
        p[33] = GetProcAddress(hLib, "AppXFreeMemory");
        p[34] = GetProcAddress(hLib, "AppXGetApplicationData");
        p[35] = GetProcAddress(hLib, "AppXGetDevelopmentMode");
        p[36] = GetProcAddress(hLib, "AppXGetOSMaxVersionTested");
        p[37] = GetProcAddress(hLib, "AppXGetOSMinVersion");
        p[38] = GetProcAddress(hLib, "AppXGetPackageCapabilities");
        p[39] = GetProcAddress(hLib, "AppXGetPackageSid");
        p[40] = GetProcAddress(hLib, "AppXGetPackageState");
        p[41] = GetProcAddress(hLib, "AppXLookupDisplayName");
        p[42] = GetProcAddress(hLib, "AppXLookupMoniker");
        p[43] = GetProcAddress(hLib, "AppXSetPackageState");
        p[44] = GetProcAddress(hLib, "ApplicationRecoveryFinished");
        p[45] = GetProcAddress(hLib, "ApplicationRecoveryInProgress");
        p[46] = GetProcAddress(hLib, "AreFileApisANSI");
        p[47] = GetProcAddress(hLib, "AssignProcessToJobObject");
        p[48] = GetProcAddress(hLib, "AttachConsole");
        p[49] = GetProcAddress(hLib, "BackupRead");
        p[50] = GetProcAddress(hLib, "BackupSeek");
        p[51] = GetProcAddress(hLib, "BackupWrite");
        p[52] = GetProcAddress(hLib, "BaseCheckAppcompatCache");
        p[53] = GetProcAddress(hLib, "BaseCheckAppcompatCacheEx");
        p[54] = GetProcAddress(hLib, "BaseCheckAppcompatCacheExWorker");
        p[55] = GetProcAddress(hLib, "BaseCheckAppcompatCacheWorker");
        p[56] = GetProcAddress(hLib, "BaseCheckElevation");
        p[57] = GetProcAddress(hLib, "BaseCheckRunApp");
        p[58] = GetProcAddress(hLib, "BaseCleanupAppcompatCacheSupport");
        p[59] = GetProcAddress(hLib, "BaseCleanupAppcompatCacheSupportWorker");
        p[60] = GetProcAddress(hLib, "BaseDestroyVDMEnvironment");
        p[61] = GetProcAddress(hLib, "BaseDllReadWriteIniFile");
        p[62] = GetProcAddress(hLib, "BaseDumpAppcompatCache");
        p[63] = GetProcAddress(hLib, "BaseDumpAppcompatCacheWorker");
        p[64] = GetProcAddress(hLib, "BaseElevationPostProcessing");
        p[65] = GetProcAddress(hLib, "BaseFlushAppcompatCache");
        p[66] = GetProcAddress(hLib, "BaseFlushAppcompatCacheWorker");
        p[67] = GetProcAddress(hLib, "BaseFormatObjectAttributes");
        p[68] = GetProcAddress(hLib, "BaseFormatTimeOut");
        p[69] = GetProcAddress(hLib, "BaseGenerateAppCompatData");
        p[70] = GetProcAddress(hLib, "BaseGetNamedObjectDirectory");
        p[71] = GetProcAddress(hLib, "BaseInitAppcompatCacheSupport");
        p[72] = GetProcAddress(hLib, "BaseInitAppcompatCacheSupportWorker");
        p[73] = GetProcAddress(hLib, "BaseIsAppcompatInfrastructureDisabled");
        p[74] = GetProcAddress(hLib, "BaseIsAppcompatInfrastructureDisabledWorker");
        p[75] = GetProcAddress(hLib, "BaseIsDosApplication");
        p[76] = GetProcAddress(hLib, "BaseQueryModuleData");
        p[77] = GetProcAddress(hLib, "BaseSetLastNTError");
        p[78] = GetProcAddress(hLib, "BaseUpdateAppcompatCache");
        p[79] = GetProcAddress(hLib, "BaseUpdateAppcompatCacheWorker");
        p[80] = GetProcAddress(hLib, "BaseUpdateVDMEntry");
        p[81] = GetProcAddress(hLib, "BaseVerifyUnicodeString");
        p[82] = GetProcAddress(hLib, "BaseWriteErrorElevationRequiredEvent");
        p[83] = GetProcAddress(hLib, "Basep8BitStringToDynamicUnicodeString");
        p[84] = GetProcAddress(hLib, "BasepAllocateActivationContextActivationBlock");
        p[85] = GetProcAddress(hLib, "BasepAnsiStringToDynamicUnicodeString");
        p[86] = GetProcAddress(hLib, "BasepAppCompatHookDLL");
        p[87] = GetProcAddress(hLib, "BasepAppContainerEnvironmentExtension");
        p[88] = GetProcAddress(hLib, "BasepAppXExtension");
        p[89] = GetProcAddress(hLib, "BasepCheckAppCompat");
        p[90] = GetProcAddress(hLib, "BasepCheckBadapp");
        p[91] = GetProcAddress(hLib, "BasepCheckWebBladeHashes");
        p[92] = GetProcAddress(hLib, "BasepCheckWinSaferRestrictions");
        p[93] = GetProcAddress(hLib, "BasepConstructSxsCreateProcessMessage");
        p[94] = GetProcAddress(hLib, "BasepCopyEncryption");
        p[95] = GetProcAddress(hLib, "BasepFreeActivationContextActivationBlock");
        p[96] = GetProcAddress(hLib, "BasepFreeAppCompatData");
        p[97] = GetProcAddress(hLib, "BasepGetAppCompatData");
        p[98] = GetProcAddress(hLib, "BasepGetComputerNameFromNtPath");
        p[99] = GetProcAddress(hLib, "BasepGetExeArchType");
        p[100] = GetProcAddress(hLib, "BasepIsProcessAllowed");
        p[101] = GetProcAddress(hLib, "BasepMapModuleHandle");
        p[102] = GetProcAddress(hLib, "BasepNotifyLoadStringResource");
        p[103] = GetProcAddress(hLib, "BasepPostSuccessAppXExtension");
        p[104] = GetProcAddress(hLib, "BasepProcessInvalidImage");
        p[105] = GetProcAddress(hLib, "BasepQueryAppCompat");
        p[106] = GetProcAddress(hLib, "BasepReleaseAppXContext");
        p[107] = GetProcAddress(hLib, "BasepReleaseSxsCreateProcessUtilityStruct");
        p[108] = GetProcAddress(hLib, "BasepReportFault");
        p[109] = GetProcAddress(hLib, "BasepSetFileEncryptionCompression");
        p[110] = GetProcAddress(hLib, "Beep");
        p[111] = GetProcAddress(hLib, "BeginUpdateResourceA");
        p[112] = GetProcAddress(hLib, "BeginUpdateResourceW");
        p[113] = GetProcAddress(hLib, "BindIoCompletionCallback");
        p[114] = GetProcAddress(hLib, "BuildCommDCBA");
        p[115] = GetProcAddress(hLib, "BuildCommDCBAndTimeoutsA");
        p[116] = GetProcAddress(hLib, "BuildCommDCBAndTimeoutsW");
        p[117] = GetProcAddress(hLib, "BuildCommDCBW");
        p[118] = GetProcAddress(hLib, "CallNamedPipeA");
        p[119] = GetProcAddress(hLib, "CallNamedPipeW");
        p[120] = GetProcAddress(hLib, "CallbackMayRunLong");
        p[121] = GetProcAddress(hLib, "CancelDeviceWakeupRequest");
        p[122] = GetProcAddress(hLib, "CancelIo");
        p[123] = GetProcAddress(hLib, "CancelIoEx");
        p[124] = GetProcAddress(hLib, "CancelSynchronousIo");
        p[125] = GetProcAddress(hLib, "CancelThreadpoolIo"); //Forwarder: NTDLL.TpCancelAsyncIoOperation
        p[126] = GetProcAddress(hLib, "CancelTimerQueueTimer");
        p[127] = GetProcAddress(hLib, "CancelWaitableTimer");
        p[128] = GetProcAddress(hLib, "CeipIsOptedIn");
        p[129] = GetProcAddress(hLib, "ChangeTimerQueueTimer");
        p[130] = GetProcAddress(hLib, "CheckAllowDecryptedRemoteDestinationPolicy");
        p[131] = GetProcAddress(hLib, "CheckElevation");
        p[132] = GetProcAddress(hLib, "CheckElevationEnabled");
        p[133] = GetProcAddress(hLib, "CheckForReadOnlyResource");
        p[134] = GetProcAddress(hLib, "CheckForReadOnlyResourceFilter");
        p[135] = GetProcAddress(hLib, "CheckNameLegalDOS8Dot3A");
        p[136] = GetProcAddress(hLib, "CheckNameLegalDOS8Dot3W");
        p[137] = GetProcAddress(hLib, "CheckRemoteDebuggerPresent");
        p[138] = GetProcAddress(hLib, "CheckTokenCapability");
        p[139] = GetProcAddress(hLib, "CheckTokenMembershipEx");
        p[140] = GetProcAddress(hLib, "ClearCommBreak");
        p[141] = GetProcAddress(hLib, "ClearCommError");
        p[142] = GetProcAddress(hLib, "CloseConsoleHandle");
        p[143] = GetProcAddress(hLib, "CloseHandle");
        p[144] = GetProcAddress(hLib, "ClosePackageInfo");
        p[145] = GetProcAddress(hLib, "ClosePrivateNamespace");
        p[146] = GetProcAddress(hLib, "CloseProfileUserMapping");
        p[147] = GetProcAddress(hLib, "CloseState");
        p[148] = GetProcAddress(hLib, "CloseStateAtom");
        p[149] = GetProcAddress(hLib, "CloseStateChangeNotification");
        p[150] = GetProcAddress(hLib, "CloseStateContainer");
        p[151] = GetProcAddress(hLib, "CloseStateLock");
        p[152] = GetProcAddress(hLib, "CloseThreadpool"); //Forwarder: NTDLL.TpReleasePool
        p[153] = GetProcAddress(hLib, "CloseThreadpoolCleanupGroup"); //Forwarder: NTDLL.TpReleaseCleanupGroup
        p[154] = GetProcAddress(hLib, "CloseThreadpoolCleanupGroupMembers"); //Forwarder: NTDLL.TpReleaseCleanupGroupMembers
        p[155] = GetProcAddress(hLib, "CloseThreadpoolIo"); //Forwarder: NTDLL.TpReleaseIoCompletion
        p[156] = GetProcAddress(hLib, "CloseThreadpoolTimer"); //Forwarder: NTDLL.TpReleaseTimer
        p[157] = GetProcAddress(hLib, "CloseThreadpoolWait"); //Forwarder: NTDLL.TpReleaseWait
        p[158] = GetProcAddress(hLib, "CloseThreadpoolWork"); //Forwarder: NTDLL.TpReleaseWork
        p[159] = GetProcAddress(hLib, "CmdBatNotification");
        p[160] = GetProcAddress(hLib, "CommConfigDialogA");
        p[161] = GetProcAddress(hLib, "CommConfigDialogW");
        p[162] = GetProcAddress(hLib, "CommitStateAtom");
        p[163] = GetProcAddress(hLib, "CompareCalendarDates");
        p[164] = GetProcAddress(hLib, "CompareFileTime");
        p[165] = GetProcAddress(hLib, "CompareStringA");
        p[166] = GetProcAddress(hLib, "CompareStringEx");
        p[167] = GetProcAddress(hLib, "CompareStringOrdinal");
        p[168] = GetProcAddress(hLib, "CompareStringW");
        p[169] = GetProcAddress(hLib, "ConnectNamedPipe");
        p[170] = GetProcAddress(hLib, "ConsoleMenuControl");
        p[171] = GetProcAddress(hLib, "ContinueDebugEvent");
        p[172] = GetProcAddress(hLib, "ConvertCalDateTimeToSystemTime");
        p[173] = GetProcAddress(hLib, "ConvertDefaultLocale");
        p[174] = GetProcAddress(hLib, "ConvertFiberToThread");
        p[175] = GetProcAddress(hLib, "ConvertNLSDayOfWeekToWin32DayOfWeek");
        p[176] = GetProcAddress(hLib, "ConvertSystemTimeToCalDateTime");
        p[177] = GetProcAddress(hLib, "ConvertThreadToFiber");
        p[178] = GetProcAddress(hLib, "ConvertThreadToFiberEx");
        p[179] = GetProcAddress(hLib, "CopyContext");
        p[180] = GetProcAddress(hLib, "CopyFile2");
        p[181] = GetProcAddress(hLib, "CopyFileA");
        p[182] = GetProcAddress(hLib, "CopyFileExA");
        p[183] = GetProcAddress(hLib, "CopyFileExW");
        p[184] = GetProcAddress(hLib, "CopyFileTransactedA");
        p[185] = GetProcAddress(hLib, "CopyFileTransactedW");
        p[186] = GetProcAddress(hLib, "CopyFileW");
        p[187] = GetProcAddress(hLib, "CopyLZFile");
        p[188] = GetProcAddress(hLib, "CreateActCtxA");
        p[189] = GetProcAddress(hLib, "CreateActCtxW");
        p[190] = GetProcAddress(hLib, "CreateActCtxWWorker");
        p[191] = GetProcAddress(hLib, "CreateBoundaryDescriptorA");
        p[192] = GetProcAddress(hLib, "CreateBoundaryDescriptorW");
        p[193] = GetProcAddress(hLib, "CreateConsoleScreenBuffer");
        p[194] = GetProcAddress(hLib, "CreateDirectoryA");
        p[195] = GetProcAddress(hLib, "CreateDirectoryExA");
        p[196] = GetProcAddress(hLib, "CreateDirectoryExW");
        p[197] = GetProcAddress(hLib, "CreateDirectoryTransactedA");
        p[198] = GetProcAddress(hLib, "CreateDirectoryTransactedW");
        p[199] = GetProcAddress(hLib, "CreateDirectoryW");
        p[200] = GetProcAddress(hLib, "CreateEventA");
        p[201] = GetProcAddress(hLib, "CreateEventExA");
        p[202] = GetProcAddress(hLib, "CreateEventExW");
        p[203] = GetProcAddress(hLib, "CreateEventW");
        p[204] = GetProcAddress(hLib, "CreateFiber");
        p[205] = GetProcAddress(hLib, "CreateFiberEx");
        p[206] = GetProcAddress(hLib, "CreateFile2");
        p[207] = GetProcAddress(hLib, "CreateFileA");
        p[208] = GetProcAddress(hLib, "CreateFileMappingA");
        p[209] = GetProcAddress(hLib, "CreateFileMappingFromApp"); //Forwarder: api-ms-win-core-memory-l1-1-1.CreateFileMappingFromApp
        p[210] = GetProcAddress(hLib, "CreateFileMappingNumaA");
        p[211] = GetProcAddress(hLib, "CreateFileMappingNumaW");
        p[212] = GetProcAddress(hLib, "CreateFileMappingW");
        p[213] = GetProcAddress(hLib, "CreateFileTransactedA");
        p[214] = GetProcAddress(hLib, "CreateFileTransactedW");
        p[215] = GetProcAddress(hLib, "CreateFileW");
        p[216] = GetProcAddress(hLib, "CreateHardLinkA");
        p[217] = GetProcAddress(hLib, "CreateHardLinkTransactedA");
        p[218] = GetProcAddress(hLib, "CreateHardLinkTransactedW");
        p[219] = GetProcAddress(hLib, "CreateHardLinkW");
        p[220] = GetProcAddress(hLib, "CreateIoCompletionPort");
        p[221] = GetProcAddress(hLib, "CreateJobObjectA");
        p[222] = GetProcAddress(hLib, "CreateJobObjectW");
        p[223] = GetProcAddress(hLib, "CreateJobSet");
        p[224] = GetProcAddress(hLib, "CreateMailslotA");
        p[225] = GetProcAddress(hLib, "CreateMailslotW");
        p[226] = GetProcAddress(hLib, "CreateMemoryResourceNotification");
        p[227] = GetProcAddress(hLib, "CreateMutexA");
        p[228] = GetProcAddress(hLib, "CreateMutexExA");
        p[229] = GetProcAddress(hLib, "CreateMutexExW");
        p[230] = GetProcAddress(hLib, "CreateMutexW");
        p[231] = GetProcAddress(hLib, "CreateNamedPipeA");
        p[232] = GetProcAddress(hLib, "CreateNamedPipeW");
        p[233] = GetProcAddress(hLib, "CreatePipe");
        p[234] = GetProcAddress(hLib, "CreatePrivateNamespaceA");
        p[235] = GetProcAddress(hLib, "CreatePrivateNamespaceW");
        p[236] = GetProcAddress(hLib, "CreateProcessA");
        p[237] = GetProcAddress(hLib, "CreateProcessAsUserW");
        p[238] = GetProcAddress(hLib, "CreateProcessInternalA");
        p[239] = GetProcAddress(hLib, "CreateProcessInternalW");
        p[240] = GetProcAddress(hLib, "CreateProcessW");
        p[241] = GetProcAddress(hLib, "CreateRemoteThread");
        p[242] = GetProcAddress(hLib, "CreateRemoteThreadEx"); //Forwarder: api-ms-win-core-processthreads-l1-1-0.CreateRemoteThreadEx
        p[243] = GetProcAddress(hLib, "CreateSemaphoreA");
        p[244] = GetProcAddress(hLib, "CreateSemaphoreExA");
        p[245] = GetProcAddress(hLib, "CreateSemaphoreExW");
        p[246] = GetProcAddress(hLib, "CreateSemaphoreW");
        p[247] = GetProcAddress(hLib, "CreateSocketHandle");
        p[248] = GetProcAddress(hLib, "CreateStateAtom");
        p[249] = GetProcAddress(hLib, "CreateStateChangeNotification");
        p[250] = GetProcAddress(hLib, "CreateStateContainer");
        p[251] = GetProcAddress(hLib, "CreateStateLock");
        p[252] = GetProcAddress(hLib, "CreateStateSubcontainer");
        p[253] = GetProcAddress(hLib, "CreateSymbolicLinkA");
        p[254] = GetProcAddress(hLib, "CreateSymbolicLinkTransactedA");
        p[255] = GetProcAddress(hLib, "CreateSymbolicLinkTransactedW");
        p[256] = GetProcAddress(hLib, "CreateSymbolicLinkW");
        p[257] = GetProcAddress(hLib, "CreateTapePartition");
        p[258] = GetProcAddress(hLib, "CreateThread");
        p[259] = GetProcAddress(hLib, "CreateThreadpool");
        p[260] = GetProcAddress(hLib, "CreateThreadpoolCleanupGroup");
        p[261] = GetProcAddress(hLib, "CreateThreadpoolIo");
        p[262] = GetProcAddress(hLib, "CreateThreadpoolTimer");
        p[263] = GetProcAddress(hLib, "CreateThreadpoolWait");
        p[264] = GetProcAddress(hLib, "CreateThreadpoolWork");
        p[265] = GetProcAddress(hLib, "CreateTimerQueue");
        p[266] = GetProcAddress(hLib, "CreateTimerQueueTimer");
        p[267] = GetProcAddress(hLib, "CreateToolhelp32Snapshot");
        p[268] = GetProcAddress(hLib, "CreateWaitableTimerA");
        p[269] = GetProcAddress(hLib, "CreateWaitableTimerExA");
        p[270] = GetProcAddress(hLib, "CreateWaitableTimerExW");
        p[271] = GetProcAddress(hLib, "CreateWaitableTimerW");
        p[272] = GetProcAddress(hLib, "CtrlRoutine"); //Forwarder: kernelbase.CtrlRoutine
        p[273] = GetProcAddress(hLib, "DeactivateActCtx");
        p[274] = GetProcAddress(hLib, "DeactivateActCtxWorker");
        p[275] = GetProcAddress(hLib, "DebugActiveProcess");
        p[276] = GetProcAddress(hLib, "DebugActiveProcessStop");
        p[277] = GetProcAddress(hLib, "DebugBreak");
        p[278] = GetProcAddress(hLib, "DebugBreakProcess");
        p[279] = GetProcAddress(hLib, "DebugSetProcessKillOnExit");
        p[280] = GetProcAddress(hLib, "DecodePointer"); //Forwarder: NTDLL.RtlDecodePointer
        p[281] = GetProcAddress(hLib, "DecodeSystemPointer"); //Forwarder: NTDLL.RtlDecodeSystemPointer
        p[282] = GetProcAddress(hLib, "DefineDosDeviceA");
        p[283] = GetProcAddress(hLib, "DefineDosDeviceW");
        p[284] = GetProcAddress(hLib, "DelayLoadFailureHook");
        p[285] = GetProcAddress(hLib, "DeleteAtom");
        p[286] = GetProcAddress(hLib, "DeleteBoundaryDescriptor");
        p[287] = GetProcAddress(hLib, "DeleteCriticalSection"); //Forwarder: NTDLL.RtlDeleteCriticalSection
        p[288] = GetProcAddress(hLib, "DeleteFiber");
        p[289] = GetProcAddress(hLib, "DeleteFileA");
        p[290] = GetProcAddress(hLib, "DeleteFileTransactedA");
        p[291] = GetProcAddress(hLib, "DeleteFileTransactedW");
        p[292] = GetProcAddress(hLib, "DeleteFileW");
        p[293] = GetProcAddress(hLib, "DeleteProcThreadAttributeList"); //Forwarder: api-ms-win-core-processthreads-l1-1-0.DeleteProcThreadAttributeList
        p[294] = GetProcAddress(hLib, "DeleteStateAtomValue");
        p[295] = GetProcAddress(hLib, "DeleteStateContainer");
        p[296] = GetProcAddress(hLib, "DeleteStateContainerValue");
        p[297] = GetProcAddress(hLib, "DeleteTimerQueue");
        p[298] = GetProcAddress(hLib, "DeleteTimerQueueEx");
        p[299] = GetProcAddress(hLib, "DeleteTimerQueueTimer");
        p[300] = GetProcAddress(hLib, "DeleteVolumeMountPointA");
        p[301] = GetProcAddress(hLib, "DeleteVolumeMountPointW");
        p[302] = GetProcAddress(hLib, "DeviceIoControl");
        p[303] = GetProcAddress(hLib, "DisableThreadLibraryCalls");
        p[304] = GetProcAddress(hLib, "DisableThreadProfiling");
        p[305] = GetProcAddress(hLib, "DisassociateCurrentThreadFromCallback"); //Forwarder: NTDLL.TpDisassociateCallback
        p[306] = GetProcAddress(hLib, "DisconnectNamedPipe");
        p[307] = GetProcAddress(hLib, "DnsHostnameToComputerNameA");
        p[308] = GetProcAddress(hLib, "DnsHostnameToComputerNameW");
        p[309] = GetProcAddress(hLib, "DosDateTimeToFileTime");
        p[310] = GetProcAddress(hLib, "DosPathToSessionPathA");
        p[311] = GetProcAddress(hLib, "DosPathToSessionPathW");
        p[312] = GetProcAddress(hLib, "DuplicateConsoleHandle");
        p[313] = GetProcAddress(hLib, "DuplicateEncryptionInfoFileExt");
        p[314] = GetProcAddress(hLib, "DuplicateHandle");
        p[315] = GetProcAddress(hLib, "DuplicateStateContainerHandle");
        p[316] = GetProcAddress(hLib, "EnableThreadProfiling");
        p[317] = GetProcAddress(hLib, "EncodePointer"); //Forwarder: NTDLL.RtlEncodePointer
        p[318] = GetProcAddress(hLib, "EncodeSystemPointer"); //Forwarder: NTDLL.RtlEncodeSystemPointer
        p[319] = GetProcAddress(hLib, "EndUpdateResourceA");
        p[320] = GetProcAddress(hLib, "EndUpdateResourceW");
        p[321] = GetProcAddress(hLib, "EnterCriticalSection"); //Forwarder: NTDLL.RtlEnterCriticalSection
        p[322] = GetProcAddress(hLib, "EnumCalendarInfoA");
        p[323] = GetProcAddress(hLib, "EnumCalendarInfoExA");
        p[324] = GetProcAddress(hLib, "EnumCalendarInfoExEx");
        p[325] = GetProcAddress(hLib, "EnumCalendarInfoExW");
        p[326] = GetProcAddress(hLib, "EnumCalendarInfoW");
        p[327] = GetProcAddress(hLib, "EnumDateFormatsA");
        p[328] = GetProcAddress(hLib, "EnumDateFormatsExA");
        p[329] = GetProcAddress(hLib, "EnumDateFormatsExEx");
        p[330] = GetProcAddress(hLib, "EnumDateFormatsExW");
        p[331] = GetProcAddress(hLib, "EnumDateFormatsW");
        p[332] = GetProcAddress(hLib, "EnumLanguageGroupLocalesA");
        p[333] = GetProcAddress(hLib, "EnumLanguageGroupLocalesW");
        p[334] = GetProcAddress(hLib, "EnumResourceLanguagesA");
        p[335] = GetProcAddress(hLib, "EnumResourceLanguagesExA");
        p[336] = GetProcAddress(hLib, "EnumResourceLanguagesExW");
        p[337] = GetProcAddress(hLib, "EnumResourceLanguagesW");
        p[338] = GetProcAddress(hLib, "EnumResourceNamesA");
        p[339] = GetProcAddress(hLib, "EnumResourceNamesExA");
        p[340] = GetProcAddress(hLib, "EnumResourceNamesExW");
        p[341] = GetProcAddress(hLib, "EnumResourceNamesW");
        p[342] = GetProcAddress(hLib, "EnumResourceTypesA");
        p[343] = GetProcAddress(hLib, "EnumResourceTypesExA");
        p[344] = GetProcAddress(hLib, "EnumResourceTypesExW");
        p[345] = GetProcAddress(hLib, "EnumResourceTypesW");
        p[346] = GetProcAddress(hLib, "EnumSystemCodePagesA");
        p[347] = GetProcAddress(hLib, "EnumSystemCodePagesW");
        p[348] = GetProcAddress(hLib, "EnumSystemFirmwareTables");
        p[349] = GetProcAddress(hLib, "EnumSystemGeoID");
        p[350] = GetProcAddress(hLib, "EnumSystemLanguageGroupsA");
        p[351] = GetProcAddress(hLib, "EnumSystemLanguageGroupsW");
        p[352] = GetProcAddress(hLib, "EnumSystemLocalesA");
        p[353] = GetProcAddress(hLib, "EnumSystemLocalesEx");
        p[354] = GetProcAddress(hLib, "EnumSystemLocalesW");
        p[355] = GetProcAddress(hLib, "EnumTimeFormatsA");
        p[356] = GetProcAddress(hLib, "EnumTimeFormatsEx");
        p[357] = GetProcAddress(hLib, "EnumTimeFormatsW");
        p[358] = GetProcAddress(hLib, "EnumUILanguagesA");
        p[359] = GetProcAddress(hLib, "EnumUILanguagesW");
        p[360] = GetProcAddress(hLib, "EnumerateLocalComputerNamesA");
        p[361] = GetProcAddress(hLib, "EnumerateLocalComputerNamesW");
        p[362] = GetProcAddress(hLib, "EnumerateStateAtomValues");
        p[363] = GetProcAddress(hLib, "EnumerateStateContainerItems");
        p[364] = GetProcAddress(hLib, "EraseTape");
        p[365] = GetProcAddress(hLib, "EscapeCommFunction");
        p[366] = GetProcAddress(hLib, "ExitProcess");
        p[367] = GetProcAddress(hLib, "ExitThread"); //Forwarder: NTDLL.RtlExitUserThread
        p[368] = GetProcAddress(hLib, "ExitVDM");
        p[369] = GetProcAddress(hLib, "ExpandEnvironmentStringsA");
        p[370] = GetProcAddress(hLib, "ExpandEnvironmentStringsW");
        p[371] = GetProcAddress(hLib, "ExpungeConsoleCommandHistoryA");
        p[372] = GetProcAddress(hLib, "ExpungeConsoleCommandHistoryW");
        p[373] = GetProcAddress(hLib, "FatalAppExitA");
        p[374] = GetProcAddress(hLib, "FatalAppExitW");
        p[375] = GetProcAddress(hLib, "FatalExit");
        p[376] = GetProcAddress(hLib, "FileTimeToDosDateTime");
        p[377] = GetProcAddress(hLib, "FileTimeToLocalFileTime");
        p[378] = GetProcAddress(hLib, "FileTimeToSystemTime");
        p[379] = GetProcAddress(hLib, "FillConsoleOutputAttribute");
        p[380] = GetProcAddress(hLib, "FillConsoleOutputCharacterA");
        p[381] = GetProcAddress(hLib, "FillConsoleOutputCharacterW");
        p[382] = GetProcAddress(hLib, "FindActCtxSectionGuid");
        p[383] = GetProcAddress(hLib, "FindActCtxSectionGuidWorker");
        p[384] = GetProcAddress(hLib, "FindActCtxSectionStringA");
        p[385] = GetProcAddress(hLib, "FindActCtxSectionStringW");
        p[386] = GetProcAddress(hLib, "FindActCtxSectionStringWWorker");
        p[387] = GetProcAddress(hLib, "FindAtomA");
        p[388] = GetProcAddress(hLib, "FindAtomW");
        p[389] = GetProcAddress(hLib, "FindClose");
        p[390] = GetProcAddress(hLib, "FindCloseChangeNotification");
        p[391] = GetProcAddress(hLib, "FindFirstChangeNotificationA");
        p[392] = GetProcAddress(hLib, "FindFirstChangeNotificationW");
        p[393] = GetProcAddress(hLib, "FindFirstFileA");
        p[394] = GetProcAddress(hLib, "FindFirstFileExA");
        p[395] = GetProcAddress(hLib, "FindFirstFileExW");
        p[396] = GetProcAddress(hLib, "FindFirstFileNameTransactedW");
        p[397] = GetProcAddress(hLib, "FindFirstFileNameW");
        p[398] = GetProcAddress(hLib, "FindFirstFileTransactedA");
        p[399] = GetProcAddress(hLib, "FindFirstFileTransactedW");
        p[400] = GetProcAddress(hLib, "FindFirstFileW");
        p[401] = GetProcAddress(hLib, "FindFirstStreamTransactedW");
        p[402] = GetProcAddress(hLib, "FindFirstStreamW");
        p[403] = GetProcAddress(hLib, "FindFirstVolumeA");
        p[404] = GetProcAddress(hLib, "FindFirstVolumeMountPointA");
        p[405] = GetProcAddress(hLib, "FindFirstVolumeMountPointW");
        p[406] = GetProcAddress(hLib, "FindFirstVolumeW");
        p[407] = GetProcAddress(hLib, "FindNLSString");
        p[408] = GetProcAddress(hLib, "FindNLSStringEx");
        p[409] = GetProcAddress(hLib, "FindNextChangeNotification");
        p[410] = GetProcAddress(hLib, "FindNextFileA");
        p[411] = GetProcAddress(hLib, "FindNextFileNameW");
        p[412] = GetProcAddress(hLib, "FindNextFileW");
        p[413] = GetProcAddress(hLib, "FindNextStreamW");
        p[414] = GetProcAddress(hLib, "FindNextVolumeA");
        p[415] = GetProcAddress(hLib, "FindNextVolumeMountPointA");
        p[416] = GetProcAddress(hLib, "FindNextVolumeMountPointW");
        p[417] = GetProcAddress(hLib, "FindNextVolumeW");
        p[418] = GetProcAddress(hLib, "FindResourceA");
        p[419] = GetProcAddress(hLib, "FindResourceExA");
        p[420] = GetProcAddress(hLib, "FindResourceExW");
        p[421] = GetProcAddress(hLib, "FindResourceW");
        p[422] = GetProcAddress(hLib, "FindStringOrdinal");
        p[423] = GetProcAddress(hLib, "FindVolumeClose");
        p[424] = GetProcAddress(hLib, "FindVolumeMountPointClose");
        p[425] = GetProcAddress(hLib, "FlsAlloc");
        p[426] = GetProcAddress(hLib, "FlsFree");
        p[427] = GetProcAddress(hLib, "FlsGetValue");
        p[428] = GetProcAddress(hLib, "FlsSetValue");
        p[429] = GetProcAddress(hLib, "FlushConsoleInputBuffer");
        p[430] = GetProcAddress(hLib, "FlushFileBuffers");
        p[431] = GetProcAddress(hLib, "FlushInstructionCache");
        p[432] = GetProcAddress(hLib, "FlushProcessWriteBuffers"); //Forwarder: NTDLL.NtFlushProcessWriteBuffers
        p[433] = GetProcAddress(hLib, "FlushViewOfFile");
        p[434] = GetProcAddress(hLib, "FoldStringA");
        p[435] = GetProcAddress(hLib, "FoldStringW");
        p[436] = GetProcAddress(hLib, "FormatMessageA");
        p[437] = GetProcAddress(hLib, "FormatMessageW");
        p[438] = GetProcAddress(hLib, "FreeConsole");
        p[439] = GetProcAddress(hLib, "FreeEnvironmentStringsA");
        p[440] = GetProcAddress(hLib, "FreeEnvironmentStringsW");
        p[441] = GetProcAddress(hLib, "FreeLibrary");
        p[442] = GetProcAddress(hLib, "FreeLibraryAndExitThread");
        p[443] = GetProcAddress(hLib, "FreeLibraryWhenCallbackReturns"); //Forwarder: NTDLL.TpCallbackUnloadDllOnCompletion
        p[444] = GetProcAddress(hLib, "FreeResource");
        p[445] = GetProcAddress(hLib, "FreeUserPhysicalPages");
        p[446] = GetProcAddress(hLib, "GenerateConsoleCtrlEvent");
        p[447] = GetProcAddress(hLib, "GetACP");
        p[448] = GetProcAddress(hLib, "GetActiveProcessorCount");
        p[449] = GetProcAddress(hLib, "GetActiveProcessorGroupCount");
        p[450] = GetProcAddress(hLib, "GetAppContainerAce");
        p[451] = GetProcAddress(hLib, "GetAppContainerNamedObjectPath");
        p[452] = GetProcAddress(hLib, "GetApplicationRecoveryCallback");
        p[453] = GetProcAddress(hLib, "GetApplicationRecoveryCallbackWorker");
        p[454] = GetProcAddress(hLib, "GetApplicationRestartSettings");
        p[455] = GetProcAddress(hLib, "GetApplicationRestartSettingsWorker");
        p[456] = GetProcAddress(hLib, "GetApplicationUserModelId");
        p[457] = GetProcAddress(hLib, "GetAtomNameA");
        p[458] = GetProcAddress(hLib, "GetAtomNameW");
        p[459] = GetProcAddress(hLib, "GetBinaryType");
        p[460] = GetProcAddress(hLib, "GetBinaryTypeA");
        p[461] = GetProcAddress(hLib, "GetBinaryTypeW");
        p[462] = GetProcAddress(hLib, "GetCPInfo");
        p[463] = GetProcAddress(hLib, "GetCPInfoExA");
        p[464] = GetProcAddress(hLib, "GetCPInfoExW");
        p[465] = GetProcAddress(hLib, "GetCachedSigningLevel");
        p[466] = GetProcAddress(hLib, "GetCalendarDateFormat");
        p[467] = GetProcAddress(hLib, "GetCalendarDateFormatEx");
        p[468] = GetProcAddress(hLib, "GetCalendarDaysInMonth");
        p[469] = GetProcAddress(hLib, "GetCalendarDifferenceInDays");
        p[470] = GetProcAddress(hLib, "GetCalendarInfoA");
        p[471] = GetProcAddress(hLib, "GetCalendarInfoEx");
        p[472] = GetProcAddress(hLib, "GetCalendarInfoW");
        p[473] = GetProcAddress(hLib, "GetCalendarMonthsInYear");
        p[474] = GetProcAddress(hLib, "GetCalendarSupportedDateRange");
        p[475] = GetProcAddress(hLib, "GetCalendarWeekNumber");
        p[476] = GetProcAddress(hLib, "GetComPlusPackageInstallStatus");
        p[477] = GetProcAddress(hLib, "GetCommConfig");
        p[478] = GetProcAddress(hLib, "GetCommMask");
        p[479] = GetProcAddress(hLib, "GetCommModemStatus");
        p[480] = GetProcAddress(hLib, "GetCommProperties");
        p[481] = GetProcAddress(hLib, "GetCommState");
        p[482] = GetProcAddress(hLib, "GetCommTimeouts");
        p[483] = GetProcAddress(hLib, "GetCommandLineA");
        p[484] = GetProcAddress(hLib, "GetCommandLineW");
        p[485] = GetProcAddress(hLib, "GetCompressedFileSizeA");
        p[486] = GetProcAddress(hLib, "GetCompressedFileSizeTransactedA");
        p[487] = GetProcAddress(hLib, "GetCompressedFileSizeTransactedW");
        p[488] = GetProcAddress(hLib, "GetCompressedFileSizeW");
        p[489] = GetProcAddress(hLib, "GetComputerNameA");
        p[490] = GetProcAddress(hLib, "GetComputerNameExA");
        p[491] = GetProcAddress(hLib, "GetComputerNameExW");
        p[492] = GetProcAddress(hLib, "GetComputerNameW");
        p[493] = GetProcAddress(hLib, "GetConsoleAliasA");
        p[494] = GetProcAddress(hLib, "GetConsoleAliasExesA");
        p[495] = GetProcAddress(hLib, "GetConsoleAliasExesLengthA");
        p[496] = GetProcAddress(hLib, "GetConsoleAliasExesLengthW");
        p[497] = GetProcAddress(hLib, "GetConsoleAliasExesW");
        p[498] = GetProcAddress(hLib, "GetConsoleAliasW");
        p[499] = GetProcAddress(hLib, "GetConsoleAliasesA");
        p[500] = GetProcAddress(hLib, "GetConsoleAliasesLengthA");
        p[501] = GetProcAddress(hLib, "GetConsoleAliasesLengthW");
        p[502] = GetProcAddress(hLib, "GetConsoleAliasesW");
        p[503] = GetProcAddress(hLib, "GetConsoleCP");
        p[504] = GetProcAddress(hLib, "GetConsoleCharType");
        p[505] = GetProcAddress(hLib, "GetConsoleCommandHistoryA");
        p[506] = GetProcAddress(hLib, "GetConsoleCommandHistoryLengthA");
        p[507] = GetProcAddress(hLib, "GetConsoleCommandHistoryLengthW");
        p[508] = GetProcAddress(hLib, "GetConsoleCommandHistoryW");
        p[509] = GetProcAddress(hLib, "GetConsoleCursorInfo");
        p[510] = GetProcAddress(hLib, "GetConsoleCursorMode");
        p[511] = GetProcAddress(hLib, "GetConsoleDisplayMode");
        p[512] = GetProcAddress(hLib, "GetConsoleFontInfo");
        p[513] = GetProcAddress(hLib, "GetConsoleFontSize");
        p[514] = GetProcAddress(hLib, "GetConsoleHardwareState");
        p[515] = GetProcAddress(hLib, "GetConsoleHistoryInfo");
        p[516] = GetProcAddress(hLib, "GetConsoleInputExeNameA"); //Forwarder: kernelbase.GetConsoleInputExeNameA
        p[517] = GetProcAddress(hLib, "GetConsoleInputExeNameW"); //Forwarder: kernelbase.GetConsoleInputExeNameW
        p[518] = GetProcAddress(hLib, "GetConsoleInputWaitHandle");
        p[519] = GetProcAddress(hLib, "GetConsoleKeyboardLayoutNameA");
        p[520] = GetProcAddress(hLib, "GetConsoleKeyboardLayoutNameW");
        p[521] = GetProcAddress(hLib, "GetConsoleMode");
        p[522] = GetProcAddress(hLib, "GetConsoleNlsMode");
        p[523] = GetProcAddress(hLib, "GetConsoleOriginalTitleA");
        p[524] = GetProcAddress(hLib, "GetConsoleOriginalTitleW");
        p[525] = GetProcAddress(hLib, "GetConsoleOutputCP");
        p[526] = GetProcAddress(hLib, "GetConsoleProcessList");
        p[527] = GetProcAddress(hLib, "GetConsoleScreenBufferInfo");
        p[528] = GetProcAddress(hLib, "GetConsoleScreenBufferInfoEx");
        p[529] = GetProcAddress(hLib, "GetConsoleSelectionInfo");
        p[530] = GetProcAddress(hLib, "GetConsoleTitleA");
        p[531] = GetProcAddress(hLib, "GetConsoleTitleW");
        p[532] = GetProcAddress(hLib, "GetConsoleWindow");
        p[533] = GetProcAddress(hLib, "GetCurrencyFormatA");
        p[534] = GetProcAddress(hLib, "GetCurrencyFormatEx");
        p[535] = GetProcAddress(hLib, "GetCurrencyFormatW");
        p[536] = GetProcAddress(hLib, "GetCurrentActCtx");
        p[537] = GetProcAddress(hLib, "GetCurrentActCtxWorker");
        p[538] = GetProcAddress(hLib, "GetCurrentApplicationUserModelId");
        p[539] = GetProcAddress(hLib, "GetCurrentConsoleFont");
        p[540] = GetProcAddress(hLib, "GetCurrentConsoleFontEx");
        p[541] = GetProcAddress(hLib, "GetCurrentDirectoryA");
        p[542] = GetProcAddress(hLib, "GetCurrentDirectoryW");
        p[543] = GetProcAddress(hLib, "GetCurrentPackageFamilyName");
        p[544] = GetProcAddress(hLib, "GetCurrentPackageFullName");
        p[545] = GetProcAddress(hLib, "GetCurrentPackageId");
        p[546] = GetProcAddress(hLib, "GetCurrentPackageInfo");
        p[547] = GetProcAddress(hLib, "GetCurrentPackagePath");
        p[548] = GetProcAddress(hLib, "GetCurrentProcess");
        p[549] = GetProcAddress(hLib, "GetCurrentProcessId");
        p[550] = GetProcAddress(hLib, "GetCurrentProcessorNumber"); //Forwarder: NTDLL.RtlGetCurrentProcessorNumber
        p[551] = GetProcAddress(hLib, "GetCurrentProcessorNumberEx"); //Forwarder: NTDLL.RtlGetCurrentProcessorNumberEx
        p[552] = GetProcAddress(hLib, "GetCurrentThread");
        p[553] = GetProcAddress(hLib, "GetCurrentThreadId");
        p[554] = GetProcAddress(hLib, "GetCurrentThreadStackLimits"); //Forwarder: api-ms-win-core-processthreads-l1-1-0.GetCurrentThreadStackLimits
        p[555] = GetProcAddress(hLib, "GetDateFormatA");
        p[556] = GetProcAddress(hLib, "GetDateFormatAWorker");
        p[557] = GetProcAddress(hLib, "GetDateFormatEx");
        p[558] = GetProcAddress(hLib, "GetDateFormatW");
        p[559] = GetProcAddress(hLib, "GetDateFormatWWorker");
        p[560] = GetProcAddress(hLib, "GetDefaultCommConfigA");
        p[561] = GetProcAddress(hLib, "GetDefaultCommConfigW");
        p[562] = GetProcAddress(hLib, "GetDevicePowerState");
        p[563] = GetProcAddress(hLib, "GetDiskFreeSpaceA");
        p[564] = GetProcAddress(hLib, "GetDiskFreeSpaceExA");
        p[565] = GetProcAddress(hLib, "GetDiskFreeSpaceExW");
        p[566] = GetProcAddress(hLib, "GetDiskFreeSpaceW");
        p[567] = GetProcAddress(hLib, "GetDllDirectoryA");
        p[568] = GetProcAddress(hLib, "GetDllDirectoryW");
        p[569] = GetProcAddress(hLib, "GetDriveTypeA");
        p[570] = GetProcAddress(hLib, "GetDriveTypeW");
        p[571] = GetProcAddress(hLib, "GetDurationFormat");
        p[572] = GetProcAddress(hLib, "GetDurationFormatEx");
        p[573] = GetProcAddress(hLib, "GetDynamicTimeZoneInformation");
        p[574] = GetProcAddress(hLib, "GetEnabledXStateFeatures");
        p[575] = GetProcAddress(hLib, "GetEnvironmentStrings");
        p[576] = GetProcAddress(hLib, "GetEnvironmentStringsA");
        p[577] = GetProcAddress(hLib, "GetEnvironmentStringsW");
        p[578] = GetProcAddress(hLib, "GetEnvironmentVariableA");
        p[579] = GetProcAddress(hLib, "GetEnvironmentVariableW");
        p[580] = GetProcAddress(hLib, "GetEraNameCountedString");
        p[581] = GetProcAddress(hLib, "GetErrorMode");
        p[582] = GetProcAddress(hLib, "GetExitCodeProcess");
        p[583] = GetProcAddress(hLib, "GetExitCodeThread");
        p[584] = GetProcAddress(hLib, "GetExpandedNameA");
        p[585] = GetProcAddress(hLib, "GetExpandedNameW");
        p[586] = GetProcAddress(hLib, "GetFileAttributesA");
        p[587] = GetProcAddress(hLib, "GetFileAttributesExA");
        p[588] = GetProcAddress(hLib, "GetFileAttributesExW");
        p[589] = GetProcAddress(hLib, "GetFileAttributesTransactedA");
        p[590] = GetProcAddress(hLib, "GetFileAttributesTransactedW");
        p[591] = GetProcAddress(hLib, "GetFileAttributesW");
        p[592] = GetProcAddress(hLib, "GetFileBandwidthReservation");
        p[593] = GetProcAddress(hLib, "GetFileInformationByHandle");
        p[594] = GetProcAddress(hLib, "GetFileInformationByHandleEx");
        p[595] = GetProcAddress(hLib, "GetFileMUIInfo");
        p[596] = GetProcAddress(hLib, "GetFileMUIPath");
        p[597] = GetProcAddress(hLib, "GetFileSize");
        p[598] = GetProcAddress(hLib, "GetFileSizeEx");
        p[599] = GetProcAddress(hLib, "GetFileTime");
        p[600] = GetProcAddress(hLib, "GetFileType");
        p[601] = GetProcAddress(hLib, "GetFinalPathNameByHandleA");
        p[602] = GetProcAddress(hLib, "GetFinalPathNameByHandleW");
        p[603] = GetProcAddress(hLib, "GetFirmwareEnvironmentVariableA");
        p[604] = GetProcAddress(hLib, "GetFirmwareEnvironmentVariableExA");
        p[605] = GetProcAddress(hLib, "GetFirmwareEnvironmentVariableExW");
        p[606] = GetProcAddress(hLib, "GetFirmwareEnvironmentVariableW");
        p[607] = GetProcAddress(hLib, "GetFirmwareType");
        p[608] = GetProcAddress(hLib, "GetFullPathNameA");
        p[609] = GetProcAddress(hLib, "GetFullPathNameTransactedA");
        p[610] = GetProcAddress(hLib, "GetFullPathNameTransactedW");
        p[611] = GetProcAddress(hLib, "GetFullPathNameW");
        p[612] = GetProcAddress(hLib, "GetGeoInfoA");
        p[613] = GetProcAddress(hLib, "GetGeoInfoW");
        p[614] = GetProcAddress(hLib, "GetHandleContext");
        p[615] = GetProcAddress(hLib, "GetHandleInformation");
        p[616] = GetProcAddress(hLib, "GetHivePath");
        p[617] = GetProcAddress(hLib, "GetLargePageMinimum");
        p[618] = GetProcAddress(hLib, "GetLargestConsoleWindowSize");
        p[619] = GetProcAddress(hLib, "GetLastError");
        p[620] = GetProcAddress(hLib, "GetLocalTime");
        p[621] = GetProcAddress(hLib, "GetLocaleInfoA");
        p[622] = GetProcAddress(hLib, "GetLocaleInfoEx");
        p[623] = GetProcAddress(hLib, "GetLocaleInfoW");
        p[624] = GetProcAddress(hLib, "GetLogicalDriveStringsA");
        p[625] = GetProcAddress(hLib, "GetLogicalDriveStringsW");
        p[626] = GetProcAddress(hLib, "GetLogicalDrives");
        p[627] = GetProcAddress(hLib, "GetLogicalProcessorInformation");
        p[628] = GetProcAddress(hLib, "GetLogicalProcessorInformationEx"); //Forwarder: api-ms-win-core-sysinfo-l1-1-0.GetLogicalProcessorInformationEx
        p[629] = GetProcAddress(hLib, "GetLongPathNameA");
        p[630] = GetProcAddress(hLib, "GetLongPathNameTransactedA");
        p[631] = GetProcAddress(hLib, "GetLongPathNameTransactedW");
        p[632] = GetProcAddress(hLib, "GetLongPathNameW");
        p[633] = GetProcAddress(hLib, "GetMailslotInfo");
        p[634] = GetProcAddress(hLib, "GetMaximumProcessorCount");
        p[635] = GetProcAddress(hLib, "GetMaximumProcessorGroupCount");
        p[636] = GetProcAddress(hLib, "GetMemoryErrorHandlingCapabilities");
        p[637] = GetProcAddress(hLib, "GetModuleFileNameA");
        p[638] = GetProcAddress(hLib, "GetModuleFileNameW");
        p[639] = GetProcAddress(hLib, "GetModuleHandleA");
        p[640] = GetProcAddress(hLib, "GetModuleHandleExA");
        p[641] = GetProcAddress(hLib, "GetModuleHandleExW");
        p[642] = GetProcAddress(hLib, "GetModuleHandleW");
        p[643] = GetProcAddress(hLib, "GetNLSVersion");
        p[644] = GetProcAddress(hLib, "GetNLSVersionEx");
        p[645] = GetProcAddress(hLib, "GetNamedPipeAttribute");
        p[646] = GetProcAddress(hLib, "GetNamedPipeClientComputerNameA");
        p[647] = GetProcAddress(hLib, "GetNamedPipeClientComputerNameW");
        p[648] = GetProcAddress(hLib, "GetNamedPipeClientProcessId");
        p[649] = GetProcAddress(hLib, "GetNamedPipeClientSessionId");
        p[650] = GetProcAddress(hLib, "GetNamedPipeHandleStateA");
        p[651] = GetProcAddress(hLib, "GetNamedPipeHandleStateW");
        p[652] = GetProcAddress(hLib, "GetNamedPipeInfo");
        p[653] = GetProcAddress(hLib, "GetNamedPipeServerProcessId");
        p[654] = GetProcAddress(hLib, "GetNamedPipeServerSessionId");
        p[655] = GetProcAddress(hLib, "GetNativeSystemInfo");
        p[656] = GetProcAddress(hLib, "GetNextVDMCommand");
        p[657] = GetProcAddress(hLib, "GetNumaAvailableMemoryNode");
        p[658] = GetProcAddress(hLib, "GetNumaAvailableMemoryNodeEx");
        p[659] = GetProcAddress(hLib, "GetNumaHighestNodeNumber");
        p[660] = GetProcAddress(hLib, "GetNumaNodeNumberFromHandle");
        p[661] = GetProcAddress(hLib, "GetNumaNodeProcessorMask");
        p[662] = GetProcAddress(hLib, "GetNumaNodeProcessorMaskEx");
        p[663] = GetProcAddress(hLib, "GetNumaProcessorNode");
        p[664] = GetProcAddress(hLib, "GetNumaProcessorNodeEx");
        p[665] = GetProcAddress(hLib, "GetNumaProximityNode");
        p[666] = GetProcAddress(hLib, "GetNumaProximityNodeEx");
        p[667] = GetProcAddress(hLib, "GetNumberFormatA");
        p[668] = GetProcAddress(hLib, "GetNumberFormatEx");
        p[669] = GetProcAddress(hLib, "GetNumberFormatW");
        p[670] = GetProcAddress(hLib, "GetNumberOfConsoleFonts");
        p[671] = GetProcAddress(hLib, "GetNumberOfConsoleInputEvents");
        p[672] = GetProcAddress(hLib, "GetNumberOfConsoleMouseButtons");
        p[673] = GetProcAddress(hLib, "GetOEMCP");
        p[674] = GetProcAddress(hLib, "GetOverlappedResult");
        p[675] = GetProcAddress(hLib, "GetOverlappedResultEx"); //Forwarder: api-ms-win-core-io-l1-1-1.GetOverlappedResultEx
        p[676] = GetProcAddress(hLib, "GetPackageFamilyName");
        p[677] = GetProcAddress(hLib, "GetPackageFullName");
        p[678] = GetProcAddress(hLib, "GetPackageId");
        p[679] = GetProcAddress(hLib, "GetPackageInfo");
        p[680] = GetProcAddress(hLib, "GetPackagePath");
        p[681] = GetProcAddress(hLib, "GetPackagesByPackageFamily");
        p[682] = GetProcAddress(hLib, "GetPhysicallyInstalledSystemMemory");
        p[683] = GetProcAddress(hLib, "GetPriorityClass");
        p[684] = GetProcAddress(hLib, "GetPrivateProfileIntA");
        p[685] = GetProcAddress(hLib, "GetPrivateProfileIntW");
        p[686] = GetProcAddress(hLib, "GetPrivateProfileSectionA");
        p[687] = GetProcAddress(hLib, "GetPrivateProfileSectionNamesA");
        p[688] = GetProcAddress(hLib, "GetPrivateProfileSectionNamesW");
        p[689] = GetProcAddress(hLib, "GetPrivateProfileSectionW");
        p[690] = GetProcAddress(hLib, "GetPrivateProfileStringA");
        p[691] = GetProcAddress(hLib, "GetPrivateProfileStringW");
        p[692] = GetProcAddress(hLib, "GetPrivateProfileStructA");
        p[693] = GetProcAddress(hLib, "GetPrivateProfileStructW");
        p[694] = GetProcAddress(hLib, "GetProcAddress");
        p[695] = GetProcAddress(hLib, "GetProcessAffinityMask");
        p[696] = GetProcAddress(hLib, "GetProcessDEPPolicy");
        p[697] = GetProcAddress(hLib, "GetProcessGroupAffinity");
        p[698] = GetProcAddress(hLib, "GetProcessHandleCount");
        p[699] = GetProcAddress(hLib, "GetProcessHeap");
        p[700] = GetProcAddress(hLib, "GetProcessHeaps");
        p[701] = GetProcAddress(hLib, "GetProcessId");
        p[702] = GetProcAddress(hLib, "GetProcessIdOfThread");
        p[703] = GetProcAddress(hLib, "GetProcessInformation");
        p[704] = GetProcAddress(hLib, "GetProcessIoCounters");
        p[705] = GetProcAddress(hLib, "GetProcessMitigationPolicy"); //Forwarder: api-ms-win-core-processthreads-l1-1-1.GetProcessMitigationPolicy
        p[706] = GetProcAddress(hLib, "GetProcessPreferredUILanguages");
        p[707] = GetProcAddress(hLib, "GetProcessPriorityBoost");
        p[708] = GetProcAddress(hLib, "GetProcessShutdownParameters");
        p[709] = GetProcAddress(hLib, "GetProcessTimes");
        p[710] = GetProcAddress(hLib, "GetProcessVersion");
        p[711] = GetProcAddress(hLib, "GetProcessWorkingSetSize");
        p[712] = GetProcAddress(hLib, "GetProcessWorkingSetSizeEx");
        p[713] = GetProcAddress(hLib, "GetProcessorSystemCycleTime");
        p[714] = GetProcAddress(hLib, "GetProductInfo");
        p[715] = GetProcAddress(hLib, "GetProfileIntA");
        p[716] = GetProcAddress(hLib, "GetProfileIntW");
        p[717] = GetProcAddress(hLib, "GetProfileSectionA");
        p[718] = GetProcAddress(hLib, "GetProfileSectionW");
        p[719] = GetProcAddress(hLib, "GetProfileStringA");
        p[720] = GetProcAddress(hLib, "GetProfileStringW");
        p[721] = GetProcAddress(hLib, "GetQueuedCompletionStatus");
        p[722] = GetProcAddress(hLib, "GetQueuedCompletionStatusEx");
        p[723] = GetProcAddress(hLib, "GetRoamingLastObservedChangeTime");
        p[724] = GetProcAddress(hLib, "GetSerializedAtomBytes");
        p[725] = GetProcAddress(hLib, "GetShortPathNameA");
        p[726] = GetProcAddress(hLib, "GetShortPathNameW");
        p[727] = GetProcAddress(hLib, "GetStartupInfoA");
        p[728] = GetProcAddress(hLib, "GetStartupInfoW");
        p[729] = GetProcAddress(hLib, "GetStateContainerDepth");
        p[730] = GetProcAddress(hLib, "GetStateFolder");
        p[731] = GetProcAddress(hLib, "GetStateRootFolder");
        p[732] = GetProcAddress(hLib, "GetStateSettingsFolder");
        p[733] = GetProcAddress(hLib, "GetStateVersion");
        p[734] = GetProcAddress(hLib, "GetStdHandle");
        p[735] = GetProcAddress(hLib, "GetStringScripts");
        p[736] = GetProcAddress(hLib, "GetStringTypeA");
        p[737] = GetProcAddress(hLib, "GetStringTypeExA");
        p[738] = GetProcAddress(hLib, "GetStringTypeExW");
        p[739] = GetProcAddress(hLib, "GetStringTypeW");
        p[740] = GetProcAddress(hLib, "GetSystemAppDataFolder");
        p[741] = GetProcAddress(hLib, "GetSystemAppDataKey");
        p[742] = GetProcAddress(hLib, "GetSystemDEPPolicy");
        p[743] = GetProcAddress(hLib, "GetSystemDefaultLCID");
        p[744] = GetProcAddress(hLib, "GetSystemDefaultLangID");
        p[745] = GetProcAddress(hLib, "GetSystemDefaultLocaleName");
        p[746] = GetProcAddress(hLib, "GetSystemDefaultUILanguage");
        p[747] = GetProcAddress(hLib, "GetSystemDirectoryA");
        p[748] = GetProcAddress(hLib, "GetSystemDirectoryW");
        p[749] = GetProcAddress(hLib, "GetSystemFileCacheSize");
        p[750] = GetProcAddress(hLib, "GetSystemFirmwareTable");
        p[751] = GetProcAddress(hLib, "GetSystemInfo");
        p[752] = GetProcAddress(hLib, "GetSystemPowerStatus");
        p[753] = GetProcAddress(hLib, "GetSystemPreferredUILanguages");
        p[754] = GetProcAddress(hLib, "GetSystemRegistryQuota");
        p[755] = GetProcAddress(hLib, "GetSystemTime");
        p[756] = GetProcAddress(hLib, "GetSystemTimeAdjustment");
        p[757] = GetProcAddress(hLib, "GetSystemTimeAsFileTime");
        p[758] = GetProcAddress(hLib, "GetSystemTimePreciseAsFileTime");
        p[759] = GetProcAddress(hLib, "GetSystemTimes");
        p[760] = GetProcAddress(hLib, "GetSystemWindowsDirectoryA");
        p[761] = GetProcAddress(hLib, "GetSystemWindowsDirectoryW");
        p[762] = GetProcAddress(hLib, "GetSystemWow64DirectoryA");
        p[763] = GetProcAddress(hLib, "GetSystemWow64DirectoryW");
        p[764] = GetProcAddress(hLib, "GetTapeParameters");
        p[765] = GetProcAddress(hLib, "GetTapePosition");
        p[766] = GetProcAddress(hLib, "GetTapeStatus");
        p[767] = GetProcAddress(hLib, "GetTempFileNameA");
        p[768] = GetProcAddress(hLib, "GetTempFileNameW");
        p[769] = GetProcAddress(hLib, "GetTempPathA");
        p[770] = GetProcAddress(hLib, "GetTempPathW");
        p[771] = GetProcAddress(hLib, "GetThreadContext");
        p[772] = GetProcAddress(hLib, "GetThreadErrorMode");
        p[773] = GetProcAddress(hLib, "GetThreadGroupAffinity");
        p[774] = GetProcAddress(hLib, "GetThreadIOPendingFlag");
        p[775] = GetProcAddress(hLib, "GetThreadId");
        p[776] = GetProcAddress(hLib, "GetThreadIdealProcessorEx");
        p[777] = GetProcAddress(hLib, "GetThreadInformation");
        p[778] = GetProcAddress(hLib, "GetThreadLocale");
        p[779] = GetProcAddress(hLib, "GetThreadPreferredUILanguages");
        p[780] = GetProcAddress(hLib, "GetThreadPriority");
        p[781] = GetProcAddress(hLib, "GetThreadPriorityBoost");
        p[782] = GetProcAddress(hLib, "GetThreadSelectorEntry");
        p[783] = GetProcAddress(hLib, "GetThreadTimes");
        p[784] = GetProcAddress(hLib, "GetThreadUILanguage");
        p[785] = GetProcAddress(hLib, "GetTickCount64");
        p[786] = GetProcAddress(hLib, "GetTickCount");
        p[787] = GetProcAddress(hLib, "GetTimeFormatA");
        p[788] = GetProcAddress(hLib, "GetTimeFormatAWorker");
        p[789] = GetProcAddress(hLib, "GetTimeFormatEx");
        p[790] = GetProcAddress(hLib, "GetTimeFormatW");
        p[791] = GetProcAddress(hLib, "GetTimeFormatWWorker");
        p[792] = GetProcAddress(hLib, "GetTimeZoneInformation");
        p[793] = GetProcAddress(hLib, "GetTimeZoneInformationForYear");
        p[794] = GetProcAddress(hLib, "GetUILanguageInfo");
        p[795] = GetProcAddress(hLib, "GetUserDefaultLCID");
        p[796] = GetProcAddress(hLib, "GetUserDefaultLangID");
        p[797] = GetProcAddress(hLib, "GetUserDefaultLocaleName");
        p[798] = GetProcAddress(hLib, "GetUserDefaultUILanguage");
        p[799] = GetProcAddress(hLib, "GetUserGeoID");
        p[800] = GetProcAddress(hLib, "GetUserPreferredUILanguages");
        p[801] = GetProcAddress(hLib, "GetVDMCurrentDirectories");
        p[802] = GetProcAddress(hLib, "GetVersion");
        p[803] = GetProcAddress(hLib, "GetVersionExA");
        p[804] = GetProcAddress(hLib, "GetVersionExW");
        p[805] = GetProcAddress(hLib, "GetVolumeInformationA");
        p[806] = GetProcAddress(hLib, "GetVolumeInformationByHandleW");
        p[807] = GetProcAddress(hLib, "GetVolumeInformationW");
        p[808] = GetProcAddress(hLib, "GetVolumeNameForVolumeMountPointA");
        p[809] = GetProcAddress(hLib, "GetVolumeNameForVolumeMountPointW");
        p[810] = GetProcAddress(hLib, "GetVolumePathNameA");
        p[811] = GetProcAddress(hLib, "GetVolumePathNameW");
        p[812] = GetProcAddress(hLib, "GetVolumePathNamesForVolumeNameA");
        p[813] = GetProcAddress(hLib, "GetVolumePathNamesForVolumeNameW");
        p[814] = GetProcAddress(hLib, "GetWindowsDirectoryA");
        p[815] = GetProcAddress(hLib, "GetWindowsDirectoryW");
        p[816] = GetProcAddress(hLib, "GetWriteWatch");
        p[817] = GetProcAddress(hLib, "GetXStateFeaturesMask");
        p[818] = GetProcAddress(hLib, "GlobalAddAtomA");
        p[819] = GetProcAddress(hLib, "GlobalAddAtomExA");
        p[820] = GetProcAddress(hLib, "GlobalAddAtomExW");
        p[821] = GetProcAddress(hLib, "GlobalAddAtomW");
        p[822] = GetProcAddress(hLib, "GlobalAlloc");
        p[823] = GetProcAddress(hLib, "GlobalCompact");
        p[824] = GetProcAddress(hLib, "GlobalDeleteAtom");
        p[825] = GetProcAddress(hLib, "GlobalFindAtomA");
        p[826] = GetProcAddress(hLib, "GlobalFindAtomW");
        p[827] = GetProcAddress(hLib, "GlobalFix");
        p[828] = GetProcAddress(hLib, "GlobalFlags");
        p[829] = GetProcAddress(hLib, "GlobalFree");
        p[830] = GetProcAddress(hLib, "GlobalGetAtomNameA");
        p[831] = GetProcAddress(hLib, "GlobalGetAtomNameW");
        p[832] = GetProcAddress(hLib, "GlobalHandle");
        p[833] = GetProcAddress(hLib, "GlobalLock");
        p[834] = GetProcAddress(hLib, "GlobalMemoryStatus");
        p[835] = GetProcAddress(hLib, "GlobalMemoryStatusEx");
        p[836] = GetProcAddress(hLib, "GlobalReAlloc");
        p[837] = GetProcAddress(hLib, "GlobalSize");
        p[838] = GetProcAddress(hLib, "GlobalUnWire");
        p[839] = GetProcAddress(hLib, "GlobalUnfix");
        p[840] = GetProcAddress(hLib, "GlobalUnlock");
        p[841] = GetProcAddress(hLib, "GlobalWire");
        p[842] = GetProcAddress(hLib, "Heap32First");
        p[843] = GetProcAddress(hLib, "Heap32ListFirst");
        p[844] = GetProcAddress(hLib, "Heap32ListNext");
        p[845] = GetProcAddress(hLib, "Heap32Next");
        p[846] = GetProcAddress(hLib, "HeapAlloc"); //Forwarder: NTDLL.RtlAllocateHeap
        p[847] = GetProcAddress(hLib, "HeapCompact");
        p[848] = GetProcAddress(hLib, "HeapCreate");
        p[849] = GetProcAddress(hLib, "HeapDestroy");
        p[850] = GetProcAddress(hLib, "HeapFree");
        p[851] = GetProcAddress(hLib, "HeapLock");
        p[852] = GetProcAddress(hLib, "HeapQueryInformation");
        p[853] = GetProcAddress(hLib, "HeapReAlloc"); //Forwarder: NTDLL.RtlReAllocateHeap
        p[854] = GetProcAddress(hLib, "HeapSetInformation");
        p[855] = GetProcAddress(hLib, "HeapSize"); //Forwarder: NTDLL.RtlSizeHeap
        p[856] = GetProcAddress(hLib, "HeapSummary");
        p[857] = GetProcAddress(hLib, "HeapUnlock");
        p[858] = GetProcAddress(hLib, "HeapValidate");
        p[859] = GetProcAddress(hLib, "HeapWalk");
        p[860] = GetProcAddress(hLib, "IdnToAscii");
        p[861] = GetProcAddress(hLib, "IdnToNameprepUnicode");
        p[862] = GetProcAddress(hLib, "IdnToUnicode");
        p[863] = GetProcAddress(hLib, "InitAtomTable");
        p[864] = GetProcAddress(hLib, "InitOnceBeginInitialize"); //Forwarder: api-ms-win-core-synch-l1-1-0.InitOnceBeginInitialize
        p[865] = GetProcAddress(hLib, "InitOnceComplete"); //Forwarder: api-ms-win-core-synch-l1-1-0.InitOnceComplete
        p[866] = GetProcAddress(hLib, "InitOnceExecuteOnce"); //Forwarder: api-ms-win-core-synch-l1-1-0.InitOnceExecuteOnce
        p[867] = GetProcAddress(hLib, "InitOnceInitialize"); //Forwarder: NTDLL.RtlRunOnceInitialize
        p[868] = GetProcAddress(hLib, "InitializeConditionVariable"); //Forwarder: NTDLL.RtlInitializeConditionVariable
        p[869] = GetProcAddress(hLib, "InitializeContext");
        p[870] = GetProcAddress(hLib, "InitializeCriticalSection"); //Forwarder: NTDLL.RtlInitializeCriticalSection
        p[871] = GetProcAddress(hLib, "InitializeCriticalSectionAndSpinCount");
        p[872] = GetProcAddress(hLib, "InitializeCriticalSectionEx");
        p[873] = GetProcAddress(hLib, "InitializeProcThreadAttributeList"); //Forwarder: api-ms-win-core-processthreads-l1-1-0.InitializeProcThreadAttributeList
        p[874] = GetProcAddress(hLib, "InitializeSListHead"); //Forwarder: NTDLL.RtlInitializeSListHead
        p[875] = GetProcAddress(hLib, "InitializeSRWLock"); //Forwarder: NTDLL.RtlInitializeSRWLock
        p[876] = GetProcAddress(hLib, "InterlockedCompareExchange64"); //Forwarder: NTDLL.RtlInterlockedCompareExchange64
        p[877] = GetProcAddress(hLib, "InterlockedCompareExchange");
        p[878] = GetProcAddress(hLib, "InterlockedDecrement");
        p[879] = GetProcAddress(hLib, "InterlockedExchange");
        p[880] = GetProcAddress(hLib, "InterlockedExchangeAdd");
        p[881] = GetProcAddress(hLib, "InterlockedFlushSList"); //Forwarder: NTDLL.RtlInterlockedFlushSList
        p[882] = GetProcAddress(hLib, "InterlockedIncrement");
        p[883] = GetProcAddress(hLib, "InterlockedPopEntrySList"); //Forwarder: NTDLL.RtlInterlockedPopEntrySList
        p[884] = GetProcAddress(hLib, "InterlockedPushEntrySList"); //Forwarder: NTDLL.RtlInterlockedPushEntrySList
        p[885] = GetProcAddress(hLib, "InterlockedPushListSListEx"); //Forwarder: NTDLL.RtlInterlockedPushListSListEx
        p[886] = GetProcAddress(hLib, "InvalidateConsoleDIBits");
        p[887] = GetProcAddress(hLib, "IsBadCodePtr");
        p[888] = GetProcAddress(hLib, "IsBadHugeReadPtr");
        p[889] = GetProcAddress(hLib, "IsBadHugeWritePtr");
        p[890] = GetProcAddress(hLib, "IsBadReadPtr");
        p[891] = GetProcAddress(hLib, "IsBadStringPtrA");
        p[892] = GetProcAddress(hLib, "IsBadStringPtrW");
        p[893] = GetProcAddress(hLib, "IsBadWritePtr");
        p[894] = GetProcAddress(hLib, "IsCalendarLeapDay");
        p[895] = GetProcAddress(hLib, "IsCalendarLeapMonth");
        p[896] = GetProcAddress(hLib, "IsCalendarLeapYear");
        p[897] = GetProcAddress(hLib, "IsDBCSLeadByte");
        p[898] = GetProcAddress(hLib, "IsDBCSLeadByteEx");
        p[899] = GetProcAddress(hLib, "IsDebuggerPresent");
        p[900] = GetProcAddress(hLib, "IsNLSDefinedString");
        p[901] = GetProcAddress(hLib, "IsNativeVhdBoot");
        p[902] = GetProcAddress(hLib, "IsNormalizedString");
        p[903] = GetProcAddress(hLib, "IsProcessInJob");
        p[904] = GetProcAddress(hLib, "IsProcessorFeaturePresent");
        p[905] = GetProcAddress(hLib, "IsSystemResumeAutomatic");
        p[906] = GetProcAddress(hLib, "IsThreadAFiber");
        p[907] = GetProcAddress(hLib, "IsThreadpoolTimerSet"); //Forwarder: NTDLL.TpIsTimerSet
        p[908] = GetProcAddress(hLib, "IsValidCalDateTime");
        p[909] = GetProcAddress(hLib, "IsValidCodePage");
        p[910] = GetProcAddress(hLib, "IsValidLanguageGroup");
        p[911] = GetProcAddress(hLib, "IsValidLocale");
        p[912] = GetProcAddress(hLib, "IsValidLocaleName");
        p[913] = GetProcAddress(hLib, "IsValidNLSVersion");
        p[914] = GetProcAddress(hLib, "IsWow64Process");
        p[915] = GetProcAddress(hLib, "K32EmptyWorkingSet");
        p[916] = GetProcAddress(hLib, "K32EnumDeviceDrivers");
        p[917] = GetProcAddress(hLib, "K32EnumPageFilesA");
        p[918] = GetProcAddress(hLib, "K32EnumPageFilesW");
        p[919] = GetProcAddress(hLib, "K32EnumProcessModules");
        p[920] = GetProcAddress(hLib, "K32EnumProcessModulesEx");
        p[921] = GetProcAddress(hLib, "K32EnumProcesses");
        p[922] = GetProcAddress(hLib, "K32GetDeviceDriverBaseNameA");
        p[923] = GetProcAddress(hLib, "K32GetDeviceDriverBaseNameW");
        p[924] = GetProcAddress(hLib, "K32GetDeviceDriverFileNameA");
        p[925] = GetProcAddress(hLib, "K32GetDeviceDriverFileNameW");
        p[926] = GetProcAddress(hLib, "K32GetMappedFileNameA");
        p[927] = GetProcAddress(hLib, "K32GetMappedFileNameW");
        p[928] = GetProcAddress(hLib, "K32GetModuleBaseNameA");
        p[929] = GetProcAddress(hLib, "K32GetModuleBaseNameW");
        p[930] = GetProcAddress(hLib, "K32GetModuleFileNameExA");
        p[931] = GetProcAddress(hLib, "K32GetModuleFileNameExW");
        p[932] = GetProcAddress(hLib, "K32GetModuleInformation");
        p[933] = GetProcAddress(hLib, "K32GetPerformanceInfo");
        p[934] = GetProcAddress(hLib, "K32GetProcessImageFileNameA");
        p[935] = GetProcAddress(hLib, "K32GetProcessImageFileNameW");
        p[936] = GetProcAddress(hLib, "K32GetProcessMemoryInfo");
        p[937] = GetProcAddress(hLib, "K32GetWsChanges");
        p[938] = GetProcAddress(hLib, "K32GetWsChangesEx");
        p[939] = GetProcAddress(hLib, "K32InitializeProcessForWsWatch");
        p[940] = GetProcAddress(hLib, "K32QueryWorkingSet");
        p[941] = GetProcAddress(hLib, "K32QueryWorkingSetEx");
        p[942] = GetProcAddress(hLib, "LCIDToLocaleName");
        p[943] = GetProcAddress(hLib, "LCMapStringA");
        p[944] = GetProcAddress(hLib, "LCMapStringEx");
        p[945] = GetProcAddress(hLib, "LCMapStringW");
        p[946] = GetProcAddress(hLib, "LZClose");
        p[947] = GetProcAddress(hLib, "LZCloseFile");
        p[948] = GetProcAddress(hLib, "LZCopy");
        p[949] = GetProcAddress(hLib, "LZCreateFileW");
        p[950] = GetProcAddress(hLib, "LZDone");
        p[951] = GetProcAddress(hLib, "LZInit");
        p[952] = GetProcAddress(hLib, "LZOpenFileA");
        p[953] = GetProcAddress(hLib, "LZOpenFileW");
        p[954] = GetProcAddress(hLib, "LZRead");
        p[955] = GetProcAddress(hLib, "LZSeek");
        p[956] = GetProcAddress(hLib, "LZStart");
        p[957] = GetProcAddress(hLib, "LeaveCriticalSection"); //Forwarder: NTDLL.RtlLeaveCriticalSection
        p[958] = GetProcAddress(hLib, "LeaveCriticalSectionWhenCallbackReturns"); //Forwarder: NTDLL.TpCallbackLeaveCriticalSectionOnCompletion
        p[959] = GetProcAddress(hLib, "LoadAppInitDlls");
        p[960] = GetProcAddress(hLib, "LoadLibraryA");
        p[961] = GetProcAddress(hLib, "LoadLibraryExA");
        p[962] = GetProcAddress(hLib, "LoadLibraryExW");
        p[963] = GetProcAddress(hLib, "LoadLibraryW");
        p[964] = GetProcAddress(hLib, "LoadModule");
        p[965] = GetProcAddress(hLib, "LoadPackagedLibrary");
        p[966] = GetProcAddress(hLib, "LoadResource");
        p[967] = GetProcAddress(hLib, "LoadStringBaseExW");
        p[968] = GetProcAddress(hLib, "LoadStringBaseW");
        p[969] = GetProcAddress(hLib, "LocalAlloc");
        p[970] = GetProcAddress(hLib, "LocalCompact");
        p[971] = GetProcAddress(hLib, "LocalFileTimeToFileTime");
        p[972] = GetProcAddress(hLib, "LocalFlags");
        p[973] = GetProcAddress(hLib, "LocalFree");
        p[974] = GetProcAddress(hLib, "LocalHandle");
        p[975] = GetProcAddress(hLib, "LocalLock");
        p[976] = GetProcAddress(hLib, "LocalReAlloc");
        p[977] = GetProcAddress(hLib, "LocalShrink");
        p[978] = GetProcAddress(hLib, "LocalSize");
        p[979] = GetProcAddress(hLib, "LocalUnlock");
        p[980] = GetProcAddress(hLib, "LocaleNameToLCID");
        p[981] = GetProcAddress(hLib, "LocateXStateFeature");
        p[982] = GetProcAddress(hLib, "LockFile");
        p[983] = GetProcAddress(hLib, "LockFileEx");
        p[984] = GetProcAddress(hLib, "LockResource");
        p[985] = GetProcAddress(hLib, "MapUserPhysicalPages");
        p[986] = GetProcAddress(hLib, "MapUserPhysicalPagesScatter");
        p[987] = GetProcAddress(hLib, "MapViewOfFile");
        p[988] = GetProcAddress(hLib, "MapViewOfFileEx");
        p[989] = GetProcAddress(hLib, "MapViewOfFileExNuma");
        p[990] = GetProcAddress(hLib, "MapViewOfFileFromApp"); //Forwarder: api-ms-win-core-memory-l1-1-1.MapViewOfFileFromApp
        p[991] = GetProcAddress(hLib, "Module32First");
        p[992] = GetProcAddress(hLib, "Module32FirstW");
        p[993] = GetProcAddress(hLib, "Module32Next");
        p[994] = GetProcAddress(hLib, "Module32NextW");
        p[995] = GetProcAddress(hLib, "MoveFileA");
        p[996] = GetProcAddress(hLib, "MoveFileExA");
        p[997] = GetProcAddress(hLib, "MoveFileExW");
        p[998] = GetProcAddress(hLib, "MoveFileTransactedA");
        p[999] = GetProcAddress(hLib, "MoveFileTransactedW");
        p[1000] = GetProcAddress(hLib, "MoveFileW");
        p[1001] = GetProcAddress(hLib, "MoveFileWithProgressA");
        p[1002] = GetProcAddress(hLib, "MoveFileWithProgressW");
        p[1003] = GetProcAddress(hLib, "MulDiv");
        p[1004] = GetProcAddress(hLib, "MultiByteToWideChar");
        p[1005] = GetProcAddress(hLib, "NeedCurrentDirectoryForExePathA");
        p[1006] = GetProcAddress(hLib, "NeedCurrentDirectoryForExePathW");
        p[1007] = GetProcAddress(hLib, "NlsCheckPolicy");
        p[1008] = GetProcAddress(hLib, "NlsEventDataDescCreate");
        p[1009] = GetProcAddress(hLib, "NlsGetCacheUpdateCount");
        p[1010] = GetProcAddress(hLib, "NlsUpdateLocale");
        p[1011] = GetProcAddress(hLib, "NlsUpdateSystemLocale");
        p[1012] = GetProcAddress(hLib, "NlsWriteEtwEvent");
        p[1013] = GetProcAddress(hLib, "NormalizeString");
        p[1014] = GetProcAddress(hLib, "NotifyMountMgr");
        p[1015] = GetProcAddress(hLib, "NotifyUILanguageChange");
        p[1016] = GetProcAddress(hLib, "NtVdm64CreateProcessInternalW");
        p[1017] = GetProcAddress(hLib, "OpenConsoleW");
        p[1018] = GetProcAddress(hLib, "OpenConsoleWStub");
        p[1019] = GetProcAddress(hLib, "OpenEventA");
        p[1020] = GetProcAddress(hLib, "OpenEventW");
        p[1021] = GetProcAddress(hLib, "OpenFile");
        p[1022] = GetProcAddress(hLib, "OpenFileById");
        p[1023] = GetProcAddress(hLib, "OpenFileMappingA");
        p[1024] = GetProcAddress(hLib, "OpenFileMappingW");
        p[1025] = GetProcAddress(hLib, "OpenJobObjectA");
        p[1026] = GetProcAddress(hLib, "OpenJobObjectW");
        p[1027] = GetProcAddress(hLib, "OpenMutexA");
        p[1028] = GetProcAddress(hLib, "OpenMutexW");
        p[1029] = GetProcAddress(hLib, "OpenPackageInfoByFullName");
        p[1030] = GetProcAddress(hLib, "OpenPrivateNamespaceA");
        p[1031] = GetProcAddress(hLib, "OpenPrivateNamespaceW");
        p[1032] = GetProcAddress(hLib, "OpenProcess");
        p[1033] = GetProcAddress(hLib, "OpenProcessToken"); //Forwarder: api-ms-win-core-processthreads-l1-1-0.OpenProcessToken
        p[1034] = GetProcAddress(hLib, "OpenProfileUserMapping");
        p[1035] = GetProcAddress(hLib, "OpenSemaphoreA");
        p[1036] = GetProcAddress(hLib, "OpenSemaphoreW");
        p[1037] = GetProcAddress(hLib, "OpenState");
        p[1038] = GetProcAddress(hLib, "OpenStateAtom");
        p[1039] = GetProcAddress(hLib, "OpenStateExplicit");
        p[1040] = GetProcAddress(hLib, "OpenThread");
        p[1041] = GetProcAddress(hLib, "OpenThreadToken"); //Forwarder: api-ms-win-core-processthreads-l1-1-0.OpenThreadToken
        p[1042] = GetProcAddress(hLib, "OpenWaitableTimerA");
        p[1043] = GetProcAddress(hLib, "OpenWaitableTimerW");
        p[1044] = GetProcAddress(hLib, "OutputDebugStringA");
        p[1045] = GetProcAddress(hLib, "OutputDebugStringW");
        p[1046] = GetProcAddress(hLib, "OverrideRoamingDataModificationTimesInRange");
        p[1047] = GetProcAddress(hLib, "PackageFamilyNameFromFullName");
        p[1048] = GetProcAddress(hLib, "PackageFamilyNameFromId");
        p[1049] = GetProcAddress(hLib, "PackageFullNameFromId");
        p[1050] = GetProcAddress(hLib, "PackageIdFromFullName");
        p[1051] = GetProcAddress(hLib, "PackageNameAndPublisherIdFromFamilyName");
        p[1052] = GetProcAddress(hLib, "PeekConsoleInputA");
        p[1053] = GetProcAddress(hLib, "PeekConsoleInputW");
        p[1054] = GetProcAddress(hLib, "PeekNamedPipe");
        p[1055] = GetProcAddress(hLib, "PostQueuedCompletionStatus");
        p[1056] = GetProcAddress(hLib, "PowerClearRequest");
        p[1057] = GetProcAddress(hLib, "PowerCreateRequest");
        p[1058] = GetProcAddress(hLib, "PowerSetRequest");
        p[1059] = GetProcAddress(hLib, "PrefetchVirtualMemory"); //Forwarder: api-ms-win-core-memory-l1-1-1.PrefetchVirtualMemory
        p[1060] = GetProcAddress(hLib, "PrepareTape");
        p[1061] = GetProcAddress(hLib, "PrivCopyFileExW");
        p[1062] = GetProcAddress(hLib, "PrivMoveFileIdentityW");
        p[1063] = GetProcAddress(hLib, "Process32First");
        p[1064] = GetProcAddress(hLib, "Process32FirstW");
        p[1065] = GetProcAddress(hLib, "Process32Next");
        p[1066] = GetProcAddress(hLib, "Process32NextW");
        p[1067] = GetProcAddress(hLib, "ProcessIdToSessionId");
        p[1068] = GetProcAddress(hLib, "PublishStateChangeNotification");
        p[1069] = GetProcAddress(hLib, "PulseEvent");
        p[1070] = GetProcAddress(hLib, "PurgeComm");
        p[1071] = GetProcAddress(hLib, "QueryActCtxSettingsW");
        p[1072] = GetProcAddress(hLib, "QueryActCtxSettingsWWorker");
        p[1073] = GetProcAddress(hLib, "QueryActCtxW");
        p[1074] = GetProcAddress(hLib, "QueryActCtxWWorker");
        p[1075] = GetProcAddress(hLib, "QueryDepthSList"); //Forwarder: NTDLL.RtlQueryDepthSList
        p[1076] = GetProcAddress(hLib, "QueryDosDeviceA");
        p[1077] = GetProcAddress(hLib, "QueryDosDeviceW");
        p[1078] = GetProcAddress(hLib, "QueryFullProcessImageNameA");
        p[1079] = GetProcAddress(hLib, "QueryFullProcessImageNameW");
        p[1080] = GetProcAddress(hLib, "QueryIdleProcessorCycleTime");
        p[1081] = GetProcAddress(hLib, "QueryIdleProcessorCycleTimeEx");
        p[1082] = GetProcAddress(hLib, "QueryInformationJobObject");
        p[1083] = GetProcAddress(hLib, "QueryMemoryResourceNotification");
        p[1084] = GetProcAddress(hLib, "QueryPerformanceCounter");
        p[1085] = GetProcAddress(hLib, "QueryPerformanceFrequency");
        p[1086] = GetProcAddress(hLib, "QueryProcessAffinityUpdateMode");
        p[1087] = GetProcAddress(hLib, "QueryProcessCycleTime");
        p[1088] = GetProcAddress(hLib, "QueryStateAtomValueInfo");
        p[1089] = GetProcAddress(hLib, "QueryStateContainerItemInfo");
        p[1090] = GetProcAddress(hLib, "QueryThreadCycleTime");
        p[1091] = GetProcAddress(hLib, "QueryThreadProfiling");
        p[1092] = GetProcAddress(hLib, "QueryThreadpoolStackInformation");
        p[1093] = GetProcAddress(hLib, "QueryUnbiasedInterruptTime");
        p[1094] = GetProcAddress(hLib, "QueueUserAPC");
        p[1095] = GetProcAddress(hLib, "QueueUserWorkItem");
        p[1096] = GetProcAddress(hLib, "RaiseException");
        p[1097] = GetProcAddress(hLib, "RaiseFailFastException");
        p[1098] = GetProcAddress(hLib, "RaiseInvalid16BitExeError");
        p[1099] = GetProcAddress(hLib, "ReOpenFile");
        p[1100] = GetProcAddress(hLib, "ReadConsoleA");
        p[1101] = GetProcAddress(hLib, "ReadConsoleInputA");
        p[1102] = GetProcAddress(hLib, "ReadConsoleInputExA"); //Forwarder: kernelbase.ReadConsoleInputExA
        p[1103] = GetProcAddress(hLib, "ReadConsoleInputExW"); //Forwarder: kernelbase.ReadConsoleInputExW
        p[1104] = GetProcAddress(hLib, "ReadConsoleInputW");
        p[1105] = GetProcAddress(hLib, "ReadConsoleOutputA");
        p[1106] = GetProcAddress(hLib, "ReadConsoleOutputAttribute");
        p[1107] = GetProcAddress(hLib, "ReadConsoleOutputCharacterA");
        p[1108] = GetProcAddress(hLib, "ReadConsoleOutputCharacterW");
        p[1109] = GetProcAddress(hLib, "ReadConsoleOutputW");
        p[1110] = GetProcAddress(hLib, "ReadConsoleW");
        p[1111] = GetProcAddress(hLib, "ReadDirectoryChangesW");
        p[1112] = GetProcAddress(hLib, "ReadFile");
        p[1113] = GetProcAddress(hLib, "ReadFileEx");
        p[1114] = GetProcAddress(hLib, "ReadFileScatter");
        p[1115] = GetProcAddress(hLib, "ReadProcessMemory");
        p[1116] = GetProcAddress(hLib, "ReadStateAtomValue");
        p[1117] = GetProcAddress(hLib, "ReadStateContainerValue");
        p[1118] = GetProcAddress(hLib, "ReadThreadProfilingData");
        p[1119] = GetProcAddress(hLib, "RegCloseKey");
        p[1120] = GetProcAddress(hLib, "RegCopyTreeW");
        p[1121] = GetProcAddress(hLib, "RegCreateKeyExA");
        p[1122] = GetProcAddress(hLib, "RegCreateKeyExW");
        p[1123] = GetProcAddress(hLib, "RegDeleteKeyExA");
        p[1124] = GetProcAddress(hLib, "RegDeleteKeyExW");
        p[1125] = GetProcAddress(hLib, "RegDeleteTreeA");
        p[1126] = GetProcAddress(hLib, "RegDeleteTreeW");
        p[1127] = GetProcAddress(hLib, "RegDeleteValueA");
        p[1128] = GetProcAddress(hLib, "RegDeleteValueW");
        p[1129] = GetProcAddress(hLib, "RegDisablePredefinedCacheEx");
        p[1130] = GetProcAddress(hLib, "RegEnumKeyExA");
        p[1131] = GetProcAddress(hLib, "RegEnumKeyExW");
        p[1132] = GetProcAddress(hLib, "RegEnumValueA");
        p[1133] = GetProcAddress(hLib, "RegEnumValueW");
        p[1134] = GetProcAddress(hLib, "RegFlushKey");
        p[1135] = GetProcAddress(hLib, "RegGetKeySecurity");
        p[1136] = GetProcAddress(hLib, "RegGetValueA");
        p[1137] = GetProcAddress(hLib, "RegGetValueW");
        p[1138] = GetProcAddress(hLib, "RegLoadKeyA");
        p[1139] = GetProcAddress(hLib, "RegLoadKeyW");
        p[1140] = GetProcAddress(hLib, "RegLoadMUIStringA");
        p[1141] = GetProcAddress(hLib, "RegLoadMUIStringW");
        p[1142] = GetProcAddress(hLib, "RegNotifyChangeKeyValue");
        p[1143] = GetProcAddress(hLib, "RegOpenCurrentUser");
        p[1144] = GetProcAddress(hLib, "RegOpenKeyExA");
        p[1145] = GetProcAddress(hLib, "RegOpenKeyExW");
        p[1146] = GetProcAddress(hLib, "RegOpenUserClassesRoot");
        p[1147] = GetProcAddress(hLib, "RegQueryInfoKeyA");
        p[1148] = GetProcAddress(hLib, "RegQueryInfoKeyW");
        p[1149] = GetProcAddress(hLib, "RegQueryValueExA");
        p[1150] = GetProcAddress(hLib, "RegQueryValueExW");
        p[1151] = GetProcAddress(hLib, "RegRestoreKeyA");
        p[1152] = GetProcAddress(hLib, "RegRestoreKeyW");
        p[1153] = GetProcAddress(hLib, "RegSaveKeyExA");
        p[1154] = GetProcAddress(hLib, "RegSaveKeyExW");
        p[1155] = GetProcAddress(hLib, "RegSetKeySecurity");
        p[1156] = GetProcAddress(hLib, "RegSetValueExA");
        p[1157] = GetProcAddress(hLib, "RegSetValueExW");
        p[1158] = GetProcAddress(hLib, "RegUnLoadKeyA");
        p[1159] = GetProcAddress(hLib, "RegUnLoadKeyW");
        p[1160] = GetProcAddress(hLib, "RegisterApplicationRecoveryCallback");
        p[1161] = GetProcAddress(hLib, "RegisterApplicationRestart");
        p[1162] = GetProcAddress(hLib, "RegisterBadMemoryNotification");
        p[1163] = GetProcAddress(hLib, "RegisterConsoleIME");
        p[1164] = GetProcAddress(hLib, "RegisterConsoleOS2");
        p[1165] = GetProcAddress(hLib, "RegisterConsoleVDM");
        p[1166] = GetProcAddress(hLib, "RegisterStateChangeNotification");
        p[1167] = GetProcAddress(hLib, "RegisterStateLock");
        p[1168] = GetProcAddress(hLib, "RegisterWaitForInputIdle");
        p[1169] = GetProcAddress(hLib, "RegisterWaitForSingleObject");
        p[1170] = GetProcAddress(hLib, "RegisterWaitForSingleObjectEx");
        p[1171] = GetProcAddress(hLib, "RegisterWowBaseHandlers");
        p[1172] = GetProcAddress(hLib, "RegisterWowExec");
        p[1173] = GetProcAddress(hLib, "ReleaseActCtx");
        p[1174] = GetProcAddress(hLib, "ReleaseActCtxWorker");
        p[1175] = GetProcAddress(hLib, "ReleaseMutex");
        p[1176] = GetProcAddress(hLib, "ReleaseMutexWhenCallbackReturns"); //Forwarder: NTDLL.TpCallbackReleaseMutexOnCompletion
        p[1177] = GetProcAddress(hLib, "ReleaseSRWLockExclusive"); //Forwarder: NTDLL.RtlReleaseSRWLockExclusive
        p[1178] = GetProcAddress(hLib, "ReleaseSRWLockShared"); //Forwarder: NTDLL.RtlReleaseSRWLockShared
        p[1179] = GetProcAddress(hLib, "ReleaseSemaphore");
        p[1180] = GetProcAddress(hLib, "ReleaseSemaphoreWhenCallbackReturns"); //Forwarder: NTDLL.TpCallbackReleaseSemaphoreOnCompletion
        p[1181] = GetProcAddress(hLib, "ReleaseStateLock");
        p[1182] = GetProcAddress(hLib, "RemoveDirectoryA");
        p[1183] = GetProcAddress(hLib, "RemoveDirectoryTransactedA");
        p[1184] = GetProcAddress(hLib, "RemoveDirectoryTransactedW");
        p[1185] = GetProcAddress(hLib, "RemoveDirectoryW");
        p[1186] = GetProcAddress(hLib, "RemoveDllDirectory"); //Forwarder: api-ms-win-core-libraryloader-l1-1-0.RemoveDllDirectory
        p[1187] = GetProcAddress(hLib, "RemoveLocalAlternateComputerNameA");
        p[1188] = GetProcAddress(hLib, "RemoveLocalAlternateComputerNameW");
        p[1189] = GetProcAddress(hLib, "RemoveSecureMemoryCacheCallback");
        p[1190] = GetProcAddress(hLib, "RemoveVectoredContinueHandler"); //Forwarder: NTDLL.RtlRemoveVectoredContinueHandler
        p[1191] = GetProcAddress(hLib, "RemoveVectoredExceptionHandler"); //Forwarder: NTDLL.RtlRemoveVectoredExceptionHandler
        p[1192] = GetProcAddress(hLib, "ReplaceFile");
        p[1193] = GetProcAddress(hLib, "ReplaceFileA");
        p[1194] = GetProcAddress(hLib, "ReplaceFileW");
        p[1195] = GetProcAddress(hLib, "ReplacePartitionUnit");
        p[1196] = GetProcAddress(hLib, "RequestDeviceWakeup");
        p[1197] = GetProcAddress(hLib, "RequestWakeupLatency");
        p[1198] = GetProcAddress(hLib, "ResetEvent");
        p[1199] = GetProcAddress(hLib, "ResetState");
        p[1200] = GetProcAddress(hLib, "ResetWriteWatch");
        p[1201] = GetProcAddress(hLib, "ResolveDelayLoadedAPI"); //Forwarder: NTDLL.LdrResolveDelayLoadedAPI
        p[1202] = GetProcAddress(hLib, "ResolveDelayLoadsFromDll"); //Forwarder: NTDLL.LdrResolveDelayLoadsFromDll
        p[1203] = GetProcAddress(hLib, "ResolveLocaleName");
        p[1204] = GetProcAddress(hLib, "RestoreLastError"); //Forwarder: NTDLL.RtlRestoreLastWin32Error
        p[1205] = GetProcAddress(hLib, "ResumeThread");
        p[1206] = GetProcAddress(hLib, "RtlCaptureContext");
        p[1207] = GetProcAddress(hLib, "RtlCaptureStackBackTrace");
        p[1208] = GetProcAddress(hLib, "RtlFillMemory");
        p[1209] = GetProcAddress(hLib, "RtlMoveMemory"); //Forwarder: NTDLL.RtlMoveMemory
        p[1210] = GetProcAddress(hLib, "RtlUnwind");
        p[1211] = GetProcAddress(hLib, "RtlZeroMemory"); //Forwarder: NTDLL.RtlZeroMemory
        p[1212] = GetProcAddress(hLib, "ScrollConsoleScreenBufferA");
        p[1213] = GetProcAddress(hLib, "ScrollConsoleScreenBufferW");
        p[1214] = GetProcAddress(hLib, "SearchPathA");
        p[1215] = GetProcAddress(hLib, "SearchPathW");
        p[1216] = GetProcAddress(hLib, "SetCachedSigningLevel");
        p[1217] = GetProcAddress(hLib, "SetCalendarInfoA");
        p[1218] = GetProcAddress(hLib, "SetCalendarInfoW");
        p[1219] = GetProcAddress(hLib, "SetComPlusPackageInstallStatus");
        p[1220] = GetProcAddress(hLib, "SetCommBreak");
        p[1221] = GetProcAddress(hLib, "SetCommConfig");
        p[1222] = GetProcAddress(hLib, "SetCommMask");
        p[1223] = GetProcAddress(hLib, "SetCommState");
        p[1224] = GetProcAddress(hLib, "SetCommTimeouts");
        p[1225] = GetProcAddress(hLib, "SetComputerNameA");
        p[1226] = GetProcAddress(hLib, "SetComputerNameExA");
        p[1227] = GetProcAddress(hLib, "SetComputerNameExW");
        p[1228] = GetProcAddress(hLib, "SetComputerNameW");
        p[1229] = GetProcAddress(hLib, "SetConsoleActiveScreenBuffer");
        p[1230] = GetProcAddress(hLib, "SetConsoleCP");
        p[1231] = GetProcAddress(hLib, "SetConsoleCtrlHandler");
        p[1232] = GetProcAddress(hLib, "SetConsoleCursor");
        p[1233] = GetProcAddress(hLib, "SetConsoleCursorInfo");
        p[1234] = GetProcAddress(hLib, "SetConsoleCursorMode");
        p[1235] = GetProcAddress(hLib, "SetConsoleCursorPosition");
        p[1236] = GetProcAddress(hLib, "SetConsoleDisplayMode");
        p[1237] = GetProcAddress(hLib, "SetConsoleFont");
        p[1238] = GetProcAddress(hLib, "SetConsoleHardwareState");
        p[1239] = GetProcAddress(hLib, "SetConsoleHistoryInfo");
        p[1240] = GetProcAddress(hLib, "SetConsoleIcon");
        p[1241] = GetProcAddress(hLib, "SetConsoleInputExeNameA"); //Forwarder: kernelbase.SetConsoleInputExeNameA
        p[1242] = GetProcAddress(hLib, "SetConsoleInputExeNameW"); //Forwarder: kernelbase.SetConsoleInputExeNameW
        p[1243] = GetProcAddress(hLib, "SetConsoleKeyShortcuts");
        p[1244] = GetProcAddress(hLib, "SetConsoleLocalEUDC");
        p[1245] = GetProcAddress(hLib, "SetConsoleMaximumWindowSize");
        p[1246] = GetProcAddress(hLib, "SetConsoleMenuClose");
        p[1247] = GetProcAddress(hLib, "SetConsoleMode");
        p[1248] = GetProcAddress(hLib, "SetConsoleNlsMode");
        p[1249] = GetProcAddress(hLib, "SetConsoleNumberOfCommandsA");
        p[1250] = GetProcAddress(hLib, "SetConsoleNumberOfCommandsW");
        p[1251] = GetProcAddress(hLib, "SetConsoleOS2OemFormat");
        p[1252] = GetProcAddress(hLib, "SetConsoleOutputCP");
        p[1253] = GetProcAddress(hLib, "SetConsolePalette");
        p[1254] = GetProcAddress(hLib, "SetConsoleScreenBufferInfoEx");
        p[1255] = GetProcAddress(hLib, "SetConsoleScreenBufferSize");
        p[1256] = GetProcAddress(hLib, "SetConsoleTextAttribute");
        p[1257] = GetProcAddress(hLib, "SetConsoleTitleA");
        p[1258] = GetProcAddress(hLib, "SetConsoleTitleW");
        p[1259] = GetProcAddress(hLib, "SetConsoleWindowInfo");
        p[1260] = GetProcAddress(hLib, "SetCriticalSectionSpinCount"); //Forwarder: NTDLL.RtlSetCriticalSectionSpinCount
        p[1261] = GetProcAddress(hLib, "SetCurrentConsoleFontEx");
        p[1262] = GetProcAddress(hLib, "SetCurrentDirectoryA");
        p[1263] = GetProcAddress(hLib, "SetCurrentDirectoryW");
        p[1264] = GetProcAddress(hLib, "SetDefaultCommConfigA");
        p[1265] = GetProcAddress(hLib, "SetDefaultCommConfigW");
        p[1266] = GetProcAddress(hLib, "SetDefaultDllDirectories"); //Forwarder: api-ms-win-core-libraryloader-l1-1-0.SetDefaultDllDirectories
        p[1267] = GetProcAddress(hLib, "SetDllDirectoryA");
        p[1268] = GetProcAddress(hLib, "SetDllDirectoryW");
        p[1269] = GetProcAddress(hLib, "SetDynamicTimeZoneInformation");
        p[1270] = GetProcAddress(hLib, "SetEndOfFile");
        p[1271] = GetProcAddress(hLib, "SetEnvironmentStringsA");
        p[1272] = GetProcAddress(hLib, "SetEnvironmentStringsW");
        p[1273] = GetProcAddress(hLib, "SetEnvironmentVariableA");
        p[1274] = GetProcAddress(hLib, "SetEnvironmentVariableW");
        p[1275] = GetProcAddress(hLib, "SetErrorMode");
        p[1276] = GetProcAddress(hLib, "SetEvent");
        p[1277] = GetProcAddress(hLib, "SetEventWhenCallbackReturns"); //Forwarder: NTDLL.TpCallbackSetEventOnCompletion
        p[1278] = GetProcAddress(hLib, "SetFileApisToANSI");
        p[1279] = GetProcAddress(hLib, "SetFileApisToOEM");
        p[1280] = GetProcAddress(hLib, "SetFileAttributesA");
        p[1281] = GetProcAddress(hLib, "SetFileAttributesTransactedA");
        p[1282] = GetProcAddress(hLib, "SetFileAttributesTransactedW");
        p[1283] = GetProcAddress(hLib, "SetFileAttributesW");
        p[1284] = GetProcAddress(hLib, "SetFileBandwidthReservation");
        p[1285] = GetProcAddress(hLib, "SetFileCompletionNotificationModes");
        p[1286] = GetProcAddress(hLib, "SetFileInformationByHandle");
        p[1287] = GetProcAddress(hLib, "SetFileIoOverlappedRange");
        p[1288] = GetProcAddress(hLib, "SetFilePointer");
        p[1289] = GetProcAddress(hLib, "SetFilePointerEx");
        p[1290] = GetProcAddress(hLib, "SetFileShortNameA");
        p[1291] = GetProcAddress(hLib, "SetFileShortNameW");
        p[1292] = GetProcAddress(hLib, "SetFileTime");
        p[1293] = GetProcAddress(hLib, "SetFileValidData");
        p[1294] = GetProcAddress(hLib, "SetFirmwareEnvironmentVariableA");
        p[1295] = GetProcAddress(hLib, "SetFirmwareEnvironmentVariableExA");
        p[1296] = GetProcAddress(hLib, "SetFirmwareEnvironmentVariableExW");
        p[1297] = GetProcAddress(hLib, "SetFirmwareEnvironmentVariableW");
        p[1298] = GetProcAddress(hLib, "SetHandleContext");
        p[1299] = GetProcAddress(hLib, "SetHandleCount");
        p[1300] = GetProcAddress(hLib, "SetHandleInformation");
        p[1301] = GetProcAddress(hLib, "SetInformationJobObject");
        p[1302] = GetProcAddress(hLib, "SetLastConsoleEventActive"); //Forwarder: kernelbase.SetLastConsoleEventActive
        p[1303] = GetProcAddress(hLib, "SetLastError");
        p[1304] = GetProcAddress(hLib, "SetLocalPrimaryComputerNameA");
        p[1305] = GetProcAddress(hLib, "SetLocalPrimaryComputerNameW");
        p[1306] = GetProcAddress(hLib, "SetLocalTime");
        p[1307] = GetProcAddress(hLib, "SetLocaleInfoA");
        p[1308] = GetProcAddress(hLib, "SetLocaleInfoW");
        p[1309] = GetProcAddress(hLib, "SetMailslotInfo");
        p[1310] = GetProcAddress(hLib, "SetMessageWaitingIndicator");
        p[1311] = GetProcAddress(hLib, "SetNamedPipeAttribute");
        p[1312] = GetProcAddress(hLib, "SetNamedPipeHandleState");
        p[1313] = GetProcAddress(hLib, "SetPriorityClass");
        p[1314] = GetProcAddress(hLib, "SetProcessAffinityMask");
        p[1315] = GetProcAddress(hLib, "SetProcessAffinityUpdateMode");
        p[1316] = GetProcAddress(hLib, "SetProcessDEPPolicy");
        p[1317] = GetProcAddress(hLib, "SetProcessInformation");
        p[1318] = GetProcAddress(hLib, "SetProcessMitigationPolicy"); //Forwarder: api-ms-win-core-processthreads-l1-1-1.SetProcessMitigationPolicy
        p[1319] = GetProcAddress(hLib, "SetProcessPreferredUILanguages");
        p[1320] = GetProcAddress(hLib, "SetProcessPriorityBoost");
        p[1321] = GetProcAddress(hLib, "SetProcessShutdownParameters");
        p[1322] = GetProcAddress(hLib, "SetProcessWorkingSetSize");
        p[1323] = GetProcAddress(hLib, "SetProcessWorkingSetSizeEx");
        p[1324] = GetProcAddress(hLib, "SetRoamingLastObservedChangeTime");
        p[1325] = GetProcAddress(hLib, "SetSearchPathMode");
        p[1326] = GetProcAddress(hLib, "SetStateVersion");
        p[1327] = GetProcAddress(hLib, "SetStdHandle");
        p[1328] = GetProcAddress(hLib, "SetStdHandleEx");
        p[1329] = GetProcAddress(hLib, "SetSystemFileCacheSize");
        p[1330] = GetProcAddress(hLib, "SetSystemPowerState");
        p[1331] = GetProcAddress(hLib, "SetSystemTime");
        p[1332] = GetProcAddress(hLib, "SetSystemTimeAdjustment");
        p[1333] = GetProcAddress(hLib, "SetTapeParameters");
        p[1334] = GetProcAddress(hLib, "SetTapePosition");
        p[1335] = GetProcAddress(hLib, "SetTermsrvAppInstallMode");
        p[1336] = GetProcAddress(hLib, "SetThreadAffinityMask");
        p[1337] = GetProcAddress(hLib, "SetThreadContext");
        p[1338] = GetProcAddress(hLib, "SetThreadErrorMode");
        p[1339] = GetProcAddress(hLib, "SetThreadExecutionState");
        p[1340] = GetProcAddress(hLib, "SetThreadGroupAffinity");
        p[1341] = GetProcAddress(hLib, "SetThreadIdealProcessor");
        p[1342] = GetProcAddress(hLib, "SetThreadIdealProcessorEx");
        p[1343] = GetProcAddress(hLib, "SetThreadInformation");
        p[1344] = GetProcAddress(hLib, "SetThreadLocale");
        p[1345] = GetProcAddress(hLib, "SetThreadPreferredUILanguages");
        p[1346] = GetProcAddress(hLib, "SetThreadPriority");
        p[1347] = GetProcAddress(hLib, "SetThreadPriorityBoost");
        p[1348] = GetProcAddress(hLib, "SetThreadStackGuarantee");
        p[1349] = GetProcAddress(hLib, "SetThreadToken"); //Forwarder: api-ms-win-core-processthreads-l1-1-0.SetThreadToken
        p[1350] = GetProcAddress(hLib, "SetThreadUILanguage");
        p[1351] = GetProcAddress(hLib, "SetThreadpoolStackInformation");
        p[1352] = GetProcAddress(hLib, "SetThreadpoolThreadMaximum"); //Forwarder: NTDLL.TpSetPoolMaxThreads
        p[1353] = GetProcAddress(hLib, "SetThreadpoolThreadMinimum");
        p[1354] = GetProcAddress(hLib, "SetThreadpoolTimer"); //Forwarder: NTDLL.TpSetTimer
        p[1355] = GetProcAddress(hLib, "SetThreadpoolTimerEx"); //Forwarder: NTDLL.TpSetTimerEx
        p[1356] = GetProcAddress(hLib, "SetThreadpoolWait"); //Forwarder: NTDLL.TpSetWait
        p[1357] = GetProcAddress(hLib, "SetThreadpoolWaitEx"); //Forwarder: NTDLL.TpSetWaitEx
        p[1358] = GetProcAddress(hLib, "SetTimeZoneInformation");
        p[1359] = GetProcAddress(hLib, "SetTimerQueueTimer");
        p[1360] = GetProcAddress(hLib, "SetUnhandledExceptionFilter");
        p[1361] = GetProcAddress(hLib, "SetUserGeoID");
        p[1362] = GetProcAddress(hLib, "SetVDMCurrentDirectories");
        p[1363] = GetProcAddress(hLib, "SetVolumeLabelA");
        p[1364] = GetProcAddress(hLib, "SetVolumeLabelW");
        p[1365] = GetProcAddress(hLib, "SetVolumeMountPointA");
        p[1366] = GetProcAddress(hLib, "SetVolumeMountPointW");
        p[1367] = GetProcAddress(hLib, "SetVolumeMountPointWStub");
        p[1368] = GetProcAddress(hLib, "SetWaitableTimer");
        p[1369] = GetProcAddress(hLib, "SetWaitableTimerEx"); //Forwarder: api-ms-win-core-synch-l1-1-0.SetWaitableTimerEx
        p[1370] = GetProcAddress(hLib, "SetXStateFeaturesMask");
        p[1371] = GetProcAddress(hLib, "SetupComm");
        p[1372] = GetProcAddress(hLib, "ShowConsoleCursor");
        p[1373] = GetProcAddress(hLib, "SignalObjectAndWait");
        p[1374] = GetProcAddress(hLib, "SizeofResource");
        p[1375] = GetProcAddress(hLib, "Sleep");
        p[1376] = GetProcAddress(hLib, "SleepConditionVariableCS"); //Forwarder: api-ms-win-core-synch-l1-1-0.SleepConditionVariableCS
        p[1377] = GetProcAddress(hLib, "SleepConditionVariableSRW"); //Forwarder: api-ms-win-core-synch-l1-1-0.SleepConditionVariableSRW
        p[1378] = GetProcAddress(hLib, "SleepEx");
        p[1379] = GetProcAddress(hLib, "SortCloseHandle");
        p[1380] = GetProcAddress(hLib, "SortGetHandle");
        p[1381] = GetProcAddress(hLib, "StartThreadpoolIo"); //Forwarder: NTDLL.TpStartAsyncIoOperation
        p[1382] = GetProcAddress(hLib, "SubmitThreadpoolWork"); //Forwarder: NTDLL.TpPostWork
        p[1383] = GetProcAddress(hLib, "SubscribeStateChangeNotification");
        p[1384] = GetProcAddress(hLib, "SuspendThread");
        p[1385] = GetProcAddress(hLib, "SwitchToFiber");
        p[1386] = GetProcAddress(hLib, "SwitchToThread");
        p[1387] = GetProcAddress(hLib, "SystemTimeToFileTime");
        p[1388] = GetProcAddress(hLib, "SystemTimeToTzSpecificLocalTime");
        p[1389] = GetProcAddress(hLib, "SystemTimeToTzSpecificLocalTimeEx"); //Forwarder: api-ms-win-core-timezone-l1-1-0.SystemTimeToTzSpecificLocalTimeEx
        p[1390] = GetProcAddress(hLib, "TerminateJobObject");
        p[1391] = GetProcAddress(hLib, "TerminateProcess");
        p[1392] = GetProcAddress(hLib, "TerminateThread");
        p[1393] = GetProcAddress(hLib, "TermsrvAppInstallMode");
        p[1394] = GetProcAddress(hLib, "TermsrvConvertSysRootToUserDir");
        p[1395] = GetProcAddress(hLib, "TermsrvCreateRegEntry");
        p[1396] = GetProcAddress(hLib, "TermsrvDeleteKey");
        p[1397] = GetProcAddress(hLib, "TermsrvDeleteValue");
        p[1398] = GetProcAddress(hLib, "TermsrvGetPreSetValue");
        p[1399] = GetProcAddress(hLib, "TermsrvGetWindowsDirectoryA");
        p[1400] = GetProcAddress(hLib, "TermsrvGetWindowsDirectoryW");
        p[1401] = GetProcAddress(hLib, "TermsrvOpenRegEntry");
        p[1402] = GetProcAddress(hLib, "TermsrvOpenUserClasses");
        p[1403] = GetProcAddress(hLib, "TermsrvRestoreKey");
        p[1404] = GetProcAddress(hLib, "TermsrvSetKeySecurity");
        p[1405] = GetProcAddress(hLib, "TermsrvSetValueKey");
        p[1406] = GetProcAddress(hLib, "TermsrvSyncUserIniFileExt");
        p[1407] = GetProcAddress(hLib, "Thread32First");
        p[1408] = GetProcAddress(hLib, "Thread32Next");
        p[1409] = GetProcAddress(hLib, "TlsAlloc");
        p[1410] = GetProcAddress(hLib, "TlsFree");
        p[1411] = GetProcAddress(hLib, "TlsGetValue");
        p[1412] = GetProcAddress(hLib, "TlsSetValue");
        p[1413] = GetProcAddress(hLib, "Toolhelp32ReadProcessMemory");
        p[1414] = GetProcAddress(hLib, "TransactNamedPipe");
        p[1415] = GetProcAddress(hLib, "TransmitCommChar");
        p[1416] = GetProcAddress(hLib, "TryAcquireSRWLockExclusive"); //Forwarder: NTDLL.RtlTryAcquireSRWLockExclusive
        p[1417] = GetProcAddress(hLib, "TryAcquireSRWLockShared"); //Forwarder: NTDLL.RtlTryAcquireSRWLockShared
        p[1418] = GetProcAddress(hLib, "TryEnterCriticalSection"); //Forwarder: NTDLL.RtlTryEnterCriticalSection
        p[1419] = GetProcAddress(hLib, "TrySubmitThreadpoolCallback");
        p[1420] = GetProcAddress(hLib, "TzSpecificLocalTimeToSystemTime");
        p[1421] = GetProcAddress(hLib, "TzSpecificLocalTimeToSystemTimeEx"); //Forwarder: api-ms-win-core-timezone-l1-1-0.TzSpecificLocalTimeToSystemTimeEx
        p[1422] = GetProcAddress(hLib, "UTRegister");
        p[1423] = GetProcAddress(hLib, "UTUnRegister");
        p[1424] = GetProcAddress(hLib, "UnhandledExceptionFilter");
        p[1425] = GetProcAddress(hLib, "UnlockFile");
        p[1426] = GetProcAddress(hLib, "UnlockFileEx");
        p[1427] = GetProcAddress(hLib, "UnmapViewOfFile");
        p[1428] = GetProcAddress(hLib, "UnmapViewOfFileEx"); //Forwarder: api-ms-win-core-memory-l1-1-1.UnmapViewOfFileEx
        p[1429] = GetProcAddress(hLib, "UnregisterApplicationRecoveryCallback");
        p[1430] = GetProcAddress(hLib, "UnregisterApplicationRestart");
        p[1431] = GetProcAddress(hLib, "UnregisterBadMemoryNotification");
        p[1432] = GetProcAddress(hLib, "UnregisterConsoleIME");
        p[1433] = GetProcAddress(hLib, "UnregisterStateChangeNotification");
        p[1434] = GetProcAddress(hLib, "UnregisterStateLock");
        p[1435] = GetProcAddress(hLib, "UnregisterWait");
        p[1436] = GetProcAddress(hLib, "UnregisterWaitEx");
        p[1437] = GetProcAddress(hLib, "UnsubscribeStateChangeNotification");
        p[1438] = GetProcAddress(hLib, "UpdateCalendarDayOfWeek");
        p[1439] = GetProcAddress(hLib, "UpdateProcThreadAttribute"); //Forwarder: api-ms-win-core-processthreads-l1-1-0.UpdateProcThreadAttribute
        p[1440] = GetProcAddress(hLib, "UpdateResourceA");
        p[1441] = GetProcAddress(hLib, "UpdateResourceW");
        p[1442] = GetProcAddress(hLib, "VDMConsoleOperation");
        p[1443] = GetProcAddress(hLib, "VDMOperationStarted");
        p[1444] = GetProcAddress(hLib, "VerLanguageNameA");
        p[1445] = GetProcAddress(hLib, "VerLanguageNameW");
        p[1446] = GetProcAddress(hLib, "VerSetConditionMask"); //Forwarder: NTDLL.VerSetConditionMask
        p[1447] = GetProcAddress(hLib, "VerifyConsoleIoHandle");
        p[1448] = GetProcAddress(hLib, "VerifyScripts");
        p[1449] = GetProcAddress(hLib, "VerifyVersionInfoA");
        p[1450] = GetProcAddress(hLib, "VerifyVersionInfoW");
        p[1451] = GetProcAddress(hLib, "VirtualAlloc");
        p[1452] = GetProcAddress(hLib, "VirtualAllocEx");
        p[1453] = GetProcAddress(hLib, "VirtualAllocExNuma");
        p[1454] = GetProcAddress(hLib, "VirtualFree");
        p[1455] = GetProcAddress(hLib, "VirtualFreeEx");
        p[1456] = GetProcAddress(hLib, "VirtualLock");
        p[1457] = GetProcAddress(hLib, "VirtualProtect");
        p[1458] = GetProcAddress(hLib, "VirtualProtectEx");
        p[1459] = GetProcAddress(hLib, "VirtualQuery");
        p[1460] = GetProcAddress(hLib, "VirtualQueryEx");
        p[1461] = GetProcAddress(hLib, "VirtualUnlock");
        p[1462] = GetProcAddress(hLib, "WTSGetActiveConsoleSessionId");
        p[1463] = GetProcAddress(hLib, "WaitCommEvent");
        p[1464] = GetProcAddress(hLib, "WaitForDebugEvent");
        p[1465] = GetProcAddress(hLib, "WaitForMultipleObjects");
        p[1466] = GetProcAddress(hLib, "WaitForMultipleObjectsEx");
        p[1467] = GetProcAddress(hLib, "WaitForSingleObject");
        p[1468] = GetProcAddress(hLib, "WaitForSingleObjectEx");
        p[1469] = GetProcAddress(hLib, "WaitForThreadpoolIoCallbacks"); //Forwarder: NTDLL.TpWaitForIoCompletion
        p[1470] = GetProcAddress(hLib, "WaitForThreadpoolTimerCallbacks"); //Forwarder: NTDLL.TpWaitForTimer
        p[1471] = GetProcAddress(hLib, "WaitForThreadpoolWaitCallbacks"); //Forwarder: NTDLL.TpWaitForWait
        p[1472] = GetProcAddress(hLib, "WaitForThreadpoolWorkCallbacks"); //Forwarder: NTDLL.TpWaitForWork
        p[1473] = GetProcAddress(hLib, "WaitNamedPipeA");
        p[1474] = GetProcAddress(hLib, "WaitNamedPipeW");
        p[1475] = GetProcAddress(hLib, "WakeAllConditionVariable"); //Forwarder: NTDLL.RtlWakeAllConditionVariable
        p[1476] = GetProcAddress(hLib, "WakeConditionVariable"); //Forwarder: NTDLL.RtlWakeConditionVariable
        p[1477] = GetProcAddress(hLib, "WerGetFlags");
        p[1478] = GetProcAddress(hLib, "WerRegisterFile");
        p[1479] = GetProcAddress(hLib, "WerRegisterFileWorker");
        p[1480] = GetProcAddress(hLib, "WerRegisterMemoryBlock");
        p[1481] = GetProcAddress(hLib, "WerRegisterMemoryBlockWorker");
        p[1482] = GetProcAddress(hLib, "WerRegisterRuntimeExceptionModule");
        p[1483] = GetProcAddress(hLib, "WerRegisterRuntimeExceptionModuleWorker");
        p[1484] = GetProcAddress(hLib, "WerSetFlags");
        p[1485] = GetProcAddress(hLib, "WerUnregisterFile");
        p[1486] = GetProcAddress(hLib, "WerUnregisterFileWorker");
        p[1487] = GetProcAddress(hLib, "WerUnregisterMemoryBlock");
        p[1488] = GetProcAddress(hLib, "WerUnregisterMemoryBlockWorker");
        p[1489] = GetProcAddress(hLib, "WerUnregisterRuntimeExceptionModule");
        p[1490] = GetProcAddress(hLib, "WerUnregisterRuntimeExceptionModuleWorker");
        p[1491] = GetProcAddress(hLib, "WerpCleanupMessageMapping");
        p[1492] = GetProcAddress(hLib, "WerpGetDebugger");
        p[1493] = GetProcAddress(hLib, "WerpInitiateRemoteRecovery");
        p[1494] = GetProcAddress(hLib, "WerpLaunchAeDebug");
        p[1495] = GetProcAddress(hLib, "WerpNotifyLoadStringResource");
        p[1496] = GetProcAddress(hLib, "WerpNotifyLoadStringResourceEx");
        p[1497] = GetProcAddress(hLib, "WerpNotifyLoadStringResourceWorker");
        p[1498] = GetProcAddress(hLib, "WerpNotifyUseStringResource");
        p[1499] = GetProcAddress(hLib, "WerpNotifyUseStringResourceWorker");
        p[1500] = GetProcAddress(hLib, "WerpStringLookup");
        p[1501] = GetProcAddress(hLib, "WideCharToMultiByte");
        p[1502] = GetProcAddress(hLib, "WinExec");
        p[1503] = GetProcAddress(hLib, "Wow64DisableWow64FsRedirection");
        p[1504] = GetProcAddress(hLib, "Wow64EnableWow64FsRedirection");
        p[1505] = GetProcAddress(hLib, "Wow64GetThreadContext");
        p[1506] = GetProcAddress(hLib, "Wow64GetThreadSelectorEntry");
        p[1507] = GetProcAddress(hLib, "Wow64RevertWow64FsRedirection");
        p[1508] = GetProcAddress(hLib, "Wow64SetThreadContext");
        p[1509] = GetProcAddress(hLib, "Wow64SuspendThread");
        p[1510] = GetProcAddress(hLib, "WriteConsoleA");
        p[1511] = GetProcAddress(hLib, "WriteConsoleInputA");
        p[1512] = GetProcAddress(hLib, "WriteConsoleInputVDMA");
        p[1513] = GetProcAddress(hLib, "WriteConsoleInputVDMW");
        p[1514] = GetProcAddress(hLib, "WriteConsoleInputW");
        p[1515] = GetProcAddress(hLib, "WriteConsoleOutputA");
        p[1516] = GetProcAddress(hLib, "WriteConsoleOutputAttribute");
        p[1517] = GetProcAddress(hLib, "WriteConsoleOutputCharacterA");
        p[1518] = GetProcAddress(hLib, "WriteConsoleOutputCharacterW");
        p[1519] = GetProcAddress(hLib, "WriteConsoleOutputW");
        p[1520] = GetProcAddress(hLib, "WriteConsoleW");
        p[1521] = GetProcAddress(hLib, "WriteFile");
        p[1522] = GetProcAddress(hLib, "WriteFileEx");
        p[1523] = GetProcAddress(hLib, "WriteFileGather");
        p[1524] = GetProcAddress(hLib, "WritePrivateProfileSectionA");
        p[1525] = GetProcAddress(hLib, "WritePrivateProfileSectionW");
        p[1526] = GetProcAddress(hLib, "WritePrivateProfileStringA");
        p[1527] = GetProcAddress(hLib, "WritePrivateProfileStringW");
        p[1528] = GetProcAddress(hLib, "WritePrivateProfileStructA");
        p[1529] = GetProcAddress(hLib, "WritePrivateProfileStructW");
        p[1530] = GetProcAddress(hLib, "WriteProcessMemory");
        p[1531] = GetProcAddress(hLib, "WriteProfileSectionA");
        p[1532] = GetProcAddress(hLib, "WriteProfileSectionW");
        p[1533] = GetProcAddress(hLib, "WriteProfileStringA");
        p[1534] = GetProcAddress(hLib, "WriteProfileStringW");
        p[1535] = GetProcAddress(hLib, "WriteStateAtomValue");
        p[1536] = GetProcAddress(hLib, "WriteStateContainerValue");
        p[1537] = GetProcAddress(hLib, "WriteTapemark");
        p[1538] = GetProcAddress(hLib, "ZombifyActCtx");
        p[1539] = GetProcAddress(hLib, "ZombifyActCtxWorker");
        p[1540] = GetProcAddress(hLib, "_hread");
        p[1541] = GetProcAddress(hLib, "_hwrite");
        p[1542] = GetProcAddress(hLib, "_lclose");
        p[1543] = GetProcAddress(hLib, "_lcreat");
        p[1544] = GetProcAddress(hLib, "_llseek");
        p[1545] = GetProcAddress(hLib, "_lopen");
        p[1546] = GetProcAddress(hLib, "_lread");
        p[1547] = GetProcAddress(hLib, "_lwrite");
        p[1548] = GetProcAddress(hLib, "lstrcat");
        p[1549] = GetProcAddress(hLib, "lstrcatA");
        p[1550] = GetProcAddress(hLib, "lstrcatW");
        p[1551] = GetProcAddress(hLib, "lstrcmp");
        p[1552] = GetProcAddress(hLib, "lstrcmpA");
        p[1553] = GetProcAddress(hLib, "lstrcmpW");
        p[1554] = GetProcAddress(hLib, "lstrcmpi");
        p[1555] = GetProcAddress(hLib, "lstrcmpiA");
        p[1556] = GetProcAddress(hLib, "lstrcmpiW");
        p[1557] = GetProcAddress(hLib, "lstrcpy");
        p[1558] = GetProcAddress(hLib, "lstrcpyA");
        p[1559] = GetProcAddress(hLib, "lstrcpyW");
        p[1560] = GetProcAddress(hLib, "lstrcpyn");
        p[1561] = GetProcAddress(hLib, "lstrcpynA");
        p[1562] = GetProcAddress(hLib, "lstrcpynW");
        p[1563] = GetProcAddress(hLib, "lstrlen");
        p[1564] = GetProcAddress(hLib, "lstrlenA");
        p[1565] = GetProcAddress(hLib, "lstrlenW");
        p[1566] = GetProcAddress(hLib, "timeBeginPeriod");
        p[1567] = GetProcAddress(hLib, "timeEndPeriod");
        p[1568] = GetProcAddress(hLib, "timeGetDevCaps");
        p[1569] = GetProcAddress(hLib, "timeGetSystemTime");
        p[1570] = GetProcAddress(hLib, "timeGetTime");
    }
    else if (fdwReason == DLL_PROCESS_DETACH)
    {
        FreeLibrary(hLib);
        return TRUE;
    }

    return TRUE;
}

extern "C"
{
    __declspec( naked ) void WINAPI PROXY_BaseThreadInitThunk() {
        __asm { jmp p[0 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InterlockedPushListSList() {
        __asm { jmp p[1 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AcquireSRWLockExclusive() {
        __asm { jmp p[2 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AcquireSRWLockShared() {
        __asm { jmp p[3 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AcquireStateLock() {
        __asm { jmp p[4 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ActivateActCtx() {
        __asm { jmp p[5 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ActivateActCtxWorker() {
        __asm { jmp p[6 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AddAtomA() {
        __asm { jmp p[7 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AddAtomW() {
        __asm { jmp p[8 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AddConsoleAliasA() {
        __asm { jmp p[9 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AddConsoleAliasW() {
        __asm { jmp p[10 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AddDllDirectory() {
        __asm { jmp p[11 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AddIntegrityLabelToBoundaryDescriptor() {
        __asm { jmp p[12 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AddLocalAlternateComputerNameA() {
        __asm { jmp p[13 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AddLocalAlternateComputerNameW() {
        __asm { jmp p[14 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AddRefActCtx() {
        __asm { jmp p[15 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AddRefActCtxWorker() {
        __asm { jmp p[16 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AddResourceAttributeAce() {
        __asm { jmp p[17 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AddSIDToBoundaryDescriptor() {
        __asm { jmp p[18 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AddScopedPolicyIDAce() {
        __asm { jmp p[19 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AddSecureMemoryCacheCallback() {
        __asm { jmp p[20 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AddVectoredContinueHandler() {
        __asm { jmp p[21 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AddVectoredExceptionHandler() {
        __asm { jmp p[22 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AdjustCalendarDate() {
        __asm { jmp p[23 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AllocConsole() {
        __asm { jmp p[24 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AllocateUserPhysicalPages() {
        __asm { jmp p[25 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AllocateUserPhysicalPagesNuma() {
        __asm { jmp p[26 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppContainerDeriveSidFromMoniker() {
        __asm { jmp p[27 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppContainerFreeMemory() {
        __asm { jmp p[28 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppContainerLookupDisplayNameMrtReference() {
        __asm { jmp p[29 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppContainerLookupMoniker() {
        __asm { jmp p[30 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppContainerRegisterSid() {
        __asm { jmp p[31 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppContainerUnregisterSid() {
        __asm { jmp p[32 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppXFreeMemory() {
        __asm { jmp p[33 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppXGetApplicationData() {
        __asm { jmp p[34 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppXGetDevelopmentMode() {
        __asm { jmp p[35 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppXGetOSMaxVersionTested() {
        __asm { jmp p[36 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppXGetOSMinVersion() {
        __asm { jmp p[37 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppXGetPackageCapabilities() {
        __asm { jmp p[38 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppXGetPackageSid() {
        __asm { jmp p[39 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppXGetPackageState() {
        __asm { jmp p[40 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppXLookupDisplayName() {
        __asm { jmp p[41 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppXLookupMoniker() {
        __asm { jmp p[42 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AppXSetPackageState() {
        __asm { jmp p[43 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ApplicationRecoveryFinished() {
        __asm { jmp p[44 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ApplicationRecoveryInProgress() {
        __asm { jmp p[45 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AreFileApisANSI() {
        __asm { jmp p[46 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AssignProcessToJobObject() {
        __asm { jmp p[47 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_AttachConsole() {
        __asm { jmp p[48 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BackupRead() {
        __asm { jmp p[49 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BackupSeek() {
        __asm { jmp p[50 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BackupWrite() {
        __asm { jmp p[51 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseCheckAppcompatCache() {
        __asm { jmp p[52 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseCheckAppcompatCacheEx() {
        __asm { jmp p[53 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseCheckAppcompatCacheExWorker() {
        __asm { jmp p[54 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseCheckAppcompatCacheWorker() {
        __asm { jmp p[55 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseCheckElevation() {
        __asm { jmp p[56 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseCheckRunApp() {
        __asm { jmp p[57 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseCleanupAppcompatCacheSupport() {
        __asm { jmp p[58 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseCleanupAppcompatCacheSupportWorker() {
        __asm { jmp p[59 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseDestroyVDMEnvironment() {
        __asm { jmp p[60 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseDllReadWriteIniFile() {
        __asm { jmp p[61 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseDumpAppcompatCache() {
        __asm { jmp p[62 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseDumpAppcompatCacheWorker() {
        __asm { jmp p[63 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseElevationPostProcessing() {
        __asm { jmp p[64 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseFlushAppcompatCache() {
        __asm { jmp p[65 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseFlushAppcompatCacheWorker() {
        __asm { jmp p[66 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseFormatObjectAttributes() {
        __asm { jmp p[67 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseFormatTimeOut() {
        __asm { jmp p[68 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseGenerateAppCompatData() {
        __asm { jmp p[69 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseGetNamedObjectDirectory() {
        __asm { jmp p[70 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseInitAppcompatCacheSupport() {
        __asm { jmp p[71 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseInitAppcompatCacheSupportWorker() {
        __asm { jmp p[72 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseIsAppcompatInfrastructureDisabled() {
        __asm { jmp p[73 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseIsAppcompatInfrastructureDisabledWorker() {
        __asm { jmp p[74 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseIsDosApplication() {
        __asm { jmp p[75 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseQueryModuleData() {
        __asm { jmp p[76 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseSetLastNTError() {
        __asm { jmp p[77 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseUpdateAppcompatCache() {
        __asm { jmp p[78 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseUpdateAppcompatCacheWorker() {
        __asm { jmp p[79 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseUpdateVDMEntry() {
        __asm { jmp p[80 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseVerifyUnicodeString() {
        __asm { jmp p[81 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BaseWriteErrorElevationRequiredEvent() {
        __asm { jmp p[82 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Basep8BitStringToDynamicUnicodeString() {
        __asm { jmp p[83 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepAllocateActivationContextActivationBlock() {
        __asm { jmp p[84 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepAnsiStringToDynamicUnicodeString() {
        __asm { jmp p[85 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepAppCompatHookDLL() {
        __asm { jmp p[86 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepAppContainerEnvironmentExtension() {
        __asm { jmp p[87 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepAppXExtension() {
        __asm { jmp p[88 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepCheckAppCompat() {
        __asm { jmp p[89 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepCheckBadapp() {
        __asm { jmp p[90 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepCheckWebBladeHashes() {
        __asm { jmp p[91 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepCheckWinSaferRestrictions() {
        __asm { jmp p[92 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepConstructSxsCreateProcessMessage() {
        __asm { jmp p[93 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepCopyEncryption() {
        __asm { jmp p[94 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepFreeActivationContextActivationBlock() {
        __asm { jmp p[95 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepFreeAppCompatData() {
        __asm { jmp p[96 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepGetAppCompatData() {
        __asm { jmp p[97 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepGetComputerNameFromNtPath() {
        __asm { jmp p[98 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepGetExeArchType() {
        __asm { jmp p[99 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepIsProcessAllowed() {
        __asm { jmp p[100 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepMapModuleHandle() {
        __asm { jmp p[101 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepNotifyLoadStringResource() {
        __asm { jmp p[102 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepPostSuccessAppXExtension() {
        __asm { jmp p[103 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepProcessInvalidImage() {
        __asm { jmp p[104 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepQueryAppCompat() {
        __asm { jmp p[105 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepReleaseAppXContext() {
        __asm { jmp p[106 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepReleaseSxsCreateProcessUtilityStruct() {
        __asm { jmp p[107 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepReportFault() {
        __asm { jmp p[108 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BasepSetFileEncryptionCompression() {
        __asm { jmp p[109 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Beep() {
        __asm { jmp p[110 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BeginUpdateResourceA() {
        __asm { jmp p[111 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BeginUpdateResourceW() {
        __asm { jmp p[112 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BindIoCompletionCallback() {
        __asm { jmp p[113 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BuildCommDCBA() {
        __asm { jmp p[114 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BuildCommDCBAndTimeoutsA() {
        __asm { jmp p[115 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BuildCommDCBAndTimeoutsW() {
        __asm { jmp p[116 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_BuildCommDCBW() {
        __asm { jmp p[117 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CallNamedPipeA() {
        __asm { jmp p[118 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CallNamedPipeW() {
        __asm { jmp p[119 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CallbackMayRunLong() {
        __asm { jmp p[120 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CancelDeviceWakeupRequest() {
        __asm { jmp p[121 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CancelIo() {
        __asm { jmp p[122 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CancelIoEx() {
        __asm { jmp p[123 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CancelSynchronousIo() {
        __asm { jmp p[124 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CancelThreadpoolIo() {
        __asm { jmp p[125 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CancelTimerQueueTimer() {
        __asm { jmp p[126 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CancelWaitableTimer() {
        __asm { jmp p[127 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CeipIsOptedIn() {
        __asm { jmp p[128 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ChangeTimerQueueTimer() {
        __asm { jmp p[129 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CheckAllowDecryptedRemoteDestinationPolicy() {
        __asm { jmp p[130 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CheckElevation() {
        __asm { jmp p[131 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CheckElevationEnabled() {
        __asm { jmp p[132 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CheckForReadOnlyResource() {
        __asm { jmp p[133 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CheckForReadOnlyResourceFilter() {
        __asm { jmp p[134 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CheckNameLegalDOS8Dot3A() {
        __asm { jmp p[135 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CheckNameLegalDOS8Dot3W() {
        __asm { jmp p[136 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CheckRemoteDebuggerPresent() {
        __asm { jmp p[137 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CheckTokenCapability() {
        __asm { jmp p[138 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CheckTokenMembershipEx() {
        __asm { jmp p[139 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ClearCommBreak() {
        __asm { jmp p[140 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ClearCommError() {
        __asm { jmp p[141 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CloseConsoleHandle() {
        __asm { jmp p[142 * 4] }
    }
    /*
    __declspec( naked ) void WINAPI PROXY_CloseHandle() {
        __asm { jmp p[143 * 4] }
    }
    */
    __declspec( naked ) void WINAPI PROXY_ClosePackageInfo() {
        __asm { jmp p[144 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ClosePrivateNamespace() {
        __asm { jmp p[145 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CloseProfileUserMapping() {
        __asm { jmp p[146 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CloseState() {
        __asm { jmp p[147 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CloseStateAtom() {
        __asm { jmp p[148 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CloseStateChangeNotification() {
        __asm { jmp p[149 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CloseStateContainer() {
        __asm { jmp p[150 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CloseStateLock() {
        __asm { jmp p[151 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CloseThreadpool() {
        __asm { jmp p[152 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CloseThreadpoolCleanupGroup() {
        __asm { jmp p[153 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CloseThreadpoolCleanupGroupMembers() {
        __asm { jmp p[154 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CloseThreadpoolIo() {
        __asm { jmp p[155 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CloseThreadpoolTimer() {
        __asm { jmp p[156 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CloseThreadpoolWait() {
        __asm { jmp p[157 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CloseThreadpoolWork() {
        __asm { jmp p[158 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CmdBatNotification() {
        __asm { jmp p[159 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CommConfigDialogA() {
        __asm { jmp p[160 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CommConfigDialogW() {
        __asm { jmp p[161 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CommitStateAtom() {
        __asm { jmp p[162 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CompareCalendarDates() {
        __asm { jmp p[163 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CompareFileTime() {
        __asm { jmp p[164 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CompareStringA() {
        __asm { jmp p[165 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CompareStringEx() {
        __asm { jmp p[166 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CompareStringOrdinal() {
        __asm { jmp p[167 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CompareStringW() {
        __asm { jmp p[168 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ConnectNamedPipe() {
        __asm { jmp p[169 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ConsoleMenuControl() {
        __asm { jmp p[170 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ContinueDebugEvent() {
        __asm { jmp p[171 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ConvertCalDateTimeToSystemTime() {
        __asm { jmp p[172 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ConvertDefaultLocale() {
        __asm { jmp p[173 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ConvertFiberToThread() {
        __asm { jmp p[174 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ConvertNLSDayOfWeekToWin32DayOfWeek() {
        __asm { jmp p[175 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ConvertSystemTimeToCalDateTime() {
        __asm { jmp p[176 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ConvertThreadToFiber() {
        __asm { jmp p[177 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ConvertThreadToFiberEx() {
        __asm { jmp p[178 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CopyContext() {
        __asm { jmp p[179 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CopyFile2() {
        __asm { jmp p[180 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CopyFileA() {
        __asm { jmp p[181 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CopyFileExA() {
        __asm { jmp p[182 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CopyFileExW() {
        __asm { jmp p[183 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CopyFileTransactedA() {
        __asm { jmp p[184 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CopyFileTransactedW() {
        __asm { jmp p[185 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CopyFileW() {
        __asm { jmp p[186 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CopyLZFile() {
        __asm { jmp p[187 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateActCtxA() {
        __asm { jmp p[188 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateActCtxW() {
        __asm { jmp p[189 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateActCtxWWorker() {
        __asm { jmp p[190 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateBoundaryDescriptorA() {
        __asm { jmp p[191 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateBoundaryDescriptorW() {
        __asm { jmp p[192 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateConsoleScreenBuffer() {
        __asm { jmp p[193 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateDirectoryA() {
        __asm { jmp p[194 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateDirectoryExA() {
        __asm { jmp p[195 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateDirectoryExW() {
        __asm { jmp p[196 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateDirectoryTransactedA() {
        __asm { jmp p[197 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateDirectoryTransactedW() {
        __asm { jmp p[198 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateDirectoryW() {
        __asm { jmp p[199 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateEventA() {
        __asm { jmp p[200 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateEventExA() {
        __asm { jmp p[201 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateEventExW() {
        __asm { jmp p[202 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateEventW() {
        __asm { jmp p[203 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateFiber() {
        __asm { jmp p[204 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateFiberEx() {
        __asm { jmp p[205 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateFile2() {
        __asm { jmp p[206 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateFileA() {
        __asm { jmp p[207 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateFileMappingA() {
        __asm { jmp p[208 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateFileMappingFromApp() {
        __asm { jmp p[209 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateFileMappingNumaA() {
        __asm { jmp p[210 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateFileMappingNumaW() {
        __asm { jmp p[211 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateFileMappingW() {
        __asm { jmp p[212 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateFileTransactedA() {
        __asm { jmp p[213 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateFileTransactedW() {
        __asm { jmp p[214 * 4] }
    }
    /*
    __declspec( naked ) void WINAPI PROXY_CreateFileW() {
        __asm { jmp p[215 * 4] }
    }
    */
    __declspec( naked ) void WINAPI PROXY_CreateHardLinkA() {
        __asm { jmp p[216 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateHardLinkTransactedA() {
        __asm { jmp p[217 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateHardLinkTransactedW() {
        __asm { jmp p[218 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateHardLinkW() {
        __asm { jmp p[219 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateIoCompletionPort() {
        __asm { jmp p[220 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateJobObjectA() {
        __asm { jmp p[221 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateJobObjectW() {
        __asm { jmp p[222 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateJobSet() {
        __asm { jmp p[223 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateMailslotA() {
        __asm { jmp p[224 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateMailslotW() {
        __asm { jmp p[225 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateMemoryResourceNotification() {
        __asm { jmp p[226 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateMutexA() {
        __asm { jmp p[227 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateMutexExA() {
        __asm { jmp p[228 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateMutexExW() {
        __asm { jmp p[229 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateMutexW() {
        __asm { jmp p[230 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateNamedPipeA() {
        __asm { jmp p[231 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateNamedPipeW() {
        __asm { jmp p[232 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreatePipe() {
        __asm { jmp p[233 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreatePrivateNamespaceA() {
        __asm { jmp p[234 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreatePrivateNamespaceW() {
        __asm { jmp p[235 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateProcessA() {
        __asm { jmp p[236 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateProcessAsUserW() {
        __asm { jmp p[237 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateProcessInternalA() {
        __asm { jmp p[238 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateProcessInternalW() {
        __asm { jmp p[239 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateProcessW() {
        __asm { jmp p[240 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateRemoteThread() {
        __asm { jmp p[241 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateRemoteThreadEx() {
        __asm { jmp p[242 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateSemaphoreA() {
        __asm { jmp p[243 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateSemaphoreExA() {
        __asm { jmp p[244 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateSemaphoreExW() {
        __asm { jmp p[245 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateSemaphoreW() {
        __asm { jmp p[246 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateSocketHandle() {
        __asm { jmp p[247 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateStateAtom() {
        __asm { jmp p[248 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateStateChangeNotification() {
        __asm { jmp p[249 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateStateContainer() {
        __asm { jmp p[250 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateStateLock() {
        __asm { jmp p[251 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateStateSubcontainer() {
        __asm { jmp p[252 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateSymbolicLinkA() {
        __asm { jmp p[253 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateSymbolicLinkTransactedA() {
        __asm { jmp p[254 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateSymbolicLinkTransactedW() {
        __asm { jmp p[255 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateSymbolicLinkW() {
        __asm { jmp p[256 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateTapePartition() {
        __asm { jmp p[257 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateThread() {
        __asm { jmp p[258 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateThreadpool() {
        __asm { jmp p[259 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateThreadpoolCleanupGroup() {
        __asm { jmp p[260 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateThreadpoolIo() {
        __asm { jmp p[261 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateThreadpoolTimer() {
        __asm { jmp p[262 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateThreadpoolWait() {
        __asm { jmp p[263 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateThreadpoolWork() {
        __asm { jmp p[264 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateTimerQueue() {
        __asm { jmp p[265 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateTimerQueueTimer() {
        __asm { jmp p[266 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateToolhelp32Snapshot() {
        __asm { jmp p[267 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateWaitableTimerA() {
        __asm { jmp p[268 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateWaitableTimerExA() {
        __asm { jmp p[269 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateWaitableTimerExW() {
        __asm { jmp p[270 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CreateWaitableTimerW() {
        __asm { jmp p[271 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_CtrlRoutine() {
        __asm { jmp p[272 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeactivateActCtx() {
        __asm { jmp p[273 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeactivateActCtxWorker() {
        __asm { jmp p[274 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DebugActiveProcess() {
        __asm { jmp p[275 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DebugActiveProcessStop() {
        __asm { jmp p[276 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DebugBreak() {
        __asm { jmp p[277 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DebugBreakProcess() {
        __asm { jmp p[278 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DebugSetProcessKillOnExit() {
        __asm { jmp p[279 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DecodePointer() {
        __asm { jmp p[280 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DecodeSystemPointer() {
        __asm { jmp p[281 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DefineDosDeviceA() {
        __asm { jmp p[282 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DefineDosDeviceW() {
        __asm { jmp p[283 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DelayLoadFailureHook() {
        __asm { jmp p[284 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteAtom() {
        __asm { jmp p[285 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteBoundaryDescriptor() {
        __asm { jmp p[286 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteCriticalSection() {
        __asm { jmp p[287 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteFiber() {
        __asm { jmp p[288 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteFileA() {
        __asm { jmp p[289 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteFileTransactedA() {
        __asm { jmp p[290 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteFileTransactedW() {
        __asm { jmp p[291 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteFileW() {
        __asm { jmp p[292 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteProcThreadAttributeList() {
        __asm { jmp p[293 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteStateAtomValue() {
        __asm { jmp p[294 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteStateContainer() {
        __asm { jmp p[295 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteStateContainerValue() {
        __asm { jmp p[296 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteTimerQueue() {
        __asm { jmp p[297 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteTimerQueueEx() {
        __asm { jmp p[298 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteTimerQueueTimer() {
        __asm { jmp p[299 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteVolumeMountPointA() {
        __asm { jmp p[300 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DeleteVolumeMountPointW() {
        __asm { jmp p[301 * 4] }
    }
    /*
    __declspec( naked ) void WINAPI PROXY_DeviceIoControl() {
        __asm { jmp p[302 * 4] }
    }
    */
    __declspec( naked ) void WINAPI PROXY_DisableThreadLibraryCalls() {
        __asm { jmp p[303 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DisableThreadProfiling() {
        __asm { jmp p[304 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DisassociateCurrentThreadFromCallback() {
        __asm { jmp p[305 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DisconnectNamedPipe() {
        __asm { jmp p[306 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DnsHostnameToComputerNameA() {
        __asm { jmp p[307 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DnsHostnameToComputerNameW() {
        __asm { jmp p[308 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DosDateTimeToFileTime() {
        __asm { jmp p[309 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DosPathToSessionPathA() {
        __asm { jmp p[310 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DosPathToSessionPathW() {
        __asm { jmp p[311 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DuplicateConsoleHandle() {
        __asm { jmp p[312 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DuplicateEncryptionInfoFileExt() {
        __asm { jmp p[313 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DuplicateHandle() {
        __asm { jmp p[314 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_DuplicateStateContainerHandle() {
        __asm { jmp p[315 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnableThreadProfiling() {
        __asm { jmp p[316 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EncodePointer() {
        __asm { jmp p[317 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EncodeSystemPointer() {
        __asm { jmp p[318 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EndUpdateResourceA() {
        __asm { jmp p[319 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EndUpdateResourceW() {
        __asm { jmp p[320 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnterCriticalSection() {
        __asm { jmp p[321 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumCalendarInfoA() {
        __asm { jmp p[322 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumCalendarInfoExA() {
        __asm { jmp p[323 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumCalendarInfoExEx() {
        __asm { jmp p[324 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumCalendarInfoExW() {
        __asm { jmp p[325 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumCalendarInfoW() {
        __asm { jmp p[326 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumDateFormatsA() {
        __asm { jmp p[327 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumDateFormatsExA() {
        __asm { jmp p[328 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumDateFormatsExEx() {
        __asm { jmp p[329 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumDateFormatsExW() {
        __asm { jmp p[330 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumDateFormatsW() {
        __asm { jmp p[331 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumLanguageGroupLocalesA() {
        __asm { jmp p[332 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumLanguageGroupLocalesW() {
        __asm { jmp p[333 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumResourceLanguagesA() {
        __asm { jmp p[334 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumResourceLanguagesExA() {
        __asm { jmp p[335 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumResourceLanguagesExW() {
        __asm { jmp p[336 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumResourceLanguagesW() {
        __asm { jmp p[337 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumResourceNamesA() {
        __asm { jmp p[338 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumResourceNamesExA() {
        __asm { jmp p[339 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumResourceNamesExW() {
        __asm { jmp p[340 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumResourceNamesW() {
        __asm { jmp p[341 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumResourceTypesA() {
        __asm { jmp p[342 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumResourceTypesExA() {
        __asm { jmp p[343 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumResourceTypesExW() {
        __asm { jmp p[344 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumResourceTypesW() {
        __asm { jmp p[345 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumSystemCodePagesA() {
        __asm { jmp p[346 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumSystemCodePagesW() {
        __asm { jmp p[347 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumSystemFirmwareTables() {
        __asm { jmp p[348 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumSystemGeoID() {
        __asm { jmp p[349 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumSystemLanguageGroupsA() {
        __asm { jmp p[350 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumSystemLanguageGroupsW() {
        __asm { jmp p[351 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumSystemLocalesA() {
        __asm { jmp p[352 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumSystemLocalesEx() {
        __asm { jmp p[353 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumSystemLocalesW() {
        __asm { jmp p[354 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumTimeFormatsA() {
        __asm { jmp p[355 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumTimeFormatsEx() {
        __asm { jmp p[356 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumTimeFormatsW() {
        __asm { jmp p[357 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumUILanguagesA() {
        __asm { jmp p[358 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumUILanguagesW() {
        __asm { jmp p[359 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumerateLocalComputerNamesA() {
        __asm { jmp p[360 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumerateLocalComputerNamesW() {
        __asm { jmp p[361 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumerateStateAtomValues() {
        __asm { jmp p[362 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EnumerateStateContainerItems() {
        __asm { jmp p[363 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EraseTape() {
        __asm { jmp p[364 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_EscapeCommFunction() {
        __asm { jmp p[365 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ExitProcess() {
        __asm { jmp p[366 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ExitThread() {
        __asm { jmp p[367 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ExitVDM() {
        __asm { jmp p[368 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ExpandEnvironmentStringsA() {
        __asm { jmp p[369 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ExpandEnvironmentStringsW() {
        __asm { jmp p[370 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ExpungeConsoleCommandHistoryA() {
        __asm { jmp p[371 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ExpungeConsoleCommandHistoryW() {
        __asm { jmp p[372 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FatalAppExitA() {
        __asm { jmp p[373 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FatalAppExitW() {
        __asm { jmp p[374 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FatalExit() {
        __asm { jmp p[375 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FileTimeToDosDateTime() {
        __asm { jmp p[376 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FileTimeToLocalFileTime() {
        __asm { jmp p[377 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FileTimeToSystemTime() {
        __asm { jmp p[378 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FillConsoleOutputAttribute() {
        __asm { jmp p[379 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FillConsoleOutputCharacterA() {
        __asm { jmp p[380 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FillConsoleOutputCharacterW() {
        __asm { jmp p[381 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindActCtxSectionGuid() {
        __asm { jmp p[382 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindActCtxSectionGuidWorker() {
        __asm { jmp p[383 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindActCtxSectionStringA() {
        __asm { jmp p[384 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindActCtxSectionStringW() {
        __asm { jmp p[385 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindActCtxSectionStringWWorker() {
        __asm { jmp p[386 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindAtomA() {
        __asm { jmp p[387 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindAtomW() {
        __asm { jmp p[388 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindClose() {
        __asm { jmp p[389 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindCloseChangeNotification() {
        __asm { jmp p[390 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindFirstChangeNotificationA() {
        __asm { jmp p[391 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindFirstChangeNotificationW() {
        __asm { jmp p[392 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindFirstFileA() {
        __asm { jmp p[393 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindFirstFileExA() {
        __asm { jmp p[394 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindFirstFileExW() {
        __asm { jmp p[395 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindFirstFileNameTransactedW() {
        __asm { jmp p[396 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindFirstFileNameW() {
        __asm { jmp p[397 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindFirstFileTransactedA() {
        __asm { jmp p[398 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindFirstFileTransactedW() {
        __asm { jmp p[399 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindFirstFileW() {
        __asm { jmp p[400 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindFirstStreamTransactedW() {
        __asm { jmp p[401 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindFirstStreamW() {
        __asm { jmp p[402 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindFirstVolumeA() {
        __asm { jmp p[403 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindFirstVolumeMountPointA() {
        __asm { jmp p[404 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindFirstVolumeMountPointW() {
        __asm { jmp p[405 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindFirstVolumeW() {
        __asm { jmp p[406 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindNLSString() {
        __asm { jmp p[407 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindNLSStringEx() {
        __asm { jmp p[408 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindNextChangeNotification() {
        __asm { jmp p[409 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindNextFileA() {
        __asm { jmp p[410 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindNextFileNameW() {
        __asm { jmp p[411 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindNextFileW() {
        __asm { jmp p[412 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindNextStreamW() {
        __asm { jmp p[413 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindNextVolumeA() {
        __asm { jmp p[414 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindNextVolumeMountPointA() {
        __asm { jmp p[415 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindNextVolumeMountPointW() {
        __asm { jmp p[416 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindNextVolumeW() {
        __asm { jmp p[417 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindResourceA() {
        __asm { jmp p[418 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindResourceExA() {
        __asm { jmp p[419 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindResourceExW() {
        __asm { jmp p[420 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindResourceW() {
        __asm { jmp p[421 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindStringOrdinal() {
        __asm { jmp p[422 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindVolumeClose() {
        __asm { jmp p[423 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FindVolumeMountPointClose() {
        __asm { jmp p[424 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FlsAlloc() {
        __asm { jmp p[425 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FlsFree() {
        __asm { jmp p[426 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FlsGetValue() {
        __asm { jmp p[427 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FlsSetValue() {
        __asm { jmp p[428 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FlushConsoleInputBuffer() {
        __asm { jmp p[429 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FlushFileBuffers() {
        __asm { jmp p[430 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FlushInstructionCache() {
        __asm { jmp p[431 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FlushProcessWriteBuffers() {
        __asm { jmp p[432 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FlushViewOfFile() {
        __asm { jmp p[433 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FoldStringA() {
        __asm { jmp p[434 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FoldStringW() {
        __asm { jmp p[435 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FormatMessageA() {
        __asm { jmp p[436 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FormatMessageW() {
        __asm { jmp p[437 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FreeConsole() {
        __asm { jmp p[438 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FreeEnvironmentStringsA() {
        __asm { jmp p[439 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FreeEnvironmentStringsW() {
        __asm { jmp p[440 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FreeLibrary() {
        __asm { jmp p[441 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FreeLibraryAndExitThread() {
        __asm { jmp p[442 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FreeLibraryWhenCallbackReturns() {
        __asm { jmp p[443 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FreeResource() {
        __asm { jmp p[444 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_FreeUserPhysicalPages() {
        __asm { jmp p[445 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GenerateConsoleCtrlEvent() {
        __asm { jmp p[446 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetACP() {
        __asm { jmp p[447 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetActiveProcessorCount() {
        __asm { jmp p[448 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetActiveProcessorGroupCount() {
        __asm { jmp p[449 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetAppContainerAce() {
        __asm { jmp p[450 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetAppContainerNamedObjectPath() {
        __asm { jmp p[451 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetApplicationRecoveryCallback() {
        __asm { jmp p[452 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetApplicationRecoveryCallbackWorker() {
        __asm { jmp p[453 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetApplicationRestartSettings() {
        __asm { jmp p[454 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetApplicationRestartSettingsWorker() {
        __asm { jmp p[455 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetApplicationUserModelId() {
        __asm { jmp p[456 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetAtomNameA() {
        __asm { jmp p[457 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetAtomNameW() {
        __asm { jmp p[458 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetBinaryType() {
        __asm { jmp p[459 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetBinaryTypeA() {
        __asm { jmp p[460 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetBinaryTypeW() {
        __asm { jmp p[461 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCPInfo() {
        __asm { jmp p[462 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCPInfoExA() {
        __asm { jmp p[463 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCPInfoExW() {
        __asm { jmp p[464 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCachedSigningLevel() {
        __asm { jmp p[465 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCalendarDateFormat() {
        __asm { jmp p[466 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCalendarDateFormatEx() {
        __asm { jmp p[467 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCalendarDaysInMonth() {
        __asm { jmp p[468 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCalendarDifferenceInDays() {
        __asm { jmp p[469 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCalendarInfoA() {
        __asm { jmp p[470 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCalendarInfoEx() {
        __asm { jmp p[471 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCalendarInfoW() {
        __asm { jmp p[472 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCalendarMonthsInYear() {
        __asm { jmp p[473 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCalendarSupportedDateRange() {
        __asm { jmp p[474 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCalendarWeekNumber() {
        __asm { jmp p[475 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetComPlusPackageInstallStatus() {
        __asm { jmp p[476 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCommConfig() {
        __asm { jmp p[477 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCommMask() {
        __asm { jmp p[478 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCommModemStatus() {
        __asm { jmp p[479 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCommProperties() {
        __asm { jmp p[480 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCommState() {
        __asm { jmp p[481 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCommTimeouts() {
        __asm { jmp p[482 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCommandLineA() {
        __asm { jmp p[483 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCommandLineW() {
        __asm { jmp p[484 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCompressedFileSizeA() {
        __asm { jmp p[485 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCompressedFileSizeTransactedA() {
        __asm { jmp p[486 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCompressedFileSizeTransactedW() {
        __asm { jmp p[487 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCompressedFileSizeW() {
        __asm { jmp p[488 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetComputerNameA() {
        __asm { jmp p[489 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetComputerNameExA() {
        __asm { jmp p[490 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetComputerNameExW() {
        __asm { jmp p[491 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetComputerNameW() {
        __asm { jmp p[492 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleAliasA() {
        __asm { jmp p[493 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleAliasExesA() {
        __asm { jmp p[494 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleAliasExesLengthA() {
        __asm { jmp p[495 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleAliasExesLengthW() {
        __asm { jmp p[496 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleAliasExesW() {
        __asm { jmp p[497 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleAliasW() {
        __asm { jmp p[498 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleAliasesA() {
        __asm { jmp p[499 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleAliasesLengthA() {
        __asm { jmp p[500 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleAliasesLengthW() {
        __asm { jmp p[501 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleAliasesW() {
        __asm { jmp p[502 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleCP() {
        __asm { jmp p[503 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleCharType() {
        __asm { jmp p[504 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleCommandHistoryA() {
        __asm { jmp p[505 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleCommandHistoryLengthA() {
        __asm { jmp p[506 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleCommandHistoryLengthW() {
        __asm { jmp p[507 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleCommandHistoryW() {
        __asm { jmp p[508 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleCursorInfo() {
        __asm { jmp p[509 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleCursorMode() {
        __asm { jmp p[510 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleDisplayMode() {
        __asm { jmp p[511 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleFontInfo() {
        __asm { jmp p[512 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleFontSize() {
        __asm { jmp p[513 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleHardwareState() {
        __asm { jmp p[514 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleHistoryInfo() {
        __asm { jmp p[515 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleInputExeNameA() {
        __asm { jmp p[516 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleInputExeNameW() {
        __asm { jmp p[517 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleInputWaitHandle() {
        __asm { jmp p[518 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleKeyboardLayoutNameA() {
        __asm { jmp p[519 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleKeyboardLayoutNameW() {
        __asm { jmp p[520 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleMode() {
        __asm { jmp p[521 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleNlsMode() {
        __asm { jmp p[522 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleOriginalTitleA() {
        __asm { jmp p[523 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleOriginalTitleW() {
        __asm { jmp p[524 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleOutputCP() {
        __asm { jmp p[525 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleProcessList() {
        __asm { jmp p[526 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleScreenBufferInfo() {
        __asm { jmp p[527 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleScreenBufferInfoEx() {
        __asm { jmp p[528 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleSelectionInfo() {
        __asm { jmp p[529 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleTitleA() {
        __asm { jmp p[530 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleTitleW() {
        __asm { jmp p[531 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetConsoleWindow() {
        __asm { jmp p[532 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrencyFormatA() {
        __asm { jmp p[533 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrencyFormatEx() {
        __asm { jmp p[534 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrencyFormatW() {
        __asm { jmp p[535 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentActCtx() {
        __asm { jmp p[536 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentActCtxWorker() {
        __asm { jmp p[537 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentApplicationUserModelId() {
        __asm { jmp p[538 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentConsoleFont() {
        __asm { jmp p[539 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentConsoleFontEx() {
        __asm { jmp p[540 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentDirectoryA() {
        __asm { jmp p[541 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentDirectoryW() {
        __asm { jmp p[542 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentPackageFamilyName() {
        __asm { jmp p[543 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentPackageFullName() {
        __asm { jmp p[544 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentPackageId() {
        __asm { jmp p[545 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentPackageInfo() {
        __asm { jmp p[546 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentPackagePath() {
        __asm { jmp p[547 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentProcess() {
        __asm { jmp p[548 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentProcessId() {
        __asm { jmp p[549 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentProcessorNumber() {
        __asm { jmp p[550 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentProcessorNumberEx() {
        __asm { jmp p[551 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentThread() {
        __asm { jmp p[552 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentThreadId() {
        __asm { jmp p[553 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetCurrentThreadStackLimits() {
        __asm { jmp p[554 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDateFormatA() {
        __asm { jmp p[555 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDateFormatAWorker() {
        __asm { jmp p[556 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDateFormatEx() {
        __asm { jmp p[557 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDateFormatW() {
        __asm { jmp p[558 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDateFormatWWorker() {
        __asm { jmp p[559 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDefaultCommConfigA() {
        __asm { jmp p[560 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDefaultCommConfigW() {
        __asm { jmp p[561 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDevicePowerState() {
        __asm { jmp p[562 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDiskFreeSpaceA() {
        __asm { jmp p[563 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDiskFreeSpaceExA() {
        __asm { jmp p[564 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDiskFreeSpaceExW() {
        __asm { jmp p[565 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDiskFreeSpaceW() {
        __asm { jmp p[566 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDllDirectoryA() {
        __asm { jmp p[567 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDllDirectoryW() {
        __asm { jmp p[568 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDriveTypeA() {
        __asm { jmp p[569 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDriveTypeW() {
        __asm { jmp p[570 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDurationFormat() {
        __asm { jmp p[571 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDurationFormatEx() {
        __asm { jmp p[572 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetDynamicTimeZoneInformation() {
        __asm { jmp p[573 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetEnabledXStateFeatures() {
        __asm { jmp p[574 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetEnvironmentStrings() {
        __asm { jmp p[575 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetEnvironmentStringsA() {
        __asm { jmp p[576 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetEnvironmentStringsW() {
        __asm { jmp p[577 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetEnvironmentVariableA() {
        __asm { jmp p[578 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetEnvironmentVariableW() {
        __asm { jmp p[579 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetEraNameCountedString() {
        __asm { jmp p[580 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetErrorMode() {
        __asm { jmp p[581 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetExitCodeProcess() {
        __asm { jmp p[582 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetExitCodeThread() {
        __asm { jmp p[583 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetExpandedNameA() {
        __asm { jmp p[584 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetExpandedNameW() {
        __asm { jmp p[585 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFileAttributesA() {
        __asm { jmp p[586 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFileAttributesExA() {
        __asm { jmp p[587 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFileAttributesExW() {
        __asm { jmp p[588 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFileAttributesTransactedA() {
        __asm { jmp p[589 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFileAttributesTransactedW() {
        __asm { jmp p[590 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFileAttributesW() {
        __asm { jmp p[591 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFileBandwidthReservation() {
        __asm { jmp p[592 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFileInformationByHandle() {
        __asm { jmp p[593 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFileInformationByHandleEx() {
        __asm { jmp p[594 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFileMUIInfo() {
        __asm { jmp p[595 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFileMUIPath() {
        __asm { jmp p[596 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFileSize() {
        __asm { jmp p[597 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFileSizeEx() {
        __asm { jmp p[598 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFileTime() {
        __asm { jmp p[599 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFileType() {
        __asm { jmp p[600 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFinalPathNameByHandleA() {
        __asm { jmp p[601 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFinalPathNameByHandleW() {
        __asm { jmp p[602 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFirmwareEnvironmentVariableA() {
        __asm { jmp p[603 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFirmwareEnvironmentVariableExA() {
        __asm { jmp p[604 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFirmwareEnvironmentVariableExW() {
        __asm { jmp p[605 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFirmwareEnvironmentVariableW() {
        __asm { jmp p[606 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFirmwareType() {
        __asm { jmp p[607 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFullPathNameA() {
        __asm { jmp p[608 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFullPathNameTransactedA() {
        __asm { jmp p[609 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFullPathNameTransactedW() {
        __asm { jmp p[610 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetFullPathNameW() {
        __asm { jmp p[611 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetGeoInfoA() {
        __asm { jmp p[612 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetGeoInfoW() {
        __asm { jmp p[613 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetHandleContext() {
        __asm { jmp p[614 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetHandleInformation() {
        __asm { jmp p[615 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetHivePath() {
        __asm { jmp p[616 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetLargePageMinimum() {
        __asm { jmp p[617 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetLargestConsoleWindowSize() {
        __asm { jmp p[618 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetLastError() {
        __asm { jmp p[619 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetLocalTime() {
        __asm { jmp p[620 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetLocaleInfoA() {
        __asm { jmp p[621 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetLocaleInfoEx() {
        __asm { jmp p[622 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetLocaleInfoW() {
        __asm { jmp p[623 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetLogicalDriveStringsA() {
        __asm { jmp p[624 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetLogicalDriveStringsW() {
        __asm { jmp p[625 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetLogicalDrives() {
        __asm { jmp p[626 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetLogicalProcessorInformation() {
        __asm { jmp p[627 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetLogicalProcessorInformationEx() {
        __asm { jmp p[628 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetLongPathNameA() {
        __asm { jmp p[629 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetLongPathNameTransactedA() {
        __asm { jmp p[630 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetLongPathNameTransactedW() {
        __asm { jmp p[631 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetLongPathNameW() {
        __asm { jmp p[632 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetMailslotInfo() {
        __asm { jmp p[633 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetMaximumProcessorCount() {
        __asm { jmp p[634 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetMaximumProcessorGroupCount() {
        __asm { jmp p[635 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetMemoryErrorHandlingCapabilities() {
        __asm { jmp p[636 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetModuleFileNameA() {
        __asm { jmp p[637 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetModuleFileNameW() {
        __asm { jmp p[638 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetModuleHandleA() {
        __asm { jmp p[639 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetModuleHandleExA() {
        __asm { jmp p[640 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetModuleHandleExW() {
        __asm { jmp p[641 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetModuleHandleW() {
        __asm { jmp p[642 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNLSVersion() {
        __asm { jmp p[643 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNLSVersionEx() {
        __asm { jmp p[644 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNamedPipeAttribute() {
        __asm { jmp p[645 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNamedPipeClientComputerNameA() {
        __asm { jmp p[646 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNamedPipeClientComputerNameW() {
        __asm { jmp p[647 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNamedPipeClientProcessId() {
        __asm { jmp p[648 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNamedPipeClientSessionId() {
        __asm { jmp p[649 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNamedPipeHandleStateA() {
        __asm { jmp p[650 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNamedPipeHandleStateW() {
        __asm { jmp p[651 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNamedPipeInfo() {
        __asm { jmp p[652 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNamedPipeServerProcessId() {
        __asm { jmp p[653 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNamedPipeServerSessionId() {
        __asm { jmp p[654 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNativeSystemInfo() {
        __asm { jmp p[655 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNextVDMCommand() {
        __asm { jmp p[656 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNumaAvailableMemoryNode() {
        __asm { jmp p[657 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNumaAvailableMemoryNodeEx() {
        __asm { jmp p[658 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNumaHighestNodeNumber() {
        __asm { jmp p[659 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNumaNodeNumberFromHandle() {
        __asm { jmp p[660 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNumaNodeProcessorMask() {
        __asm { jmp p[661 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNumaNodeProcessorMaskEx() {
        __asm { jmp p[662 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNumaProcessorNode() {
        __asm { jmp p[663 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNumaProcessorNodeEx() {
        __asm { jmp p[664 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNumaProximityNode() {
        __asm { jmp p[665 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNumaProximityNodeEx() {
        __asm { jmp p[666 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNumberFormatA() {
        __asm { jmp p[667 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNumberFormatEx() {
        __asm { jmp p[668 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNumberFormatW() {
        __asm { jmp p[669 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNumberOfConsoleFonts() {
        __asm { jmp p[670 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNumberOfConsoleInputEvents() {
        __asm { jmp p[671 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetNumberOfConsoleMouseButtons() {
        __asm { jmp p[672 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetOEMCP() {
        __asm { jmp p[673 * 4] }
    }
    /*
    __declspec( naked ) void WINAPI PROXY_GetOverlappedResult() {
        __asm { jmp p[674 * 4] }
    }
    */
    __declspec( naked ) void WINAPI PROXY_GetOverlappedResultEx() {
        __asm { jmp p[675 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPackageFamilyName() {
        __asm { jmp p[676 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPackageFullName() {
        __asm { jmp p[677 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPackageId() {
        __asm { jmp p[678 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPackageInfo() {
        __asm { jmp p[679 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPackagePath() {
        __asm { jmp p[680 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPackagesByPackageFamily() {
        __asm { jmp p[681 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPhysicallyInstalledSystemMemory() {
        __asm { jmp p[682 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPriorityClass() {
        __asm { jmp p[683 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPrivateProfileIntA() {
        __asm { jmp p[684 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPrivateProfileIntW() {
        __asm { jmp p[685 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPrivateProfileSectionA() {
        __asm { jmp p[686 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPrivateProfileSectionNamesA() {
        __asm { jmp p[687 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPrivateProfileSectionNamesW() {
        __asm { jmp p[688 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPrivateProfileSectionW() {
        __asm { jmp p[689 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPrivateProfileStringA() {
        __asm { jmp p[690 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPrivateProfileStringW() {
        __asm { jmp p[691 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPrivateProfileStructA() {
        __asm { jmp p[692 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetPrivateProfileStructW() {
        __asm { jmp p[693 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcAddress() {
        __asm { jmp p[694 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessAffinityMask() {
        __asm { jmp p[695 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessDEPPolicy() {
        __asm { jmp p[696 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessGroupAffinity() {
        __asm { jmp p[697 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessHandleCount() {
        __asm { jmp p[698 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessHeap() {
        __asm { jmp p[699 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessHeaps() {
        __asm { jmp p[700 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessId() {
        __asm { jmp p[701 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessIdOfThread() {
        __asm { jmp p[702 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessInformation() {
        __asm { jmp p[703 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessIoCounters() {
        __asm { jmp p[704 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessMitigationPolicy() {
        __asm { jmp p[705 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessPreferredUILanguages() {
        __asm { jmp p[706 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessPriorityBoost() {
        __asm { jmp p[707 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessShutdownParameters() {
        __asm { jmp p[708 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessTimes() {
        __asm { jmp p[709 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessVersion() {
        __asm { jmp p[710 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessWorkingSetSize() {
        __asm { jmp p[711 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessWorkingSetSizeEx() {
        __asm { jmp p[712 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProcessorSystemCycleTime() {
        __asm { jmp p[713 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProductInfo() {
        __asm { jmp p[714 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProfileIntA() {
        __asm { jmp p[715 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProfileIntW() {
        __asm { jmp p[716 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProfileSectionA() {
        __asm { jmp p[717 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProfileSectionW() {
        __asm { jmp p[718 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProfileStringA() {
        __asm { jmp p[719 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetProfileStringW() {
        __asm { jmp p[720 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetQueuedCompletionStatus() {
        __asm { jmp p[721 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetQueuedCompletionStatusEx() {
        __asm { jmp p[722 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetRoamingLastObservedChangeTime() {
        __asm { jmp p[723 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSerializedAtomBytes() {
        __asm { jmp p[724 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetShortPathNameA() {
        __asm { jmp p[725 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetShortPathNameW() {
        __asm { jmp p[726 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetStartupInfoA() {
        __asm { jmp p[727 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetStartupInfoW() {
        __asm { jmp p[728 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetStateContainerDepth() {
        __asm { jmp p[729 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetStateFolder() {
        __asm { jmp p[730 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetStateRootFolder() {
        __asm { jmp p[731 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetStateSettingsFolder() {
        __asm { jmp p[732 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetStateVersion() {
        __asm { jmp p[733 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetStdHandle() {
        __asm { jmp p[734 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetStringScripts() {
        __asm { jmp p[735 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetStringTypeA() {
        __asm { jmp p[736 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetStringTypeExA() {
        __asm { jmp p[737 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetStringTypeExW() {
        __asm { jmp p[738 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetStringTypeW() {
        __asm { jmp p[739 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemAppDataFolder() {
        __asm { jmp p[740 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemAppDataKey() {
        __asm { jmp p[741 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemDEPPolicy() {
        __asm { jmp p[742 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemDefaultLCID() {
        __asm { jmp p[743 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemDefaultLangID() {
        __asm { jmp p[744 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemDefaultLocaleName() {
        __asm { jmp p[745 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemDefaultUILanguage() {
        __asm { jmp p[746 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemDirectoryA() {
        __asm { jmp p[747 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemDirectoryW() {
        __asm { jmp p[748 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemFileCacheSize() {
        __asm { jmp p[749 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemFirmwareTable() {
        __asm { jmp p[750 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemInfo() {
        __asm { jmp p[751 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemPowerStatus() {
        __asm { jmp p[752 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemPreferredUILanguages() {
        __asm { jmp p[753 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemRegistryQuota() {
        __asm { jmp p[754 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemTime() {
        __asm { jmp p[755 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemTimeAdjustment() {
        __asm { jmp p[756 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemTimeAsFileTime() {
        __asm { jmp p[757 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemTimePreciseAsFileTime() {
        __asm { jmp p[758 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemTimes() {
        __asm { jmp p[759 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemWindowsDirectoryA() {
        __asm { jmp p[760 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemWindowsDirectoryW() {
        __asm { jmp p[761 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemWow64DirectoryA() {
        __asm { jmp p[762 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetSystemWow64DirectoryW() {
        __asm { jmp p[763 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetTapeParameters() {
        __asm { jmp p[764 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetTapePosition() {
        __asm { jmp p[765 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetTapeStatus() {
        __asm { jmp p[766 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetTempFileNameA() {
        __asm { jmp p[767 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetTempFileNameW() {
        __asm { jmp p[768 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetTempPathA() {
        __asm { jmp p[769 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetTempPathW() {
        __asm { jmp p[770 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetThreadContext() {
        __asm { jmp p[771 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetThreadErrorMode() {
        __asm { jmp p[772 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetThreadGroupAffinity() {
        __asm { jmp p[773 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetThreadIOPendingFlag() {
        __asm { jmp p[774 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetThreadId() {
        __asm { jmp p[775 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetThreadIdealProcessorEx() {
        __asm { jmp p[776 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetThreadInformation() {
        __asm { jmp p[777 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetThreadLocale() {
        __asm { jmp p[778 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetThreadPreferredUILanguages() {
        __asm { jmp p[779 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetThreadPriority() {
        __asm { jmp p[780 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetThreadPriorityBoost() {
        __asm { jmp p[781 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetThreadSelectorEntry() {
        __asm { jmp p[782 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetThreadTimes() {
        __asm { jmp p[783 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetThreadUILanguage() {
        __asm { jmp p[784 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetTickCount64() {
        __asm { jmp p[785 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetTickCount() {
        __asm { jmp p[786 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetTimeFormatA() {
        __asm { jmp p[787 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetTimeFormatAWorker() {
        __asm { jmp p[788 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetTimeFormatEx() {
        __asm { jmp p[789 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetTimeFormatW() {
        __asm { jmp p[790 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetTimeFormatWWorker() {
        __asm { jmp p[791 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetTimeZoneInformation() {
        __asm { jmp p[792 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetTimeZoneInformationForYear() {
        __asm { jmp p[793 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetUILanguageInfo() {
        __asm { jmp p[794 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetUserDefaultLCID() {
        __asm { jmp p[795 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetUserDefaultLangID() {
        __asm { jmp p[796 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetUserDefaultLocaleName() {
        __asm { jmp p[797 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetUserDefaultUILanguage() {
        __asm { jmp p[798 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetUserGeoID() {
        __asm { jmp p[799 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetUserPreferredUILanguages() {
        __asm { jmp p[800 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetVDMCurrentDirectories() {
        __asm { jmp p[801 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetVersion() {
        __asm { jmp p[802 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetVersionExA() {
        __asm { jmp p[803 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetVersionExW() {
        __asm { jmp p[804 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetVolumeInformationA() {
        __asm { jmp p[805 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetVolumeInformationByHandleW() {
        __asm { jmp p[806 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetVolumeInformationW() {
        __asm { jmp p[807 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetVolumeNameForVolumeMountPointA() {
        __asm { jmp p[808 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetVolumeNameForVolumeMountPointW() {
        __asm { jmp p[809 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetVolumePathNameA() {
        __asm { jmp p[810 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetVolumePathNameW() {
        __asm { jmp p[811 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetVolumePathNamesForVolumeNameA() {
        __asm { jmp p[812 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetVolumePathNamesForVolumeNameW() {
        __asm { jmp p[813 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetWindowsDirectoryA() {
        __asm { jmp p[814 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetWindowsDirectoryW() {
        __asm { jmp p[815 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetWriteWatch() {
        __asm { jmp p[816 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GetXStateFeaturesMask() {
        __asm { jmp p[817 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalAddAtomA() {
        __asm { jmp p[818 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalAddAtomExA() {
        __asm { jmp p[819 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalAddAtomExW() {
        __asm { jmp p[820 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalAddAtomW() {
        __asm { jmp p[821 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalAlloc() {
        __asm { jmp p[822 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalCompact() {
        __asm { jmp p[823 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalDeleteAtom() {
        __asm { jmp p[824 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalFindAtomA() {
        __asm { jmp p[825 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalFindAtomW() {
        __asm { jmp p[826 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalFix() {
        __asm { jmp p[827 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalFlags() {
        __asm { jmp p[828 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalFree() {
        __asm { jmp p[829 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalGetAtomNameA() {
        __asm { jmp p[830 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalGetAtomNameW() {
        __asm { jmp p[831 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalHandle() {
        __asm { jmp p[832 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalLock() {
        __asm { jmp p[833 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalMemoryStatus() {
        __asm { jmp p[834 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalMemoryStatusEx() {
        __asm { jmp p[835 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalReAlloc() {
        __asm { jmp p[836 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalSize() {
        __asm { jmp p[837 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalUnWire() {
        __asm { jmp p[838 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalUnfix() {
        __asm { jmp p[839 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalUnlock() {
        __asm { jmp p[840 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_GlobalWire() {
        __asm { jmp p[841 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Heap32First() {
        __asm { jmp p[842 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Heap32ListFirst() {
        __asm { jmp p[843 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Heap32ListNext() {
        __asm { jmp p[844 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Heap32Next() {
        __asm { jmp p[845 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_HeapAlloc() {
        __asm { jmp p[846 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_HeapCompact() {
        __asm { jmp p[847 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_HeapCreate() {
        __asm { jmp p[848 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_HeapDestroy() {
        __asm { jmp p[849 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_HeapFree() {
        __asm { jmp p[850 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_HeapLock() {
        __asm { jmp p[851 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_HeapQueryInformation() {
        __asm { jmp p[852 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_HeapReAlloc() {
        __asm { jmp p[853 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_HeapSetInformation() {
        __asm { jmp p[854 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_HeapSize() {
        __asm { jmp p[855 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_HeapSummary() {
        __asm { jmp p[856 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_HeapUnlock() {
        __asm { jmp p[857 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_HeapValidate() {
        __asm { jmp p[858 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_HeapWalk() {
        __asm { jmp p[859 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IdnToAscii() {
        __asm { jmp p[860 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IdnToNameprepUnicode() {
        __asm { jmp p[861 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IdnToUnicode() {
        __asm { jmp p[862 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InitAtomTable() {
        __asm { jmp p[863 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InitOnceBeginInitialize() {
        __asm { jmp p[864 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InitOnceComplete() {
        __asm { jmp p[865 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InitOnceExecuteOnce() {
        __asm { jmp p[866 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InitOnceInitialize() {
        __asm { jmp p[867 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InitializeConditionVariable() {
        __asm { jmp p[868 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InitializeContext() {
        __asm { jmp p[869 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InitializeCriticalSection() {
        __asm { jmp p[870 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InitializeCriticalSectionAndSpinCount() {
        __asm { jmp p[871 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InitializeCriticalSectionEx() {
        __asm { jmp p[872 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InitializeProcThreadAttributeList() {
        __asm { jmp p[873 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InitializeSListHead() {
        __asm { jmp p[874 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InitializeSRWLock() {
        __asm { jmp p[875 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InterlockedCompareExchange64() {
        __asm { jmp p[876 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InterlockedCompareExchange() {
        __asm { jmp p[877 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InterlockedDecrement() {
        __asm { jmp p[878 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InterlockedExchange() {
        __asm { jmp p[879 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InterlockedExchangeAdd() {
        __asm { jmp p[880 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InterlockedFlushSList() {
        __asm { jmp p[881 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InterlockedIncrement() {
        __asm { jmp p[882 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InterlockedPopEntrySList() {
        __asm { jmp p[883 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InterlockedPushEntrySList() {
        __asm { jmp p[884 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InterlockedPushListSListEx() {
        __asm { jmp p[885 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_InvalidateConsoleDIBits() {
        __asm { jmp p[886 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsBadCodePtr() {
        __asm { jmp p[887 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsBadHugeReadPtr() {
        __asm { jmp p[888 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsBadHugeWritePtr() {
        __asm { jmp p[889 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsBadReadPtr() {
        __asm { jmp p[890 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsBadStringPtrA() {
        __asm { jmp p[891 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsBadStringPtrW() {
        __asm { jmp p[892 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsBadWritePtr() {
        __asm { jmp p[893 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsCalendarLeapDay() {
        __asm { jmp p[894 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsCalendarLeapMonth() {
        __asm { jmp p[895 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsCalendarLeapYear() {
        __asm { jmp p[896 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsDBCSLeadByte() {
        __asm { jmp p[897 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsDBCSLeadByteEx() {
        __asm { jmp p[898 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsDebuggerPresent() {
        __asm { jmp p[899 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsNLSDefinedString() {
        __asm { jmp p[900 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsNativeVhdBoot() {
        __asm { jmp p[901 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsNormalizedString() {
        __asm { jmp p[902 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsProcessInJob() {
        __asm { jmp p[903 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsProcessorFeaturePresent() {
        __asm { jmp p[904 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsSystemResumeAutomatic() {
        __asm { jmp p[905 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsThreadAFiber() {
        __asm { jmp p[906 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsThreadpoolTimerSet() {
        __asm { jmp p[907 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsValidCalDateTime() {
        __asm { jmp p[908 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsValidCodePage() {
        __asm { jmp p[909 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsValidLanguageGroup() {
        __asm { jmp p[910 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsValidLocale() {
        __asm { jmp p[911 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsValidLocaleName() {
        __asm { jmp p[912 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsValidNLSVersion() {
        __asm { jmp p[913 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_IsWow64Process() {
        __asm { jmp p[914 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32EmptyWorkingSet() {
        __asm { jmp p[915 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32EnumDeviceDrivers() {
        __asm { jmp p[916 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32EnumPageFilesA() {
        __asm { jmp p[917 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32EnumPageFilesW() {
        __asm { jmp p[918 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32EnumProcessModules() {
        __asm { jmp p[919 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32EnumProcessModulesEx() {
        __asm { jmp p[920 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32EnumProcesses() {
        __asm { jmp p[921 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetDeviceDriverBaseNameA() {
        __asm { jmp p[922 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetDeviceDriverBaseNameW() {
        __asm { jmp p[923 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetDeviceDriverFileNameA() {
        __asm { jmp p[924 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetDeviceDriverFileNameW() {
        __asm { jmp p[925 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetMappedFileNameA() {
        __asm { jmp p[926 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetMappedFileNameW() {
        __asm { jmp p[927 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetModuleBaseNameA() {
        __asm { jmp p[928 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetModuleBaseNameW() {
        __asm { jmp p[929 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetModuleFileNameExA() {
        __asm { jmp p[930 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetModuleFileNameExW() {
        __asm { jmp p[931 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetModuleInformation() {
        __asm { jmp p[932 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetPerformanceInfo() {
        __asm { jmp p[933 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetProcessImageFileNameA() {
        __asm { jmp p[934 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetProcessImageFileNameW() {
        __asm { jmp p[935 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetProcessMemoryInfo() {
        __asm { jmp p[936 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetWsChanges() {
        __asm { jmp p[937 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32GetWsChangesEx() {
        __asm { jmp p[938 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32InitializeProcessForWsWatch() {
        __asm { jmp p[939 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32QueryWorkingSet() {
        __asm { jmp p[940 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_K32QueryWorkingSetEx() {
        __asm { jmp p[941 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LCIDToLocaleName() {
        __asm { jmp p[942 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LCMapStringA() {
        __asm { jmp p[943 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LCMapStringEx() {
        __asm { jmp p[944 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LCMapStringW() {
        __asm { jmp p[945 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LZClose() {
        __asm { jmp p[946 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LZCloseFile() {
        __asm { jmp p[947 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LZCopy() {
        __asm { jmp p[948 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LZCreateFileW() {
        __asm { jmp p[949 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LZDone() {
        __asm { jmp p[950 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LZInit() {
        __asm { jmp p[951 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LZOpenFileA() {
        __asm { jmp p[952 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LZOpenFileW() {
        __asm { jmp p[953 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LZRead() {
        __asm { jmp p[954 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LZSeek() {
        __asm { jmp p[955 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LZStart() {
        __asm { jmp p[956 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LeaveCriticalSection() {
        __asm { jmp p[957 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LeaveCriticalSectionWhenCallbackReturns() {
        __asm { jmp p[958 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LoadAppInitDlls() {
        __asm { jmp p[959 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LoadLibraryA() {
        __asm { jmp p[960 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LoadLibraryExA() {
        __asm { jmp p[961 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LoadLibraryExW() {
        __asm { jmp p[962 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LoadLibraryW() {
        __asm { jmp p[963 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LoadModule() {
        __asm { jmp p[964 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LoadPackagedLibrary() {
        __asm { jmp p[965 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LoadResource() {
        __asm { jmp p[966 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LoadStringBaseExW() {
        __asm { jmp p[967 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LoadStringBaseW() {
        __asm { jmp p[968 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LocalAlloc() {
        __asm { jmp p[969 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LocalCompact() {
        __asm { jmp p[970 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LocalFileTimeToFileTime() {
        __asm { jmp p[971 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LocalFlags() {
        __asm { jmp p[972 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LocalFree() {
        __asm { jmp p[973 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LocalHandle() {
        __asm { jmp p[974 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LocalLock() {
        __asm { jmp p[975 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LocalReAlloc() {
        __asm { jmp p[976 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LocalShrink() {
        __asm { jmp p[977 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LocalSize() {
        __asm { jmp p[978 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LocalUnlock() {
        __asm { jmp p[979 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LocaleNameToLCID() {
        __asm { jmp p[980 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LocateXStateFeature() {
        __asm { jmp p[981 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LockFile() {
        __asm { jmp p[982 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LockFileEx() {
        __asm { jmp p[983 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_LockResource() {
        __asm { jmp p[984 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_MapUserPhysicalPages() {
        __asm { jmp p[985 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_MapUserPhysicalPagesScatter() {
        __asm { jmp p[986 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_MapViewOfFile() {
        __asm { jmp p[987 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_MapViewOfFileEx() {
        __asm { jmp p[988 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_MapViewOfFileExNuma() {
        __asm { jmp p[989 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_MapViewOfFileFromApp() {
        __asm { jmp p[990 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Module32First() {
        __asm { jmp p[991 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Module32FirstW() {
        __asm { jmp p[992 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Module32Next() {
        __asm { jmp p[993 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Module32NextW() {
        __asm { jmp p[994 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_MoveFileA() {
        __asm { jmp p[995 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_MoveFileExA() {
        __asm { jmp p[996 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_MoveFileExW() {
        __asm { jmp p[997 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_MoveFileTransactedA() {
        __asm { jmp p[998 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_MoveFileTransactedW() {
        __asm { jmp p[999 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_MoveFileW() {
        __asm { jmp p[1000 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_MoveFileWithProgressA() {
        __asm { jmp p[1001 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_MoveFileWithProgressW() {
        __asm { jmp p[1002 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_MulDiv() {
        __asm { jmp p[1003 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_MultiByteToWideChar() {
        __asm { jmp p[1004 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_NeedCurrentDirectoryForExePathA() {
        __asm { jmp p[1005 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_NeedCurrentDirectoryForExePathW() {
        __asm { jmp p[1006 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_NlsCheckPolicy() {
        __asm { jmp p[1007 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_NlsEventDataDescCreate() {
        __asm { jmp p[1008 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_NlsGetCacheUpdateCount() {
        __asm { jmp p[1009 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_NlsUpdateLocale() {
        __asm { jmp p[1010 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_NlsUpdateSystemLocale() {
        __asm { jmp p[1011 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_NlsWriteEtwEvent() {
        __asm { jmp p[1012 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_NormalizeString() {
        __asm { jmp p[1013 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_NotifyMountMgr() {
        __asm { jmp p[1014 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_NotifyUILanguageChange() {
        __asm { jmp p[1015 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_NtVdm64CreateProcessInternalW() {
        __asm { jmp p[1016 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenConsoleW() {
        __asm { jmp p[1017 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenConsoleWStub() {
        __asm { jmp p[1018 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenEventA() {
        __asm { jmp p[1019 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenEventW() {
        __asm { jmp p[1020 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenFile() {
        __asm { jmp p[1021 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenFileById() {
        __asm { jmp p[1022 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenFileMappingA() {
        __asm { jmp p[1023 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenFileMappingW() {
        __asm { jmp p[1024 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenJobObjectA() {
        __asm { jmp p[1025 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenJobObjectW() {
        __asm { jmp p[1026 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenMutexA() {
        __asm { jmp p[1027 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenMutexW() {
        __asm { jmp p[1028 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenPackageInfoByFullName() {
        __asm { jmp p[1029 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenPrivateNamespaceA() {
        __asm { jmp p[1030 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenPrivateNamespaceW() {
        __asm { jmp p[1031 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenProcess() {
        __asm { jmp p[1032 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenProcessToken() {
        __asm { jmp p[1033 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenProfileUserMapping() {
        __asm { jmp p[1034 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenSemaphoreA() {
        __asm { jmp p[1035 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenSemaphoreW() {
        __asm { jmp p[1036 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenState() {
        __asm { jmp p[1037 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenStateAtom() {
        __asm { jmp p[1038 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenStateExplicit() {
        __asm { jmp p[1039 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenThread() {
        __asm { jmp p[1040 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenThreadToken() {
        __asm { jmp p[1041 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenWaitableTimerA() {
        __asm { jmp p[1042 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OpenWaitableTimerW() {
        __asm { jmp p[1043 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OutputDebugStringA() {
        __asm { jmp p[1044 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OutputDebugStringW() {
        __asm { jmp p[1045 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_OverrideRoamingDataModificationTimesInRange() {
        __asm { jmp p[1046 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PackageFamilyNameFromFullName() {
        __asm { jmp p[1047 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PackageFamilyNameFromId() {
        __asm { jmp p[1048 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PackageFullNameFromId() {
        __asm { jmp p[1049 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PackageIdFromFullName() {
        __asm { jmp p[1050 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PackageNameAndPublisherIdFromFamilyName() {
        __asm { jmp p[1051 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PeekConsoleInputA() {
        __asm { jmp p[1052 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PeekConsoleInputW() {
        __asm { jmp p[1053 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PeekNamedPipe() {
        __asm { jmp p[1054 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PostQueuedCompletionStatus() {
        __asm { jmp p[1055 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PowerClearRequest() {
        __asm { jmp p[1056 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PowerCreateRequest() {
        __asm { jmp p[1057 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PowerSetRequest() {
        __asm { jmp p[1058 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PrefetchVirtualMemory() {
        __asm { jmp p[1059 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PrepareTape() {
        __asm { jmp p[1060 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PrivCopyFileExW() {
        __asm { jmp p[1061 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PrivMoveFileIdentityW() {
        __asm { jmp p[1062 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Process32First() {
        __asm { jmp p[1063 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Process32FirstW() {
        __asm { jmp p[1064 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Process32Next() {
        __asm { jmp p[1065 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Process32NextW() {
        __asm { jmp p[1066 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ProcessIdToSessionId() {
        __asm { jmp p[1067 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PublishStateChangeNotification() {
        __asm { jmp p[1068 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PulseEvent() {
        __asm { jmp p[1069 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_PurgeComm() {
        __asm { jmp p[1070 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryActCtxSettingsW() {
        __asm { jmp p[1071 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryActCtxSettingsWWorker() {
        __asm { jmp p[1072 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryActCtxW() {
        __asm { jmp p[1073 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryActCtxWWorker() {
        __asm { jmp p[1074 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryDepthSList() {
        __asm { jmp p[1075 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryDosDeviceA() {
        __asm { jmp p[1076 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryDosDeviceW() {
        __asm { jmp p[1077 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryFullProcessImageNameA() {
        __asm { jmp p[1078 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryFullProcessImageNameW() {
        __asm { jmp p[1079 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryIdleProcessorCycleTime() {
        __asm { jmp p[1080 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryIdleProcessorCycleTimeEx() {
        __asm { jmp p[1081 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryInformationJobObject() {
        __asm { jmp p[1082 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryMemoryResourceNotification() {
        __asm { jmp p[1083 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryPerformanceCounter() {
        __asm { jmp p[1084 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryPerformanceFrequency() {
        __asm { jmp p[1085 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryProcessAffinityUpdateMode() {
        __asm { jmp p[1086 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryProcessCycleTime() {
        __asm { jmp p[1087 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryStateAtomValueInfo() {
        __asm { jmp p[1088 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryStateContainerItemInfo() {
        __asm { jmp p[1089 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryThreadCycleTime() {
        __asm { jmp p[1090 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryThreadProfiling() {
        __asm { jmp p[1091 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryThreadpoolStackInformation() {
        __asm { jmp p[1092 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueryUnbiasedInterruptTime() {
        __asm { jmp p[1093 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueueUserAPC() {
        __asm { jmp p[1094 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_QueueUserWorkItem() {
        __asm { jmp p[1095 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RaiseException() {
        __asm { jmp p[1096 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RaiseFailFastException() {
        __asm { jmp p[1097 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RaiseInvalid16BitExeError() {
        __asm { jmp p[1098 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReOpenFile() {
        __asm { jmp p[1099 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadConsoleA() {
        __asm { jmp p[1100 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadConsoleInputA() {
        __asm { jmp p[1101 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadConsoleInputExA() {
        __asm { jmp p[1102 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadConsoleInputExW() {
        __asm { jmp p[1103 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadConsoleInputW() {
        __asm { jmp p[1104 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadConsoleOutputA() {
        __asm { jmp p[1105 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadConsoleOutputAttribute() {
        __asm { jmp p[1106 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadConsoleOutputCharacterA() {
        __asm { jmp p[1107 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadConsoleOutputCharacterW() {
        __asm { jmp p[1108 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadConsoleOutputW() {
        __asm { jmp p[1109 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadConsoleW() {
        __asm { jmp p[1110 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadDirectoryChangesW() {
        __asm { jmp p[1111 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadFile() {
        __asm { jmp p[1112 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadFileEx() {
        __asm { jmp p[1113 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadFileScatter() {
        __asm { jmp p[1114 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadProcessMemory() {
        __asm { jmp p[1115 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadStateAtomValue() {
        __asm { jmp p[1116 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadStateContainerValue() {
        __asm { jmp p[1117 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReadThreadProfilingData() {
        __asm { jmp p[1118 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegCloseKey() {
        __asm { jmp p[1119 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegCopyTreeW() {
        __asm { jmp p[1120 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegCreateKeyExA() {
        __asm { jmp p[1121 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegCreateKeyExW() {
        __asm { jmp p[1122 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegDeleteKeyExA() {
        __asm { jmp p[1123 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegDeleteKeyExW() {
        __asm { jmp p[1124 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegDeleteTreeA() {
        __asm { jmp p[1125 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegDeleteTreeW() {
        __asm { jmp p[1126 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegDeleteValueA() {
        __asm { jmp p[1127 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegDeleteValueW() {
        __asm { jmp p[1128 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegDisablePredefinedCacheEx() {
        __asm { jmp p[1129 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegEnumKeyExA() {
        __asm { jmp p[1130 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegEnumKeyExW() {
        __asm { jmp p[1131 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegEnumValueA() {
        __asm { jmp p[1132 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegEnumValueW() {
        __asm { jmp p[1133 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegFlushKey() {
        __asm { jmp p[1134 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegGetKeySecurity() {
        __asm { jmp p[1135 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegGetValueA() {
        __asm { jmp p[1136 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegGetValueW() {
        __asm { jmp p[1137 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegLoadKeyA() {
        __asm { jmp p[1138 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegLoadKeyW() {
        __asm { jmp p[1139 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegLoadMUIStringA() {
        __asm { jmp p[1140 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegLoadMUIStringW() {
        __asm { jmp p[1141 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegNotifyChangeKeyValue() {
        __asm { jmp p[1142 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegOpenCurrentUser() {
        __asm { jmp p[1143 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegOpenKeyExA() {
        __asm { jmp p[1144 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegOpenKeyExW() {
        __asm { jmp p[1145 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegOpenUserClassesRoot() {
        __asm { jmp p[1146 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegQueryInfoKeyA() {
        __asm { jmp p[1147 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegQueryInfoKeyW() {
        __asm { jmp p[1148 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegQueryValueExA() {
        __asm { jmp p[1149 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegQueryValueExW() {
        __asm { jmp p[1150 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegRestoreKeyA() {
        __asm { jmp p[1151 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegRestoreKeyW() {
        __asm { jmp p[1152 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegSaveKeyExA() {
        __asm { jmp p[1153 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegSaveKeyExW() {
        __asm { jmp p[1154 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegSetKeySecurity() {
        __asm { jmp p[1155 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegSetValueExA() {
        __asm { jmp p[1156 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegSetValueExW() {
        __asm { jmp p[1157 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegUnLoadKeyA() {
        __asm { jmp p[1158 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegUnLoadKeyW() {
        __asm { jmp p[1159 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegisterApplicationRecoveryCallback() {
        __asm { jmp p[1160 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegisterApplicationRestart() {
        __asm { jmp p[1161 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegisterBadMemoryNotification() {
        __asm { jmp p[1162 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegisterConsoleIME() {
        __asm { jmp p[1163 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegisterConsoleOS2() {
        __asm { jmp p[1164 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegisterConsoleVDM() {
        __asm { jmp p[1165 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegisterStateChangeNotification() {
        __asm { jmp p[1166 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegisterStateLock() {
        __asm { jmp p[1167 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegisterWaitForInputIdle() {
        __asm { jmp p[1168 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegisterWaitForSingleObject() {
        __asm { jmp p[1169 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegisterWaitForSingleObjectEx() {
        __asm { jmp p[1170 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegisterWowBaseHandlers() {
        __asm { jmp p[1171 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RegisterWowExec() {
        __asm { jmp p[1172 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReleaseActCtx() {
        __asm { jmp p[1173 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReleaseActCtxWorker() {
        __asm { jmp p[1174 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReleaseMutex() {
        __asm { jmp p[1175 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReleaseMutexWhenCallbackReturns() {
        __asm { jmp p[1176 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReleaseSRWLockExclusive() {
        __asm { jmp p[1177 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReleaseSRWLockShared() {
        __asm { jmp p[1178 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReleaseSemaphore() {
        __asm { jmp p[1179 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReleaseSemaphoreWhenCallbackReturns() {
        __asm { jmp p[1180 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReleaseStateLock() {
        __asm { jmp p[1181 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RemoveDirectoryA() {
        __asm { jmp p[1182 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RemoveDirectoryTransactedA() {
        __asm { jmp p[1183 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RemoveDirectoryTransactedW() {
        __asm { jmp p[1184 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RemoveDirectoryW() {
        __asm { jmp p[1185 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RemoveDllDirectory() {
        __asm { jmp p[1186 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RemoveLocalAlternateComputerNameA() {
        __asm { jmp p[1187 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RemoveLocalAlternateComputerNameW() {
        __asm { jmp p[1188 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RemoveSecureMemoryCacheCallback() {
        __asm { jmp p[1189 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RemoveVectoredContinueHandler() {
        __asm { jmp p[1190 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RemoveVectoredExceptionHandler() {
        __asm { jmp p[1191 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReplaceFile() {
        __asm { jmp p[1192 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReplaceFileA() {
        __asm { jmp p[1193 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReplaceFileW() {
        __asm { jmp p[1194 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ReplacePartitionUnit() {
        __asm { jmp p[1195 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RequestDeviceWakeup() {
        __asm { jmp p[1196 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RequestWakeupLatency() {
        __asm { jmp p[1197 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ResetEvent() {
        __asm { jmp p[1198 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ResetState() {
        __asm { jmp p[1199 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ResetWriteWatch() {
        __asm { jmp p[1200 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ResolveDelayLoadedAPI() {
        __asm { jmp p[1201 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ResolveDelayLoadsFromDll() {
        __asm { jmp p[1202 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ResolveLocaleName() {
        __asm { jmp p[1203 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RestoreLastError() {
        __asm { jmp p[1204 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ResumeThread() {
        __asm { jmp p[1205 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RtlCaptureContext() {
        __asm { jmp p[1206 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RtlCaptureStackBackTrace() {
        __asm { jmp p[1207 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RtlFillMemory() {
        __asm { jmp p[1208 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RtlMoveMemory() {
        __asm { jmp p[1209 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RtlUnwind() {
        __asm { jmp p[1210 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_RtlZeroMemory() {
        __asm { jmp p[1211 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ScrollConsoleScreenBufferA() {
        __asm { jmp p[1212 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ScrollConsoleScreenBufferW() {
        __asm { jmp p[1213 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SearchPathA() {
        __asm { jmp p[1214 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SearchPathW() {
        __asm { jmp p[1215 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetCachedSigningLevel() {
        __asm { jmp p[1216 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetCalendarInfoA() {
        __asm { jmp p[1217 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetCalendarInfoW() {
        __asm { jmp p[1218 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetComPlusPackageInstallStatus() {
        __asm { jmp p[1219 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetCommBreak() {
        __asm { jmp p[1220 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetCommConfig() {
        __asm { jmp p[1221 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetCommMask() {
        __asm { jmp p[1222 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetCommState() {
        __asm { jmp p[1223 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetCommTimeouts() {
        __asm { jmp p[1224 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetComputerNameA() {
        __asm { jmp p[1225 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetComputerNameExA() {
        __asm { jmp p[1226 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetComputerNameExW() {
        __asm { jmp p[1227 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetComputerNameW() {
        __asm { jmp p[1228 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleActiveScreenBuffer() {
        __asm { jmp p[1229 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleCP() {
        __asm { jmp p[1230 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleCtrlHandler() {
        __asm { jmp p[1231 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleCursor() {
        __asm { jmp p[1232 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleCursorInfo() {
        __asm { jmp p[1233 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleCursorMode() {
        __asm { jmp p[1234 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleCursorPosition() {
        __asm { jmp p[1235 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleDisplayMode() {
        __asm { jmp p[1236 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleFont() {
        __asm { jmp p[1237 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleHardwareState() {
        __asm { jmp p[1238 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleHistoryInfo() {
        __asm { jmp p[1239 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleIcon() {
        __asm { jmp p[1240 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleInputExeNameA() {
        __asm { jmp p[1241 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleInputExeNameW() {
        __asm { jmp p[1242 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleKeyShortcuts() {
        __asm { jmp p[1243 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleLocalEUDC() {
        __asm { jmp p[1244 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleMaximumWindowSize() {
        __asm { jmp p[1245 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleMenuClose() {
        __asm { jmp p[1246 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleMode() {
        __asm { jmp p[1247 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleNlsMode() {
        __asm { jmp p[1248 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleNumberOfCommandsA() {
        __asm { jmp p[1249 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleNumberOfCommandsW() {
        __asm { jmp p[1250 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleOS2OemFormat() {
        __asm { jmp p[1251 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleOutputCP() {
        __asm { jmp p[1252 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsolePalette() {
        __asm { jmp p[1253 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleScreenBufferInfoEx() {
        __asm { jmp p[1254 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleScreenBufferSize() {
        __asm { jmp p[1255 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleTextAttribute() {
        __asm { jmp p[1256 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleTitleA() {
        __asm { jmp p[1257 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleTitleW() {
        __asm { jmp p[1258 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetConsoleWindowInfo() {
        __asm { jmp p[1259 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetCriticalSectionSpinCount() {
        __asm { jmp p[1260 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetCurrentConsoleFontEx() {
        __asm { jmp p[1261 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetCurrentDirectoryA() {
        __asm { jmp p[1262 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetCurrentDirectoryW() {
        __asm { jmp p[1263 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetDefaultCommConfigA() {
        __asm { jmp p[1264 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetDefaultCommConfigW() {
        __asm { jmp p[1265 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetDefaultDllDirectories() {
        __asm { jmp p[1266 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetDllDirectoryA() {
        __asm { jmp p[1267 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetDllDirectoryW() {
        __asm { jmp p[1268 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetDynamicTimeZoneInformation() {
        __asm { jmp p[1269 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetEndOfFile() {
        __asm { jmp p[1270 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetEnvironmentStringsA() {
        __asm { jmp p[1271 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetEnvironmentStringsW() {
        __asm { jmp p[1272 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetEnvironmentVariableA() {
        __asm { jmp p[1273 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetEnvironmentVariableW() {
        __asm { jmp p[1274 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetErrorMode() {
        __asm { jmp p[1275 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetEvent() {
        __asm { jmp p[1276 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetEventWhenCallbackReturns() {
        __asm { jmp p[1277 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFileApisToANSI() {
        __asm { jmp p[1278 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFileApisToOEM() {
        __asm { jmp p[1279 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFileAttributesA() {
        __asm { jmp p[1280 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFileAttributesTransactedA() {
        __asm { jmp p[1281 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFileAttributesTransactedW() {
        __asm { jmp p[1282 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFileAttributesW() {
        __asm { jmp p[1283 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFileBandwidthReservation() {
        __asm { jmp p[1284 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFileCompletionNotificationModes() {
        __asm { jmp p[1285 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFileInformationByHandle() {
        __asm { jmp p[1286 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFileIoOverlappedRange() {
        __asm { jmp p[1287 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFilePointer() {
        __asm { jmp p[1288 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFilePointerEx() {
        __asm { jmp p[1289 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFileShortNameA() {
        __asm { jmp p[1290 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFileShortNameW() {
        __asm { jmp p[1291 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFileTime() {
        __asm { jmp p[1292 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFileValidData() {
        __asm { jmp p[1293 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFirmwareEnvironmentVariableA() {
        __asm { jmp p[1294 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFirmwareEnvironmentVariableExA() {
        __asm { jmp p[1295 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFirmwareEnvironmentVariableExW() {
        __asm { jmp p[1296 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetFirmwareEnvironmentVariableW() {
        __asm { jmp p[1297 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetHandleContext() {
        __asm { jmp p[1298 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetHandleCount() {
        __asm { jmp p[1299 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetHandleInformation() {
        __asm { jmp p[1300 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetInformationJobObject() {
        __asm { jmp p[1301 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetLastConsoleEventActive() {
        __asm { jmp p[1302 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetLastError() {
        __asm { jmp p[1303 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetLocalPrimaryComputerNameA() {
        __asm { jmp p[1304 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetLocalPrimaryComputerNameW() {
        __asm { jmp p[1305 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetLocalTime() {
        __asm { jmp p[1306 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetLocaleInfoA() {
        __asm { jmp p[1307 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetLocaleInfoW() {
        __asm { jmp p[1308 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetMailslotInfo() {
        __asm { jmp p[1309 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetMessageWaitingIndicator() {
        __asm { jmp p[1310 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetNamedPipeAttribute() {
        __asm { jmp p[1311 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetNamedPipeHandleState() {
        __asm { jmp p[1312 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetPriorityClass() {
        __asm { jmp p[1313 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetProcessAffinityMask() {
        __asm { jmp p[1314 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetProcessAffinityUpdateMode() {
        __asm { jmp p[1315 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetProcessDEPPolicy() {
        __asm { jmp p[1316 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetProcessInformation() {
        __asm { jmp p[1317 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetProcessMitigationPolicy() {
        __asm { jmp p[1318 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetProcessPreferredUILanguages() {
        __asm { jmp p[1319 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetProcessPriorityBoost() {
        __asm { jmp p[1320 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetProcessShutdownParameters() {
        __asm { jmp p[1321 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetProcessWorkingSetSize() {
        __asm { jmp p[1322 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetProcessWorkingSetSizeEx() {
        __asm { jmp p[1323 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetRoamingLastObservedChangeTime() {
        __asm { jmp p[1324 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetSearchPathMode() {
        __asm { jmp p[1325 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetStateVersion() {
        __asm { jmp p[1326 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetStdHandle() {
        __asm { jmp p[1327 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetStdHandleEx() {
        __asm { jmp p[1328 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetSystemFileCacheSize() {
        __asm { jmp p[1329 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetSystemPowerState() {
        __asm { jmp p[1330 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetSystemTime() {
        __asm { jmp p[1331 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetSystemTimeAdjustment() {
        __asm { jmp p[1332 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetTapeParameters() {
        __asm { jmp p[1333 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetTapePosition() {
        __asm { jmp p[1334 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetTermsrvAppInstallMode() {
        __asm { jmp p[1335 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadAffinityMask() {
        __asm { jmp p[1336 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadContext() {
        __asm { jmp p[1337 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadErrorMode() {
        __asm { jmp p[1338 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadExecutionState() {
        __asm { jmp p[1339 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadGroupAffinity() {
        __asm { jmp p[1340 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadIdealProcessor() {
        __asm { jmp p[1341 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadIdealProcessorEx() {
        __asm { jmp p[1342 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadInformation() {
        __asm { jmp p[1343 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadLocale() {
        __asm { jmp p[1344 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadPreferredUILanguages() {
        __asm { jmp p[1345 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadPriority() {
        __asm { jmp p[1346 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadPriorityBoost() {
        __asm { jmp p[1347 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadStackGuarantee() {
        __asm { jmp p[1348 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadToken() {
        __asm { jmp p[1349 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadUILanguage() {
        __asm { jmp p[1350 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadpoolStackInformation() {
        __asm { jmp p[1351 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadpoolThreadMaximum() {
        __asm { jmp p[1352 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadpoolThreadMinimum() {
        __asm { jmp p[1353 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadpoolTimer() {
        __asm { jmp p[1354 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadpoolTimerEx() {
        __asm { jmp p[1355 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadpoolWait() {
        __asm { jmp p[1356 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetThreadpoolWaitEx() {
        __asm { jmp p[1357 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetTimeZoneInformation() {
        __asm { jmp p[1358 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetTimerQueueTimer() {
        __asm { jmp p[1359 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetUnhandledExceptionFilter() {
        __asm { jmp p[1360 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetUserGeoID() {
        __asm { jmp p[1361 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetVDMCurrentDirectories() {
        __asm { jmp p[1362 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetVolumeLabelA() {
        __asm { jmp p[1363 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetVolumeLabelW() {
        __asm { jmp p[1364 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetVolumeMountPointA() {
        __asm { jmp p[1365 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetVolumeMountPointW() {
        __asm { jmp p[1366 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetVolumeMountPointWStub() {
        __asm { jmp p[1367 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetWaitableTimer() {
        __asm { jmp p[1368 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetWaitableTimerEx() {
        __asm { jmp p[1369 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetXStateFeaturesMask() {
        __asm { jmp p[1370 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SetupComm() {
        __asm { jmp p[1371 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ShowConsoleCursor() {
        __asm { jmp p[1372 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SignalObjectAndWait() {
        __asm { jmp p[1373 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SizeofResource() {
        __asm { jmp p[1374 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Sleep() {
        __asm { jmp p[1375 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SleepConditionVariableCS() {
        __asm { jmp p[1376 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SleepConditionVariableSRW() {
        __asm { jmp p[1377 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SleepEx() {
        __asm { jmp p[1378 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SortCloseHandle() {
        __asm { jmp p[1379 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SortGetHandle() {
        __asm { jmp p[1380 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_StartThreadpoolIo() {
        __asm { jmp p[1381 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SubmitThreadpoolWork() {
        __asm { jmp p[1382 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SubscribeStateChangeNotification() {
        __asm { jmp p[1383 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SuspendThread() {
        __asm { jmp p[1384 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SwitchToFiber() {
        __asm { jmp p[1385 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SwitchToThread() {
        __asm { jmp p[1386 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SystemTimeToFileTime() {
        __asm { jmp p[1387 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SystemTimeToTzSpecificLocalTime() {
        __asm { jmp p[1388 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_SystemTimeToTzSpecificLocalTimeEx() {
        __asm { jmp p[1389 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TerminateJobObject() {
        __asm { jmp p[1390 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TerminateProcess() {
        __asm { jmp p[1391 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TerminateThread() {
        __asm { jmp p[1392 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TermsrvAppInstallMode() {
        __asm { jmp p[1393 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TermsrvConvertSysRootToUserDir() {
        __asm { jmp p[1394 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TermsrvCreateRegEntry() {
        __asm { jmp p[1395 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TermsrvDeleteKey() {
        __asm { jmp p[1396 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TermsrvDeleteValue() {
        __asm { jmp p[1397 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TermsrvGetPreSetValue() {
        __asm { jmp p[1398 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TermsrvGetWindowsDirectoryA() {
        __asm { jmp p[1399 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TermsrvGetWindowsDirectoryW() {
        __asm { jmp p[1400 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TermsrvOpenRegEntry() {
        __asm { jmp p[1401 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TermsrvOpenUserClasses() {
        __asm { jmp p[1402 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TermsrvRestoreKey() {
        __asm { jmp p[1403 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TermsrvSetKeySecurity() {
        __asm { jmp p[1404 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TermsrvSetValueKey() {
        __asm { jmp p[1405 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TermsrvSyncUserIniFileExt() {
        __asm { jmp p[1406 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Thread32First() {
        __asm { jmp p[1407 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Thread32Next() {
        __asm { jmp p[1408 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TlsAlloc() {
        __asm { jmp p[1409 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TlsFree() {
        __asm { jmp p[1410 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TlsGetValue() {
        __asm { jmp p[1411 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TlsSetValue() {
        __asm { jmp p[1412 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Toolhelp32ReadProcessMemory() {
        __asm { jmp p[1413 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TransactNamedPipe() {
        __asm { jmp p[1414 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TransmitCommChar() {
        __asm { jmp p[1415 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TryAcquireSRWLockExclusive() {
        __asm { jmp p[1416 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TryAcquireSRWLockShared() {
        __asm { jmp p[1417 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TryEnterCriticalSection() {
        __asm { jmp p[1418 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TrySubmitThreadpoolCallback() {
        __asm { jmp p[1419 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TzSpecificLocalTimeToSystemTime() {
        __asm { jmp p[1420 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_TzSpecificLocalTimeToSystemTimeEx() {
        __asm { jmp p[1421 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UTRegister() {
        __asm { jmp p[1422 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UTUnRegister() {
        __asm { jmp p[1423 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UnhandledExceptionFilter() {
        __asm { jmp p[1424 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UnlockFile() {
        __asm { jmp p[1425 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UnlockFileEx() {
        __asm { jmp p[1426 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UnmapViewOfFile() {
        __asm { jmp p[1427 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UnmapViewOfFileEx() {
        __asm { jmp p[1428 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UnregisterApplicationRecoveryCallback() {
        __asm { jmp p[1429 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UnregisterApplicationRestart() {
        __asm { jmp p[1430 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UnregisterBadMemoryNotification() {
        __asm { jmp p[1431 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UnregisterConsoleIME() {
        __asm { jmp p[1432 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UnregisterStateChangeNotification() {
        __asm { jmp p[1433 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UnregisterStateLock() {
        __asm { jmp p[1434 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UnregisterWait() {
        __asm { jmp p[1435 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UnregisterWaitEx() {
        __asm { jmp p[1436 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UnsubscribeStateChangeNotification() {
        __asm { jmp p[1437 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UpdateCalendarDayOfWeek() {
        __asm { jmp p[1438 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UpdateProcThreadAttribute() {
        __asm { jmp p[1439 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UpdateResourceA() {
        __asm { jmp p[1440 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_UpdateResourceW() {
        __asm { jmp p[1441 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VDMConsoleOperation() {
        __asm { jmp p[1442 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VDMOperationStarted() {
        __asm { jmp p[1443 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VerLanguageNameA() {
        __asm { jmp p[1444 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VerLanguageNameW() {
        __asm { jmp p[1445 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VerSetConditionMask() {
        __asm { jmp p[1446 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VerifyConsoleIoHandle() {
        __asm { jmp p[1447 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VerifyScripts() {
        __asm { jmp p[1448 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VerifyVersionInfoA() {
        __asm { jmp p[1449 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VerifyVersionInfoW() {
        __asm { jmp p[1450 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VirtualAlloc() {
        __asm { jmp p[1451 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VirtualAllocEx() {
        __asm { jmp p[1452 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VirtualAllocExNuma() {
        __asm { jmp p[1453 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VirtualFree() {
        __asm { jmp p[1454 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VirtualFreeEx() {
        __asm { jmp p[1455 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VirtualLock() {
        __asm { jmp p[1456 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VirtualProtect() {
        __asm { jmp p[1457 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VirtualProtectEx() {
        __asm { jmp p[1458 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VirtualQuery() {
        __asm { jmp p[1459 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VirtualQueryEx() {
        __asm { jmp p[1460 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_VirtualUnlock() {
        __asm { jmp p[1461 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WTSGetActiveConsoleSessionId() {
        __asm { jmp p[1462 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WaitCommEvent() {
        __asm { jmp p[1463 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WaitForDebugEvent() {
        __asm { jmp p[1464 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WaitForMultipleObjects() {
        __asm { jmp p[1465 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WaitForMultipleObjectsEx() {
        __asm { jmp p[1466 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WaitForSingleObject() {
        __asm { jmp p[1467 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WaitForSingleObjectEx() {
        __asm { jmp p[1468 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WaitForThreadpoolIoCallbacks() {
        __asm { jmp p[1469 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WaitForThreadpoolTimerCallbacks() {
        __asm { jmp p[1470 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WaitForThreadpoolWaitCallbacks() {
        __asm { jmp p[1471 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WaitForThreadpoolWorkCallbacks() {
        __asm { jmp p[1472 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WaitNamedPipeA() {
        __asm { jmp p[1473 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WaitNamedPipeW() {
        __asm { jmp p[1474 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WakeAllConditionVariable() {
        __asm { jmp p[1475 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WakeConditionVariable() {
        __asm { jmp p[1476 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerGetFlags() {
        __asm { jmp p[1477 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerRegisterFile() {
        __asm { jmp p[1478 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerRegisterFileWorker() {
        __asm { jmp p[1479 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerRegisterMemoryBlock() {
        __asm { jmp p[1480 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerRegisterMemoryBlockWorker() {
        __asm { jmp p[1481 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerRegisterRuntimeExceptionModule() {
        __asm { jmp p[1482 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerRegisterRuntimeExceptionModuleWorker() {
        __asm { jmp p[1483 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerSetFlags() {
        __asm { jmp p[1484 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerUnregisterFile() {
        __asm { jmp p[1485 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerUnregisterFileWorker() {
        __asm { jmp p[1486 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerUnregisterMemoryBlock() {
        __asm { jmp p[1487 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerUnregisterMemoryBlockWorker() {
        __asm { jmp p[1488 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerUnregisterRuntimeExceptionModule() {
        __asm { jmp p[1489 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerUnregisterRuntimeExceptionModuleWorker() {
        __asm { jmp p[1490 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerpCleanupMessageMapping() {
        __asm { jmp p[1491 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerpGetDebugger() {
        __asm { jmp p[1492 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerpInitiateRemoteRecovery() {
        __asm { jmp p[1493 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerpLaunchAeDebug() {
        __asm { jmp p[1494 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerpNotifyLoadStringResource() {
        __asm { jmp p[1495 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerpNotifyLoadStringResourceEx() {
        __asm { jmp p[1496 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerpNotifyLoadStringResourceWorker() {
        __asm { jmp p[1497 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerpNotifyUseStringResource() {
        __asm { jmp p[1498 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerpNotifyUseStringResourceWorker() {
        __asm { jmp p[1499 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WerpStringLookup() {
        __asm { jmp p[1500 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WideCharToMultiByte() {
        __asm { jmp p[1501 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WinExec() {
        __asm { jmp p[1502 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Wow64DisableWow64FsRedirection() {
        __asm { jmp p[1503 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Wow64EnableWow64FsRedirection() {
        __asm { jmp p[1504 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Wow64GetThreadContext() {
        __asm { jmp p[1505 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Wow64GetThreadSelectorEntry() {
        __asm { jmp p[1506 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Wow64RevertWow64FsRedirection() {
        __asm { jmp p[1507 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Wow64SetThreadContext() {
        __asm { jmp p[1508 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_Wow64SuspendThread() {
        __asm { jmp p[1509 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteConsoleA() {
        __asm { jmp p[1510 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteConsoleInputA() {
        __asm { jmp p[1511 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteConsoleInputVDMA() {
        __asm { jmp p[1512 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteConsoleInputVDMW() {
        __asm { jmp p[1513 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteConsoleInputW() {
        __asm { jmp p[1514 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteConsoleOutputA() {
        __asm { jmp p[1515 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteConsoleOutputAttribute() {
        __asm { jmp p[1516 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteConsoleOutputCharacterA() {
        __asm { jmp p[1517 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteConsoleOutputCharacterW() {
        __asm { jmp p[1518 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteConsoleOutputW() {
        __asm { jmp p[1519 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteConsoleW() {
        __asm { jmp p[1520 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteFile() {
        __asm { jmp p[1521 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteFileEx() {
        __asm { jmp p[1522 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteFileGather() {
        __asm { jmp p[1523 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WritePrivateProfileSectionA() {
        __asm { jmp p[1524 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WritePrivateProfileSectionW() {
        __asm { jmp p[1525 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WritePrivateProfileStringA() {
        __asm { jmp p[1526 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WritePrivateProfileStringW() {
        __asm { jmp p[1527 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WritePrivateProfileStructA() {
        __asm { jmp p[1528 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WritePrivateProfileStructW() {
        __asm { jmp p[1529 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteProcessMemory() {
        __asm { jmp p[1530 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteProfileSectionA() {
        __asm { jmp p[1531 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteProfileSectionW() {
        __asm { jmp p[1532 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteProfileStringA() {
        __asm { jmp p[1533 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteProfileStringW() {
        __asm { jmp p[1534 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteStateAtomValue() {
        __asm { jmp p[1535 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteStateContainerValue() {
        __asm { jmp p[1536 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_WriteTapemark() {
        __asm { jmp p[1537 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ZombifyActCtx() {
        __asm { jmp p[1538 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_ZombifyActCtxWorker() {
        __asm { jmp p[1539 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY__hread() {
        __asm { jmp p[1540 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY__hwrite() {
        __asm { jmp p[1541 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY__lclose() {
        __asm { jmp p[1542 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY__lcreat() {
        __asm { jmp p[1543 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY__llseek() {
        __asm { jmp p[1544 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY__lopen() {
        __asm { jmp p[1545 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY__lread() {
        __asm { jmp p[1546 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY__lwrite() {
        __asm { jmp p[1547 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrcat() {
        __asm { jmp p[1548 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrcatA() {
        __asm { jmp p[1549 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrcatW() {
        __asm { jmp p[1550 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrcmp() {
        __asm { jmp p[1551 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrcmpA() {
        __asm { jmp p[1552 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrcmpW() {
        __asm { jmp p[1553 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrcmpi() {
        __asm { jmp p[1554 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrcmpiA() {
        __asm { jmp p[1555 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrcmpiW() {
        __asm { jmp p[1556 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrcpy() {
        __asm { jmp p[1557 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrcpyA() {
        __asm { jmp p[1558 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrcpyW() {
        __asm { jmp p[1559 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrcpyn() {
        __asm { jmp p[1560 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrcpynA() {
        __asm { jmp p[1561 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrcpynW() {
        __asm { jmp p[1562 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrlen() {
        __asm { jmp p[1563 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrlenA() {
        __asm { jmp p[1564 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_lstrlenW() {
        __asm { jmp p[1565 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_timeBeginPeriod() {
        __asm { jmp p[1566 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_timeEndPeriod() {
        __asm { jmp p[1567 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_timeGetDevCaps() {
        __asm { jmp p[1568 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_timeGetSystemTime() {
        __asm { jmp p[1569 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_timeGetTime() {
        __asm { jmp p[1570 * 4] }
    }
}
