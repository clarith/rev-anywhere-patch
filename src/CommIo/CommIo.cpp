#include <windows.h>

HINSTANCE hLibMine;
HINSTANCE hLib;
FARPROC p[30];

bool lastCoinState;
DWORD lastButtonState;
DWORD currentButtonState;

POINT lastMousePos;
bool lastMouseLButton;

// typedef DWORD (WINAPI *cCommIo_Open)(const char* port);

namespace {
    void updateTouchInfo(int x, int y, UINT pointerFlags)
    {
        POINTER_TOUCH_INFO contact = {};

        contact.pointerInfo.pointerType = PT_TOUCH;
        contact.pointerInfo.pointerFlags = pointerFlags;
        contact.pointerInfo.pointerId = 0;          //contact 0
        contact.pointerInfo.ptPixelLocation.x = x; // X co-ordinate of touch on screen
        contact.pointerInfo.ptPixelLocation.y = y; // Y co-ordinate of touch on screen

        InjectTouchInput(1, &contact);
    }

    void updateMouse()
    {
        bool currentMouseLButton = (GetAsyncKeyState(VK_LBUTTON) >> 15) & 1;

        if (currentMouseLButton && !lastMouseLButton) {
            GetCursorPos(&lastMousePos);
            updateTouchInfo(lastMousePos.x, lastMousePos.y, POINTER_FLAG_DOWN | POINTER_FLAG_INRANGE | POINTER_FLAG_INCONTACT);
        }

        if (currentMouseLButton && lastMouseLButton) {
            GetCursorPos(&lastMousePos);
            updateTouchInfo(lastMousePos.x, lastMousePos.y, POINTER_FLAG_UPDATE | POINTER_FLAG_INRANGE | POINTER_FLAG_INCONTACT);
        }

        if (!currentMouseLButton && lastMouseLButton) {
            // When POINTER_FLAG_UP is set, ptPixelLocation of POINTER_INFO should be the same as
            // the value of the previous touch injection frame with POINTER_FLAG_UPDATE.
            // Otherwise, the injection fails with ERROR_INVALID_PARAMETER
            updateTouchInfo(lastMousePos.x, lastMousePos.y, POINTER_FLAG_UP);
        }

        lastMouseLButton = currentMouseLButton;
    }
}

extern "C"
{
    __declspec(dllexport) DWORD WINAPI PROXY_cCommIo_Open(const char* port)
    {
        // (cCommIo_Open(p[23]))(port);
        return 1;
    }
        
    __declspec(dllexport) DWORD WINAPI PROXY_cCommIo_GetCoin()
    {
        lastButtonState = currentButtonState;
        currentButtonState =
            (((GetAsyncKeyState(VK_SPACE) >> 15) & 1) << 0) | // TEST
            (((GetAsyncKeyState(VK_DOWN) >> 15) & 1) << 1) | // DOWN
            (((GetAsyncKeyState(VK_ESCAPE) >> 15) & 1) << 2) | // CANCEL
            (((GetAsyncKeyState(VK_UP) >> 15) & 1) << 3) | // UP
            (((GetAsyncKeyState('H') >> 15) & 1) << 8); // HEADPHONE CONNECTED

        // @todo Don't know how to check SERVICE button

        updateMouse();

        bool currentCoinState = (GetAsyncKeyState('C') >> 15) & 1;

        if (currentCoinState && !lastCoinState) {
            lastCoinState = currentCoinState;
            return 1;
        } else {
            lastCoinState = currentCoinState;
            return 0;
        }
    }

    __declspec(dllexport) DWORD WINAPI PROXY_cCommIo_GetRelease()
    {
        return (~currentButtonState & lastButtonState);
    }

    __declspec(dllexport) DWORD WINAPI PROXY_cCommIo_GetTrigger()
    {
        return (currentButtonState & ~lastButtonState);
    }

    __declspec(dllexport) DWORD WINAPI PROXY_cCommIo_GetStatus()
    {
        return 1;
    }
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    if (fdwReason == DLL_PROCESS_ATTACH)
    {
        InitializeTouchInjection(1, TOUCH_FEEDBACK_DEFAULT);

        hLibMine = hinstDLL;
        hLib = LoadLibrary("CommIo.dll.orig");
        if(!hLib) return FALSE;

        /*
        p[0] = GetProcAddress(hLib, "?Close@cCommIo@@QAEXXZ");
        p[1] = GetProcAddress(hLib, "?GetCoin@cCommIo@@QAEIXZ");
        p[2] = GetProcAddress(hLib, "?GetRelease@cCommIo@@QAEIXZ");
        p[3] = GetProcAddress(hLib, "?GetStatus@cCommIo@@QAEIXZ");
        p[4] = GetProcAddress(hLib, "?GetSwitch@cCommIo@@QAEIXZ");
        p[5] = GetProcAddress(hLib, "?GetTrigger@cCommIo@@QAEIXZ");
        p[6] = GetProcAddress(hLib, "?GetVolume@cCommIo@@QAEJH@Z");
        p[7] = GetProcAddress(hLib, "?GetVolume@cCommIo@@QAEJXZ");
        p[8] = GetProcAddress(hLib, "?Open@cCommIo@@QAEHPBD@Z");
        p[9] = GetProcAddress(hLib, "?SetAmpMute@cCommIo@@QAEJHH@Z");
        p[10] = GetProcAddress(hLib, "?SetCoin@cCommIo@@QAEXH@Z");
        p[11] = GetProcAddress(hLib, "?SetOutput@cCommIo@@QAEXK@Z");
        p[12] = GetProcAddress(hLib, "?SetVolume@cCommIo@@QAEJHH@Z");
        p[13] = GetProcAddress(hLib, "?Update@cCommIo@@QAEXXZ");
        */
        p[14] = GetProcAddress(hLib, "cCommIo_Close");
        p[15] = GetProcAddress(hLib, "cCommIo_GetAmpVolume");
        p[16] = GetProcAddress(hLib, "cCommIo_GetCoin");
        p[17] = GetProcAddress(hLib, "cCommIo_GetRelease");
        p[18] = GetProcAddress(hLib, "cCommIo_GetStatus");
        p[19] = GetProcAddress(hLib, "cCommIo_GetSwitch");
        p[20] = GetProcAddress(hLib, "cCommIo_GetTrigger");
        p[21] = GetProcAddress(hLib, "cCommIo_GetVolume");
        p[22] = GetProcAddress(hLib, "cCommIo_IsLoaded");
        p[23] = GetProcAddress(hLib, "cCommIo_Open");
        p[24] = GetProcAddress(hLib, "cCommIo_Recieve");
        p[25] = GetProcAddress(hLib, "cCommIo_SetAmpMute");
        p[26] = GetProcAddress(hLib, "cCommIo_SetAmpVolume");
        p[27] = GetProcAddress(hLib, "cCommIo_SetCoin");
        p[28] = GetProcAddress(hLib, "cCommIo_SetOutput");
        p[29] = GetProcAddress(hLib, "cCommIo_Update");
    }
    else if (fdwReason == DLL_PROCESS_DETACH)
    {
        FreeLibrary(hLib);
        return TRUE;
    }

    return TRUE;
}

extern "C"
{
    /*
    __declspec( naked ) void WINAPI PROXY_?Close@cCommIo@@QAEXXZ() {
        __asm { jmp p[0 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_?GetCoin@cCommIo@@QAEIXZ() {
        __asm { jmp p[1 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_?GetRelease@cCommIo@@QAEIXZ() {
        __asm { jmp p[2 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_?GetStatus@cCommIo@@QAEIXZ() {
        __asm { jmp p[3 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_?GetSwitch@cCommIo@@QAEIXZ() {
        __asm { jmp p[4 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_?GetTrigger@cCommIo@@QAEIXZ() {
        __asm { jmp p[5 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_?GetVolume@cCommIo@@QAEJH@Z() {
        __asm { jmp p[6 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_?GetVolume@cCommIo@@QAEJXZ() {
        __asm { jmp p[7 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_?Open@cCommIo@@QAEHPBD@Z() {
        __asm { jmp p[8 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_?SetAmpMute@cCommIo@@QAEJHH@Z() {
        __asm { jmp p[9 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_?SetCoin@cCommIo@@QAEXH@Z() {
        __asm { jmp p[10 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_?SetOutput@cCommIo@@QAEXK@Z() {
        __asm { jmp p[11 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_?SetVolume@cCommIo@@QAEJHH@Z() {
        __asm { jmp p[12 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_?Update@cCommIo@@QAEXXZ() {
        __asm { jmp p[13 * 4] }
    }
    */
    __declspec( naked ) void WINAPI PROXY_cCommIo_Close() {
        __asm { jmp p[14 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_cCommIo_GetAmpVolume() {
        __asm { jmp p[15 * 4] }
    }
    /*
    __declspec( naked ) void WINAPI PROXY_cCommIo_GetCoin() {
        __asm { jmp p[16 * 4] }
    }
    */
    /*
    __declspec( naked ) void WINAPI PROXY_cCommIo_GetRelease() {
        __asm { jmp p[17 * 4] }
    }
    */
    /*
    __declspec( naked ) void WINAPI PROXY_cCommIo_GetStatus() {
        __asm { jmp p[18 * 4] }
    }
    */
    __declspec( naked ) void WINAPI PROXY_cCommIo_GetSwitch() {
        __asm { jmp p[19 * 4] }
    }
    /*
    __declspec( naked ) void WINAPI PROXY_cCommIo_GetTrigger() {
        __asm { jmp p[20 * 4] }
    }
    */
    __declspec( naked ) void WINAPI PROXY_cCommIo_GetVolume() {
        __asm { jmp p[21 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_cCommIo_IsLoaded() {
        __asm { jmp p[22 * 4] }
    }
    /*
    __declspec( naked ) void WINAPI PROXY_cCommIo_Open() {
        __asm { jmp p[23 * 4] }
    }
    */
    __declspec( naked ) void WINAPI PROXY_cCommIo_Recieve() {
        __asm { jmp p[24 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_cCommIo_SetAmpMute() {
        __asm { jmp p[25 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_cCommIo_SetAmpVolume() {
        __asm { jmp p[26 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_cCommIo_SetCoin() {
        __asm { jmp p[27 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_cCommIo_SetOutput() {
        __asm { jmp p[28 * 4] }
    }
    __declspec( naked ) void WINAPI PROXY_cCommIo_Update() {
        __asm { jmp p[29 * 4] }
    }
}
