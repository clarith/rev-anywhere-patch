# REV Anywhere Patch
With this patch, you can run REV on normal PC.
Also useful on nu board (external I/O board is needless)

## WARNING
Don't run game.bat. It erases all files on Y:

## Features
- Dongle check bypass
- Mouse input support
- Insert coin emulation
- Service button emulation

## Setup
1. Rename original CommIo.dll to CommIo.dll.orig
2. Copy Xernel32.dll, CommIo.dll to same directory as Rev_v11.exe
3. Import sega.reg to registry
4. Patch Rev_v11.exe to inject Xernel32.dll

## How to patch exe
For offline final version (md5: d6ed706cfffc77686b694a24139a8a2b)

```
009261c0: 4b -> 58
```

For other versions, "KERNEL32.dll" appears two times in exe.
Modify later part to "XERNEL32.dll".

## Usage
Use mouse to emulate touch.
- Left click: Tap
- Left drag: Swipe/Flick

Use keyboard to emulate the service button/coin.
- 'C': Insert Coin
- 'H': Headphone Connected
- Space: TEST button
- Cursor up: UP Button
- Cursor down: DOWN Button
- ESC: CANCEL button

## Limitation
- Works for offline version only (Currently)
- Test menu settings is not saved on normal PC
  - Only useful on nu board
